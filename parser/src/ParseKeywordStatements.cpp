
#include "ParseKeywordStatements.h"

#include <algorithm>
#include <sstream>

#include "Logger.h"
#include "Utility.h"

#include "ErrorCount.h"
#include "Parser.h"
#include "ParseExpressions.h"
#include "ParseVariableExpression.h"
#include "details/ScopeHandling.h"

#include "ast/ForExpression.h"
#include "ast/IfExpression.h"
#include "ast/ElseIfExpression.h"
#include "ast/ElseExpression.h"
#include "ast/WhileExpression.h"
#include "ast/DoWhileExpression.h"
#include "ast/LoopExpression.h"
#include "ast/StructureExpression.h"
// #include "ast/EnumerationExpression.h"
#include "ast/FunctionDefinitionExpression.h"
#include "ast/ReturnExpression.h"
#include "ast/ImportExpression.h"
#include "ast/ExternExpression.h"

auto ISL::Parser::parseLoopStatement(TokenListIter&, const TokenListIter&)
    -> AST::ExpressionPtr
{
    Common::Log::debug("Loop Statement.");

    auto loop_expr = std::make_unique<AST::LoopExpression>();
    details::setExpressionInserterFunction(loop_expr->getBody());

    return std::move(loop_expr);
}

/**
 * @details <ReturnExpression> ::= return <Expression>
 */
auto ISL::Parser::parseReturnStatement(TokenListIter& iter,
                                       const TokenListIter& end)
    -> AST::ExpressionPtr
{
    if(auto expr = parseExpression(iter, end); expr)
        return std::make_unique<AST::ReturnExpression>(std::move(expr.value()));
    else {
        return std::make_unique<AST::EmptyExpression>();
    }
}

/**
 * @details <ImportExpression> ::= import <ValueExpression>
 */
auto ISL::Parser::parseImportStatement(TokenListIter& iter,
                                       const TokenListIter& end)
    -> AST::ExpressionPtr
{
    if(auto expr = parseValue(iter, end); expr)
        return std::make_unique<AST::ImportExpression>(std::move(expr.value()));
    else {
        return std::make_unique<AST::EmptyExpression>();
    }
}

/**
 * @details <FunctionDefinition> ::= function <Identifier> ([<ParameterExpression>, ...]) -> <TypeExpression>
 */
static auto parseFunctionStatement(ISL::Parser::TokenListIter& iter,
                                   const ISL::Parser::TokenListIter& end,
                                   bool require_param_names,
                                   bool create_inserter_function)
    -> ISL::Parser::AST::FunctionDefinitionExpressionPtr
{
    using namespace std::string_literals;

    ISL::Common::Log::debug("Beginning to parse function definition statement.");

    std::string error_message;

    ISL::Parser::AST::IdentifierExpressionPtr func_iden;
    std::vector<ISL::Parser::AST::ParameterExpressionPtr> param_list;
    ISL::Parser::AST::TypeExpressionPtr return_type;

    std::unique_ptr<ISL::Parser::AST::FunctionDefinitionExpression> func_def_expr;

    ISL::Parser::TokenListIter end_expr(end);

    if(auto iden = ISL::Parser::parseIdentifier(iter, end); iden) {
        func_iden = std::move(iden.value());
    } else {
        error_message = "Unexpected token before function name.";
        goto error;
    }

    ++iter;
    if(iter->getTokenType() != ISL::Lexer::TokenType::TOK_START_OPERATOR) {
        error_message = "Unexpected token before '('.";
        goto error;
    }

    ++iter;
    {
        std::stringstream ss;
        for(auto it = iter; it != end; ++it)
            std::visit([&ss](auto&& t) {
                using T = std::decay_t<decltype(t)>;

                ss << "[" << t.getTokenType() << "]";
                if constexpr(std::is_same_v<T, ISL::Lexer::FuncArgEndToken>) {
                    ss << "(" << t.getParameterCount() << " params)";
                } else if constexpr(std::is_same_v<T, ISL::Lexer::OperatorToken>) {
                    ss << "(" << t.getOperator() << ")";
                } else if constexpr(std::is_same_v<T, ISL::Lexer::IdentifierToken>) {
                    ss << "(" << t.getIdentifier() << ")";
                } else if constexpr(std::is_same_v<T, ISL::Lexer::ValueToken>) {
                    std::visit([&ss](auto&& v) {
                        ss << "(" << v << ")";
                    }, t.getValueType());
                }

                ss << " => ";
            }, *it.raw());
        ss << " DONE";
        ISL::Common::Log::debug(ss.str());
    }

    while(iter != end && iter->getTokenType() != ISL::Lexer::TokenType::TOK_END_OPERATOR)
    {
        ISL::Parser::TokenListIter arg_sep(iter);

        {
            std::stringstream ss;
            ss << "Remaining tokens in param list: ";
            for(auto it = arg_sep; it != end; ++it)
                std::visit([&ss](auto&& t) {
                    using T = std::decay_t<decltype(t)>;

                    ss << "[" << t.getTokenType() << "]";
                    if constexpr(std::is_same_v<T, ISL::Lexer::FuncArgEndToken>) {
                        ss << "(" << t.getParameterCount() << " params)";
                    } else if constexpr(std::is_same_v<T, ISL::Lexer::OperatorToken>) {
                        ss << "(" << t.getOperator() << ")";
                    } else if constexpr(std::is_same_v<T, ISL::Lexer::IdentifierToken>) {
                        ss << "(" << t.getIdentifier() << ")";
                    } else if constexpr(std::is_same_v<T, ISL::Lexer::ValueToken>) {
                        std::visit([&ss](auto&& v) {
                            ss << "(" << v << ")";
                        }, t.getValueType());
                    }

                    ss << " => ";
                }, *it.raw());
            ss << " DONE";
            ISL::Common::Log::debug(ss.str());
        }

        bool at_end = false;
        int paren_count = 0;
        while(arg_sep + 1 != end) {
            if(arg_sep->getTokenType() == ISL::Lexer::TokenType::TOK_START_OPERATOR) {
                ++paren_count;
                ISL::Common::Log::debug("Found '('. Currently within "s + std::to_string(paren_count) + " nested parentheses.");
            } else if(arg_sep->getTokenType() == ISL::Lexer::TokenType::TOK_END_OPERATOR) {
                --paren_count;
                ISL::Common::Log::debug("Found ')'. Currently within "s + std::to_string(paren_count) + " nested parentheses.");

                if(paren_count < 0) {
                    at_end = true;
                    break;
                }
            } else if(arg_sep->getTokenType() == ISL::Lexer::TokenType::TOK_ARG_SEPERATOR &&
                      paren_count == 0)
            {
                ISL::Common::Log::debug("Found ','. End of parameter arg found.");
                break;
            }

            if(paren_count < -1) {
                error_message = "Mismatched parentheses!";
                goto error;
            }

            ++arg_sep;
        }

        {
            std::stringstream ss;
            ss << "iter=" << iter->getTokenType() << ", arg_sep=" << arg_sep->getTokenType();
            ISL::Common::Log::debug(ss.str());
        }

        if(auto param = ISL::Parser::parseParameterExpression(iter, arg_sep,
                                                              require_param_names); param)
        {
            param_list.push_back(std::move(param.value()));
        } else {
            error_message = "Failed to parse parameter #"s +
                            std::to_string(param_list.size() + 1) + ".";
            goto error;
        }

        iter = arg_sep + (int)(!at_end);
    }

    // Once we break out of the loop, we should be at the ')'.
    // The only other way to break out is if 'iter = end', which means the
    //  statement must be malformed.
    if(iter == end) {
        error_message = "Unexpected end of statement before ')'.";
        goto error;
    }

    ++iter;
    if(iter->getTokenType() != ISL::Lexer::TokenType::TOK_FUNC_ARROW) {
        error_message = "Unexpected token before '->'";
        goto error;
    }

    ++iter;
    if(auto rt = ISL::Parser::parseTypeExpression(iter, end); rt) {
        return_type = std::move(rt.value());
    } else {
        error_message = "Failed to parse return type.";
        goto error;
    }

    ISL::Common::Log::debug("Function Definition parsed successfully.");
    func_def_expr = std::make_unique<ISL::Parser::AST::FunctionDefinitionExpression>(
                        std::move(func_iden), std::move(param_list),
                        std::move(return_type));
    if(create_inserter_function) {
        ISL::Parser::details::setExpressionInserterFunction(func_def_expr->getBody());
    }
    return func_def_expr;

error:
    ISL::Common::Log::error(error_message);
    ISL::Parser::error();
    return nullptr;
}

auto ISL::Parser::parseFunctionStatement(TokenListIter& iter,
                                         const TokenListIter& end)
    -> AST::ExpressionPtr
{
    if(auto func = ::parseFunctionStatement(iter, end, true, true); func) {
        return func;
    } else {
        return std::make_unique<ISL::Parser::AST::EmptyExpression>();
    }
}

/**
 * @details <ExternExpression> ::= extern <FunctionDefinition>
 *                                 extern <ParameterExpression>
 */
auto ISL::Parser::parseExternStatement(TokenListIter& iter,
                                       const TokenListIter& end)
    -> AST::ExpressionPtr
{
    Common::Log::debug("Beginning to parse extern expression.");

    const auto& token = *iter.raw();

    // This is an external function, parse it as such
    if(iter->getTokenType() == Lexer::TokenType::TOK_IDENTIFIER) {
        if(std::get<Lexer::IdentifierToken>(token).getIdentifier() == "function")
        {
            ++iter;

            Common::Log::debug("parsing extern as a function.");
            if(auto function = ::parseFunctionStatement(iter, end, false, false);
                    function)
            {
                return std::make_unique<AST::ExternExpression>(std::move(function));
            }
        } else {
            Common::Log::debug("parsing extern as a parameter/value.");
            if(auto var_expr = parseParameterExpression(iter, end, true); var_expr)
            {
                return std::make_unique<AST::ExternExpression>(std::move(var_expr.value()));
            }
        }
    } else {
        std::stringstream ss;
        ss << "Invalid token following 'extern': " << iter->getTokenType();
        Common::Log::error(ss.str());
        error();
    }

    return std::make_unique<AST::EmptyExpression>();
}

/**
 * @details <ForExpression> ::= <ForStatement> <Statement>... <EndForStatement>
 *          <ForStatement> ::= for ( <VariableExpression>; <Expression>; <Expression> );
 *          <EndForStatement> ::= endfor;
 */
auto ISL::Parser::parseForStatement(TokenListIter& iter,
                                   const TokenListIter& end) -> AST::ExpressionPtr
{
    Common::Log::debug("Beginning to parse for statement.");

    Common::Log::debug("Current token stream is: ");
    for(auto it = iter; it != end; ++it) {
        std::stringstream ss;
        ss << "\t" << it->getTokenType() << ", ";

        Common::Log::debug(ss.str());
    }

    std::string error_message;

    AST::VariableExpressionPtr var_expr;
    AST::ExpressionPtr cond_expr, inc_expr;
    std::unique_ptr<AST::ForExpression> for_expr;

    TokenListIter end_expr(end);

    if(iter->getTokenType() != Lexer::TokenType::TOK_START_OPERATOR) {
        error_message = "Unexpected token before '('.";
        goto error;
    }

    ++iter;
    end_expr = std::find_if(iter, end, [](auto&& token) -> bool {
        return token.getTokenType() == Lexer::TokenType::TOK_END_STATEMENT;
    });

    if(end_expr == end) {
        error_message = "Unexpected token when parsing variable expression before ';'.";
        goto error;
    }

    Common::Log::debug("Parsing variable expression portion.");
    if(var_expr = parseVariableExpression(iter, end_expr); var_expr == nullptr)
    {
        goto error;
    }

    // Move iter up to the end and search for the next end
    iter = end_expr + 1;
    end_expr = std::find_if(iter, end, [](auto&& token) -> bool {
        return token.getTokenType() == Lexer::TokenType::TOK_END_STATEMENT;
    });

    if(end_expr == end) {
        error_message = "Unexpected token when parsing condition expression before ';'.";
        goto error;
    }

    Common::Log::debug("Parsing condition expression portion.");
    if(auto expr = parseExpression(iter, end_expr); expr)
        cond_expr = std::move(expr.value());
    else {
        goto error;
    }

    iter = end_expr + 1;
    end_expr = std::find_if(iter, end, [](auto&& token) -> bool {
        return token.getTokenType() == Lexer::TokenType::TOK_END_OPERATOR;
    });

    if(end_expr == end) {
        error_message = "Unexpected token before ')'.";
        goto error;
    }

    Common::Log::debug("Parsing incrementer expression portion.");
    if(auto expr = parseExpression(iter, end_expr); expr)
        inc_expr = std::move(expr.value());
    else
        goto error;

    if(iter = end_expr; iter->getTokenType() != Lexer::TokenType::TOK_END_OPERATOR)
    {
        error_message = "Unexpected token when parsing inc expression before ';'.";
        goto error;
    }

    Common::Log::debug("For loop parsed successfully.");
    for_expr = std::make_unique<AST::ForExpression>(std::move(var_expr),
                                                    std::move(cond_expr),
                                                    std::move(inc_expr));
    details::setExpressionInserterFunction(for_expr->getBody());
    return for_expr;

error:
    Common::Log::error(error_message);
    error();
    return std::make_unique<AST::EmptyExpression>();
}

ISL::Parser::AST::ExpressionPtr ISL::Parser::parseEndBlockStatement(TokenListIter&,
                                                                    const TokenListIter&)
{
    Common::Log::debug("Ending scope block.");
    if(details::getInserterFunctions().size() == 1) {
        Common::Log::warn("Attempting to end block when only 1 inserter "
                          "function exists. Possibly mismatched scopes.");
    }
    details::getInserterFunctions().pop();

    return std::make_unique<AST::EmptyExpression>();
}

template<typename IET>
ISL::Parser::AST::ExpressionPtr parseConditional(ISL::Parser::TokenListIter& iter,
                                             const ISL::Parser::TokenListIter& end)
{
    ISL::Common::Log::debug("Current token stream is: ");
    for(auto it = iter; it != end; ++it) {
        std::stringstream ss;
        ss << "\t" << it->getTokenType() << ", ";

        ISL::Common::Log::debug(ss.str());
    }

    std::string error_message;

    ISL::Parser::AST::ExpressionPtr cond_expr;
    std::unique_ptr<IET> if_expr;

    ISL::Parser::TokenListIter end_expr(end);

    if(iter->getTokenType() != ISL::Lexer::TokenType::TOK_START_OPERATOR) {
        error_message = "Unexpected token before '('.";
        goto error;
    }

    ++iter;
    end_expr = std::find_if(iter, end, [](auto&& token) -> bool {
        std::stringstream ss;
        ISL::Common::Log::debug(ss.str());

        return token.getTokenType() == ISL::Lexer::TokenType::TOK_END_OPERATOR;
    });

    if(end_expr == end) {
        error_message = "Unexpected token before ')'.";
        goto error;
    }

    if(auto expr = ISL::Parser::parseExpression(iter, end_expr); expr)
        cond_expr = std::move(expr.value());
    else
        goto error;

    if(++iter; iter->getTokenType() != ISL::Lexer::TokenType::TOK_END_STATEMENT) {
        error_message = "Unexpected token before ';'.";
        goto error;
    }

    ISL::Common::Log::debug("Conditional Statement parsed successfully.");
    if_expr = std::make_unique<IET>(std::move(cond_expr));
    ISL::Parser::details::setExpressionInserterFunction(if_expr->getBody());

    return std::move(if_expr);

error:
    ISL::Common::Log::error(error_message);
    ISL::Parser::error();
    return std::make_unique<ISL::Parser::AST::EmptyExpression>();
}

/**
 * @details <IfStatement> ::= if ( <Expression> );
 */
ISL::Parser::AST::ExpressionPtr ISL::Parser::parseIfStatement(TokenListIter& iter,
                                                              const TokenListIter& end)
{
    Common::Log::debug("If statement.");
    return parseConditional<AST::IfExpression>(iter, end);
}

/**
 * @details <ElseIfStatement> ::= elseif ( <Expression> );
 */
ISL::Parser::AST::ExpressionPtr ISL::Parser::parseElseIfStatement(TokenListIter& iter,
                                                                  const TokenListIter& end)
{
    Common::Log::debug("ElseIf statement.");
    // Do this because we are ending the previous If/ElseIf block
    details::getInserterFunctions().pop();

    return parseConditional<AST::ElseIfExpression>(iter, end);
}

ISL::Parser::AST::ExpressionPtr ISL::Parser::parseElseStatement(TokenListIter&, const TokenListIter&) {
    Common::Log::debug("Else statement.");

    // Do this because we are ending the previous If/ElseIf block
    details::getInserterFunctions().pop();

    auto else_expr = std::make_unique<AST::ElseExpression>();
    ISL::Parser::details::setExpressionInserterFunction(else_expr->getBody());

    return std::move(else_expr);
}

auto ISL::Parser::parseWhileStatement(TokenListIter& iter, const TokenListIter& end)
    -> AST::ExpressionPtr
{
    Common::Log::debug("While Loop statement.");
    return parseConditional<AST::WhileExpression>(iter, end);
}

auto ISL::Parser::parseDoWhileStatement(TokenListIter& iter, const TokenListIter& end)
    -> AST::ExpressionPtr
{
    Common::Log::debug("DoWhile Loop statement.");
    return parseConditional<AST::DoWhileExpression>(iter, end);
}

auto ISL::Parser::parseStructureStatement(TokenListIter& iter,
                                          const TokenListIter& end)
    -> AST::ExpressionPtr
{
    using namespace std::string_literals;

    Common::Log::debug("Beginning to parse structure definition statement.");

    AST::IdentifierExpressionPtr struct_iden;

    std::unique_ptr<AST::StructureExpression> expr;

    TokenListIter end_expr(end);

    if(auto iden = parseIdentifier(iter, end); iden) {
        struct_iden = std::move(iden.value());
    } else {
        Common::Log::error("Unexpected token before structure name.");
        error();
        return std::make_unique<AST::EmptyExpression>();
    }

    Common::Log::debug("Structure parsed successfully.");
    expr = std::make_unique<AST::StructureExpression>(std::move(struct_iden));
    auto& member_list = expr->getMembers();

    details::setExpressionInserterFunction([&member_list](AST::ExpressionPtr& expr)
    {
        if(auto e = Common::unique_dynamic_cast<AST::VariableExpression>(std::move(expr)))
        {
            member_list.push_back(std::move(e));
            return true;
        } else {
            Common::Log::error("Structures can only have member variables!");
            error();
            return false;
        }
    });
    return expr;
}

#if 0
auto ISL::Parser::parseEnumerationStatement(TokenListIter& iter,
                                            const TokenListIter& end)
    -> AST::ExpressionPtr
{
    using namespace std::string_literals;

    Common::Log::debug("Beginning to parse enumeration definition statement.");

    std::string error_message;

    AST::IdentifierExpressionPtr enum_iden;

    std::unique_ptr<AST::EnumerationExpression> expr;

    TokenListIter end_expr(end);

    if(auto iden = parseIdentifier(iter, end); iden) {
        enum_iden = std::move(iden.value());
    } else {
        error_message = "Unexpected token before enumeration name.";
        goto error;
    }

    Common::Log::debug("Enumeration parsed successfully.");
    expr = std::make_unique<AST::EnumerationExpression>(std::move(enum_iden));
    // details::setExpressionInserterFunction(expr->getBody());
    return expr;

error:
    Common::Log::error(error_message);
    error();
    return std::make_unique<AST::EmptyExpression>();
}
#endif

