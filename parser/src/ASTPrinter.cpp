
#include "ASTPrinter.h"

#include <sstream>

#define VISIT_SUB_EXPR(EXPR, VALUE, NAME) \
    do {                                  \
        traverseSubExprStart(NAME);       \
        traverse##EXPR ( VALUE );         \
        traverseSubExprEnd(NAME);         \
    } while(0)



ISL::Parser::ASTPrinter::ASTPrinter(std::ostream& stream): m_out(stream) {
}

bool ISL::Parser::ASTPrinter::traverseAST(const std::vector<AST::ExpressionPtr>& ast)
{
    m_sub_expr_count.push(ast.size());

    /* std::cout << formatStartOfTraversal() << formatNewLine(); */

    m_current_text = formatStartOfTraversal() + formatNewLine();

    return ASTVisitor::traverseAST(ast);
}

bool ISL::Parser::ASTPrinter::visitIdentifierExpression(const AST::IdentifierExpression& expr) {
    using namespace std::string_literals;

    m_current_text += indent() + formatProperty("type", "\"Identifier\"") + formatSeperator() + formatNewLine() +
                      indent() + formatProperty("iden", formatQuote() + expr.getIdentifier() + formatQuote()) + formatNewLine();

    return true;
}

bool ISL::Parser::ASTPrinter::visitConstantExpression(const AST::ConstantExpression& expr)
{
    std::stringstream ss;
    ss << expr.getValue();

    m_current_text += indent() + formatProperty("type", formatQuote() + "Value" + formatQuote()) + formatSeperator() + formatNewLine() +
                      indent() + formatProperty("value", ss.str()) + formatNewLine();

    return true;
}

bool ISL::Parser::ASTPrinter::visitBinaryExpression(const AST::BinaryExpression& expr)
{
    using namespace std::string_literals;

    std::stringstream ss;
    ss << expr.getOperator();

    m_current_text += indent() + formatProperty("type", formatQuote() + "BinaryExpression" + formatQuote()) + formatSeperator() + formatNewLine() +
                      indent() + formatProperty("operator", formatQuote() + ss.str() + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(2); // LHS, RHS

    VISIT_SUB_EXPR(Expression, expr.getLHS(), "lhs");
    VISIT_SUB_EXPR(Expression, expr.getRHS(), "rhs");

    return true;
}

bool ISL::Parser::ASTPrinter::visitUnaryExpression(const AST::UnaryExpression& expr) {
    using namespace std::string_literals;

    std::stringstream ss;
    ss << expr.getOperator();

    m_current_text += indent() + formatProperty("type", formatQuote() + "UnaryExpression" + formatQuote()) + formatSeperator() + formatNewLine() +
                      indent() + formatProperty("operator", formatQuote() + ss.str() + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Operand

    VISIT_SUB_EXPR(Expression, expr.getOperand(), "operand");

    return true;
}

bool ISL::Parser::ASTPrinter::visitCallExpression(const AST::CallExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "CallExpression" + formatQuote()) + formatSeperator() + formatNewLine() +
                      indent() + formatProperty("identifier", formatQuote() + expr.getIdentifier() + formatQuote()) + formatSeperator() + formatNewLine();
    m_sub_expr_count.push(expr.getArgumentList().size());

    for(auto i = 0; i < expr.getArgumentList().size(); ++i) {
        VISIT_SUB_EXPR(Expression, expr.getArgumentList().at(i), std::to_string(i));
    }

    return true;
}

bool ISL::Parser::ASTPrinter::visitWhileExpression(const AST::WhileExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "WhileExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(2); // Cond, Body

    VISIT_SUB_EXPR(Expression, expr.getCondExpression(), "cond");

    return true;
}

bool ISL::Parser::ASTPrinter::visitDoWhileExpression(const AST::DoWhileExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "DoWhileExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(2); // Cond, Body

    VISIT_SUB_EXPR(Expression, expr.getCondExpression(), "cond");

    return true;
}

bool ISL::Parser::ASTPrinter::visitIfExpression(const AST::IfExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "IfExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(2); // Cond, Body

    VISIT_SUB_EXPR(Expression, expr.getCondExpression(), "cond");

    return true;
}

bool ISL::Parser::ASTPrinter::visitElseIfExpression(const AST::ElseIfExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "ElseIfExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(2); // Cond, Body

    VISIT_SUB_EXPR(Expression, expr.getCondExpression(), "cond");

    return true;
}

bool ISL::Parser::ASTPrinter::visitElseExpression(const AST::ElseExpression&) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "ElseExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Body

    return true;
}

bool ISL::Parser::ASTPrinter::visitForExpression(const AST::ForExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "ForExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(4); // Var, Cond, Inc, Body

    VISIT_SUB_EXPR(VariableExpression, expr.getVarExpression(), "var");
    VISIT_SUB_EXPR(Expression, expr.getCondExpression(), "cond");
    VISIT_SUB_EXPR(Expression, expr.getIncExpression(), "inc");

    return true;
}

bool ISL::Parser::ASTPrinter::visitFunctionDefinitionExpression(const AST::FunctionDefinitionExpression&) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "FunctionDefinitionExpression" + formatQuote()) + formatSeperator() + formatNewLine();
    
    return true;
}

bool ISL::Parser::ASTPrinter::visitImportExpression(const AST::ImportExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "ImportExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Value

    VISIT_SUB_EXPR(ConstantExpression, expr.getValue(), "value");

    return true;
}

bool ISL::Parser::ASTPrinter::visitExternExpression(const AST::ExternExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "ExternExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Value

    std::visit([this](auto&& symbol) {
        using T = std::decay_t<decltype(symbol)>;
        if constexpr(std::is_same_v<T, AST::ParameterExpressionPtr>) {
            VISIT_SUB_EXPR(ParameterExpression, symbol, "symbol");
        } else if constexpr(std::is_same_v<T, AST::FunctionDefinitionExpressionPtr>) {
            VISIT_SUB_EXPR(FunctionDefinitionExpression, symbol, "symbol");
        }
    }, expr.getSymbol());

    return true;
}

bool ISL::Parser::ASTPrinter::visitLoopExpression(const AST::LoopExpression&) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "LoopExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Body

    return true;
}

bool ISL::Parser::ASTPrinter::visitReturnExpression(const AST::ReturnExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "ReturnExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Expression

    VISIT_SUB_EXPR(Expression, expr.getExpr(), "expr");

    return true;
}

bool ISL::Parser::ASTPrinter::visitStructureExpression(const AST::StructureExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "StructureExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Members

    return true;
}

bool ISL::Parser::ASTPrinter::visitVariableExpression(const AST::VariableExpression& expr) {
    m_current_text += indent() + formatProperty("type", formatQuote() + "VariableExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(3); // Type, Identifier, Value

    VISIT_SUB_EXPR(TypeExpression, expr.getTypeExpression(), "var_type");
    VISIT_SUB_EXPR(IdentifierExpression, expr.getVarIdentifier(), "iden");
    VISIT_SUB_EXPR(Expression, expr.getValueExpr(), "value");

    return true;
}

bool ISL::Parser::ASTPrinter::visitSimpleTypeExpression(const AST::SimpleTypeExpression&)
{
    m_current_text += indent() + formatProperty("type", formatQuote() + "SimpleTypeExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Identifier

    return true;
}

bool ISL::Parser::ASTPrinter::visitTypeOfExpression(const AST::TypeOfExpression&)
{
    m_current_text += indent() + formatProperty("type", formatQuote() + "TypeOfExpression" + formatQuote()) + formatSeperator() + formatNewLine();

    m_sub_expr_count.push(1); // Expression

    return true;
}

std::string ISL::Parser::ASTPrinter::indent() const {
    std::string s;
    for(int i = 0; i < m_depth; ++i) {
        s += "    ";
    }

    return s;
}

void ISL::Parser::ASTPrinter::traverseSubExprStart(const std::string& expr_name)
{
    // if(!m_sub_expr_count.empty())
    //     --m_sub_expr_count.top();

    auto property = formatPropertyKey(expr_name);

    // std::cout << indent() << property << (property.empty() ? "" : " ") << formatExprStart(expr_name);

    m_current_text += indent() + property + (property.empty() ? "" : " ") + formatExprStart(expr_name);

    // std::cout << formatNewLine();
    m_current_text += formatNewLine();

    ++m_depth;
}

#include <iostream>
void ISL::Parser::ASTPrinter::traverseSubExprEnd(const std::string& expr_name) {
    --m_depth;

    // std::cout << indent() << formatExprEnd(expr_name);

    m_current_text = m_current_text + indent() + formatExprEnd(expr_name);

    /*
    if(!m_sub_expr_count.empty())
        m_current_text += "[" + std::to_string(m_sub_expr_count.top()) + "|" + std::to_string(m_sub_expr_count.size()) + "]";
    */

    if(!m_sub_expr_count.empty() && m_sub_expr_count.top() > 1) {
        m_current_text += formatSeperator();
    }

    if(!m_sub_expr_count.empty() && m_sub_expr_count.top() > 0) {
        --m_sub_expr_count.top();
    }

    if(!m_sub_expr_count.empty() && m_sub_expr_count.top() == 0) {
        m_sub_expr_count.pop();
    }

    // std::cout << formatNewLine();
    m_current_text += formatNewLine();

}

void ISL::Parser::ASTPrinter::traverseSubExprGroupStart(const std::string& expr_name)
{
    auto property = formatPropertyKey(expr_name);

    m_sub_expr_count.push(getSizeOfGroup());

    m_current_text += indent() + property + (property.empty() ? "" : " ") + formatGroupStart(expr_name) + formatNewLine();

    ++m_depth;
}

void ISL::Parser::ASTPrinter::traverseSubExprGroupEnd(const std::string& expr_name)
{
    --m_depth;

    if(!m_sub_expr_count.empty())
        m_sub_expr_count.pop();

    m_current_text = m_current_text + indent() + formatGroupEnd(expr_name);

    m_current_text += formatNewLine();
}

void ISL::Parser::ASTPrinter::traversalFinished() {
    // std::cout << formatEndOfTraversal();

    m_current_text += formatEndOfTraversal();

    m_out << m_current_text;
}

std::string ISL::Parser::ASTPrinter::formatProperty(const std::string& key,
                                                        const std::string& value)
{
    return formatPropertyKey(key) + " " + formatPropertyValue(value);
}

std::string ISL::Parser::ASTPrinter::formatQuote() {
    return "\"";
}

std::string ISL::Parser::ASTPrinter::formatEscapedQuote() {
    return "\\\"";
}

