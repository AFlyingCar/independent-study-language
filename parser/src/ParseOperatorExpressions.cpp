
#include "ParseOperatorExpressions.h"

#include "ast/UnaryExpression.h"
#include "ast/BinaryExpression.h"

auto ISL::Parser::parseUnaryOperator(AST::ExpressionPtr operand1,
                                     const Lexer::OperatorToken& op)
    -> AST::ExpressionPtr
{
    return std::make_unique<AST::UnaryExpression>(op.getOperator(),
                                                  std::move(operand1));
}

auto ISL::Parser::parseBinaryOperator(AST::ExpressionPtr operand1,
                                      AST::ExpressionPtr operand2,
                                      const Lexer::OperatorToken& op)
    -> AST::ExpressionPtr
{
    return std::make_unique<AST::BinaryExpression>(op.getOperator(),
                                                   std::move(operand1),
                                                   std::move(operand2));
}

