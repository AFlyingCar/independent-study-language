
#include "ParseVariableExpression.h"

#include <algorithm>

#include "ast/TypeExpression.h"

#include "ParseExpressions.h"

#include "Logger.h"

auto ISL::Parser::parseVariableExpression(TokenListIter iter, TokenListIter end)
    -> AST::VariableExpressionPtr
{
    Common::Log::debug("parseVariableExpression");

    AST::VariableExpressionPtr var_expr = nullptr;

    AST::TypeExpressionPtr type = nullptr;

    for(auto i = iter; i != end; ++i) {
        if(!std::visit([&](auto&& token) -> bool {
            using T = std::decay_t<decltype(token)>;

            if constexpr(std::is_same_v<T, Lexer::IdentifierToken>) {
                const auto& iden = token.getIdentifier();
                if(iden == "typeof") {
                    Common::Log::debug("typeof expression!");
                    if((i + 1)->getTokenType() != Lexer::TokenType::TOK_START_OPERATOR)
                        return false;

                    size_t p = 1;
                    auto end_paren = std::find_if((i + 2).raw(), end.raw(), [&p](auto& tok) {
                        p += std::visit([](auto&& t) {
                                if(t.getTokenType() == Lexer::TokenType::TOK_START_OPERATOR)
                                    return 1;
                                else if(t.getTokenType() == Lexer::TokenType::TOK_END_OPERATOR)
                                    return -1;
                                else
                                    return 0;
                             }, tok);

                        if(p == 0)
                           return true;
                        else
                           return false;
                    });

                    auto next = i + 1;

                    AST::ExpressionPtr typeof_expr;
                    if(auto expr = parseExpression(next, end_paren); expr)
                        typeof_expr = std::move(expr.value());
                    else
                        typeof_expr = std::make_unique<AST::EmptyExpression>();

                    type = std::make_unique<AST::TypeOfExpression>(std::move(typeof_expr), std::move(type));
                } else {
                    auto is_equals = std::visit([](auto&& token) -> bool {
                        using T = std::decay_t<decltype(token)>;

                        if constexpr(std::is_same_v<T, Lexer::OperatorToken>) {
                            return token.getOperator().getOperatorType() == ISL::Common::OperatorType::ASSIGN;
                        }

                        return false;
                    }, *(i + 1).raw());

                    if(is_equals) {
                        // We _probably_ have a variable expression now

                        // Check: do we have a TypeExpression yet?
                        //  In otherwords, don't allow this to match "var = value"
                        if(type != nullptr) {

                            auto next = i + 2;

                            Common::Log::debug("Parsing expression for variable");
                            AST::ExpressionPtr value_expr;
                            if(auto expr = parseExpression(next, end); expr)
                                value_expr = std::move(expr.value());
                            else
                                value_expr = std::make_unique<AST::EmptyExpression>();

                            Common::Log::debug("Done");
                            AST::IdentifierExpressionPtr iden_expr;
                            if(auto opt = parseIdentifierImpl(token); opt) {
                                iden_expr = std::move(opt.value());
                            } else {
                                // TODO: throw error
                            }

                            var_expr = std::make_unique<AST::VariableExpression>(std::move(type),
                                                                             std::move(iden_expr),
                                                                             std::move(value_expr));
                            return false;
                        } else {
                            return false;
                        }
                    } else {
                        AST::IdentifierExpressionPtr iden_expr;
                        if(auto expr = parseIdentifierImpl(token); expr) {
                            iden_expr = std::move(expr.value());
                        } else {
                            // TODO: Error checking
                        }

                        // Still an identifier, but not a typeof, and not the
                        //   last one, so it will be part of the type
                        type = std::make_unique<AST::SimpleTypeExpression>(std::move(iden_expr),
                                                                           std::move(type));
                    }
                }
            } else {
                return false;
            }

            return true;
        }, *i.raw()))
        {
            break;
        }
    }

    return var_expr;
}

