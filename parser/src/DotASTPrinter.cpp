
#include "DotASTPrinter.h"

std::string escapeQuotes(const std::string& str) {
    std::string s;
    for(auto&& c : str) {
        if(c == '"')
            s += "\\";
        s += c;
    }
    return s;
}

ISL::Parser::DotASTPrinter::DotASTPrinter(std::ostream& os): ASTPrinter(os),
                                                             m_nodes(),
                                                             m_parents(),
                                                             m_current_node(0)
{
    m_nodes.push_back(Node{"Root", {}, true, static_cast<size_t>(-1)});
    m_parents.push(0);
}

std::string ISL::Parser::DotASTPrinter::formatStartOfTraversal() {
    return "digraph AST{\n";
}

std::string ISL::Parser::DotASTPrinter::formatEndOfTraversal() {
    std::string nodes;// = "    Node0 [shape=record, label=\"Root\"];\n";
    std::string connections;

    // Table Idx --> Current Child Idx
    std::map<size_t, size_t> children_counts;

    for(auto i = 0; i < m_nodes.size(); ++i) {
        std::string node_name = "Node" + std::to_string(i);

        auto&& node = m_nodes.at(i);

        // https://graphviz.gitlab.io/_pages/Gallery/directed/datastruct.gv.txt
        // "label=<fN>text
        // ...
        // "NodeMName":fN -> "NodeM_2Name":fN

        nodes += "    " + node_name + " " + formatAttributes(node) + ";\n";

        if(auto parent = node.parent; parent < m_nodes.size()) {
            connections += "    \"Node" + std::to_string(parent) + "\"";

            if(auto& parent_node = m_nodes.at(parent); parent_node.is_table) {
                connections += ":f" + std::to_string(children_counts[parent] + 1);
                ++children_counts[parent];
            }

            connections += " -> Node" + std::to_string(i) + ";\n";
        }
    }

    return nodes + "\n" + connections + "}";
}

std::string ISL::Parser::DotASTPrinter::formatSeperator() {
    return "";
}

std::string ISL::Parser::DotASTPrinter::formatNewLine() {
    return "";
}


std::string ISL::Parser::DotASTPrinter::formatProperty(const std::string& key,
                                                       const std::string& value)
{
    // This shouldn't fail, if it does then something else is wrong.
    m_nodes.at(m_parents.top()).attribs.push_back(std::make_pair(key, value));

    return "";
}

std::string ISL::Parser::DotASTPrinter::formatPropertyKey(const std::string&)
{
    return "";
}

std::string ISL::Parser::DotASTPrinter::formatPropertyValue(const std::string&)
{
    return "";
}

std::string ISL::Parser::DotASTPrinter::formatExprStart(const std::string& expr_name)
{
    bool is_in_group = false;

    if(!m_parents.empty()) {
        if(auto& node = m_nodes.at(m_parents.top()); node.is_table) {
            node.attribs.push_back(std::make_pair(expr_name,
                                                  std::to_string(m_nodes.size())));
            is_in_group = true;
        }
    }

    m_nodes.push_back(Node{is_in_group ? "" : expr_name, {}, false, m_parents.empty() ? 0 : m_parents.top()});

    m_parents.push(m_nodes.size() - 1);

    return "";
}

std::string ISL::Parser::DotASTPrinter::formatExprEnd(const std::string&) {
    if(!m_parents.empty())
        m_parents.pop();

    return "";
}

std::string ISL::Parser::DotASTPrinter::indent() const {
    return "";
}

std::string ISL::Parser::DotASTPrinter::formatAttributes(const Node& node) const
{
    std::string attribs = "[shape=record, ";

    attribs += "label=\"";
    if(!node.label.empty())
        attribs += "{" + node.label;
    if(!node.attribs.empty()) {
        if(!node.label.empty())
            attribs += "|";
        attribs += "{";
    }

    if(node.is_table) {
        size_t idx = 0;
        for(auto&& [key, value] : node.attribs) {
            attribs += "<f" + key + ">" + key;

            ++idx;
            if(idx < node.attribs.size())
                attribs += "|";
        }
    } else {
        size_t idx = 0;
        for(auto&& [key, value] : node.attribs) {
            attribs += key + "=" + escapeQuotes(value);

            ++idx;
            if(idx < node.attribs.size())
                attribs += "|";
        }
    }
    if(!node.attribs.empty())
        attribs += "}";

    if(!node.label.empty())
        attribs += "}";

    attribs += "\"]";

    return attribs;
}

std::string ISL::Parser::DotASTPrinter::formatGroupStart(const std::string& group_name)
{
    m_nodes.push_back(Node{group_name, {}, true, m_parents.empty() ? 0 : m_parents.top()});
    m_parents.push(m_nodes.size() - 1);

    return "";
}

std::string ISL::Parser::DotASTPrinter::formatGroupEnd(const std::string&) {
    if(!m_parents.empty())
        m_parents.pop();

    return "";
}

