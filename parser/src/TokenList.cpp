
#include "TokenList.h"

ISL::Parser::TokenListIter::TokenListIter(RawTokenListIter iter):
    m_iter(iter)
{ }

ISL::Parser::TokenListIter::TokenListIter(const TokenListIter& other):
    m_iter(other.m_iter)
{
}

ISL::Parser::TokenListIter& ISL::Parser::TokenListIter::operator=(const TokenListIter& other)
{
    m_iter = other.m_iter;

    return *this;
}


ISL::Parser::TokenListIter ISL::Parser::TokenListIter::operator+(int amount) const
{
    return TokenListIter(m_iter + amount);
}

bool ISL::Parser::TokenListIter::operator==(const TokenListIter& iter) const {
    return m_iter == iter.m_iter;
}

bool ISL::Parser::TokenListIter::operator!=(const TokenListIter& iter) const {
    return m_iter != iter.m_iter;
}

ISL::Parser::TokenListIter ISL::Parser::TokenListIter::operator++() {
    TokenListIter tli(m_iter);

    m_iter++;

    return tli;
}

ISL::Parser::TokenListIter& ISL::Parser::TokenListIter::operator++(int) {
    ++m_iter;
    return *this;
}

const ISL::Lexer::Token& ISL::Parser::TokenListIter::operator*() const {
    return std::visit([](auto&& token) -> const Lexer::Token& {
        return token;
    }, *m_iter);
}

const ISL::Lexer::Token* ISL::Parser::TokenListIter::operator->() const {
    return std::visit([](auto&& token) -> const Lexer::Token* {
        return &token;
    }, *m_iter);
}

ISL::Parser::RawTokenListIter ISL::Parser::TokenListIter::raw() {
    return m_iter;
}



