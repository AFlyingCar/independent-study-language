
#include "ParseExpressions.h"

#include <deque>
#include <memory>
#include <stack>
#include <queue>
#include <sstream>
#include <algorithm>

#include "Logger.h"
#include "ErrorCount.h"

#include "Token.h"

#include "ast/StringExpression.h"
#include "ast/BoolExpression.h"
#include "ast/CharacterExpression.h"
#include "ast/IntegerExpression.h"
#include "ast/FloatExpression.h"
#include "ast/DoubleExpression.h"
#include "ast/BinaryExpression.h"
#include "ast/CallExpression.h"

#include "ParseOperatorExpressions.h"

template<typename Iter>
ISL::Parser::AST::ExpressionPtr parseExpressionImpl(Iter iter,
                                                    const Iter& eiter)
{
    using namespace std::string_literals;

    std::stack<ISL::Parser::AST::ExpressionPtr> operands;
    std::stack<size_t> in_function;

    ISL::Common::Log::debug("parseExpressionImpl(...)");

    std::stringstream ss;
    for(auto it = iter; it != eiter; ++it)
        std::visit([&ss](auto&& t) {
            using T = std::decay_t<decltype(t)>;

            ss << "[" << t.getTokenType() << "]";
            if constexpr(std::is_same_v<T, ISL::Lexer::FuncArgEndToken>) {
                ss << "(" << t.getParameterCount() << " params)";
            } else if constexpr(std::is_same_v<T, ISL::Lexer::OperatorToken>) {
                ss << "(" << t.getOperator() << ")";
            } else if constexpr(std::is_same_v<T, ISL::Lexer::IdentifierToken>) {
                ss << "(" << t.getIdentifier() << ")";
            } else if constexpr(std::is_same_v<T, ISL::Lexer::ValueToken>) {
                std::visit([&ss](auto&& v) {
                    ss << "(" << v << ")";
                }, t.getValueType());
            }

            ss << " => ";
        }, *it);
    ss << " DONE";
    ISL::Common::Log::debug(ss.str());

    for(; iter != eiter; ++iter) {
        std::visit([&](auto&& token) -> bool {
            using T = std::decay_t<decltype(token)>;

            if constexpr(std::is_same_v<T, ISL::Lexer::OperatorToken>) {
                ISL::Common::Log::debug("Operator Token.");

                // TODO: What about unary operators?

                // Initialization becomes TYPE VARIABLE VALUE_EXPR ASSIGN

                if(operands.size() >= 2) {
                    // Popped off in Right -> Left order
                    auto op2 = std::move(operands.top());
                    operands.pop();
                    auto op1 = std::move(operands.top());
                    operands.pop();
                    operands.push(ISL::Parser::parseBinaryOperator(std::move(op1),
                                                                   std::move(op2),
                                                                   token));
                } else if(operands.size() == 1) {
                    auto op1 = std::move(operands.top());
                    operands.pop();
                    operands.push(ISL::Parser::parseUnaryOperator(std::move(op1),
                                                                  token));
                } else {

                    std::stringstream ss;
                    ss << "Invalid number of operands for "
                       << token.getOperator().getOperatorType() << ". Got "
                       << operands.size();
                    ISL::Common::Log::error(ss.str());
                }
            } else if constexpr(std::is_same_v<T, ISL::Lexer::ValueToken>) {
                ISL::Common::Log::debug("Value Token.");
                if(auto expr = ISL::Parser::parseValue(token.getValueType()); expr)
                    operands.push(std::move(expr.value()));
                else
                    return false;
                ISL::Common::Log::debug("There are "s + std::to_string(operands.size()) + " operands.");
            } else if constexpr(std::is_same_v<T, ISL::Lexer::IdentifierToken>) {
                if(auto iden = ISL::Parser::parseIdentifierImpl(token); iden) {
                    ISL::Common::Log::debug(token.getIdentifier());
                    operands.push(std::move(iden.value()));
                } else {
                    ISL::Common::Log::error("Skipping this expression.");
                    // return std::make_unique<ISL::Parser::AST::EmptyExpression>();
                    return false;
                }
            } else if constexpr(std::is_same_v<T, ISL::Lexer::FuncArgStartToken>) {
                if(!in_function.empty()) {

                    ISL::Parser::AST::CallExpression::ArgumentList args;

                    while(operands.size() > in_function.top()) {
                        args.push_back(std::move(operands.top()));
                        operands.pop();
                    }
                    // Make sure the arguments are in the correct order
                    std::reverse(args.begin(), args.end());

                    ISL::Common::Log::debug("FuncArgStartToken: Found "s +
                                            std::to_string(args.size()) +
                                            " arguments.");

                    operands.push(std::make_unique<ISL::Parser::AST::CallExpression>(
                            token.getFunctionName(), std::move(args))
                    );

                    in_function.pop();
                } else {
                    // TODO: Raise error
                    ISL::Common::Log::error("Found Function Argument start, but we aren't in a function!");
                }
            } else if constexpr(std::is_same_v<T, ISL::Lexer::Token>) {
#if 0
                switch(token.getTokenType()) {
                    case ISL::Lexer::TokenType::TOK_FUNC_ARG_END:
                        using namespace std::string_literals;
                        ISL::Common::Log::debug("Parsing function end. It says there should be "s +
                                                std::to_string() + " arguments. We detect " + std::to_string(operands.size()) + " left.");
                        in_function.push(operands.size());

                        break;
                    default:;
                }
#endif
            } else if constexpr(std::is_same_v<T, ISL::Lexer::FuncArgEndToken>) {
                    using namespace std::string_literals;
                    ISL::Common::Log::debug("Parsing function end. It says there should be "s +
                                            std::to_string(token.getParameterCount()) +
                                            " arguments. We detect " +
                                            std::to_string(operands.size()) +
                                            " left.");

                    in_function.push(operands.size());
            }

            return true;
        }, *iter);
    }

    ISL::Common::Log::debug("Done parsing each expression.");

    if(operands.size() != 1) {
        using namespace std::string_literals;
        ISL::Common::Log::error("Possibly mismatched values. Too many operands"
                                " left on stack ("s +
                                std::to_string(operands.size()) + ")");

        ISL::Parser::AST::ExpressionPtr expr = std::make_unique<ISL::Parser::AST::EmptyExpression>();
        std::stringstream ss;
        while(!operands.empty()) {
            expr = std::move(operands.top());
            operands.pop();
            expr->toJSON(ss);
            ss << std::endl;
        }
        ss << " DONE";
        ISL::Common::Log::debug(ss.str());

        return expr;
    }

    auto top = std::move(operands.top());
    return top;
}

auto ISL::Parser::parseIdentifierImpl(const Lexer::TokenVariant& token)
    -> std::optional<AST::IdentifierExpressionPtr>
{
    Common::Log::debug("Parsing identifier.");
    return std::visit([&](auto&& token)
        -> std::optional<AST::IdentifierExpressionPtr>
    {
        using T = std::decay_t<decltype(token)>;

        if constexpr(std::is_same_v<T, Lexer::IdentifierToken>) {
            return std::make_unique<AST::IdentifierExpression>(token.getIdentifier());
        } else {
            error();
            Common::Log::error("Expected identifier.");

            return std::nullopt;
        }
    }, token);
}

/**
 * @brief Parses a single expression
 * @details <Expression> ::= <Expression> <BinaryOperator> <Expression>
 *                        |  <UnaryOperator> <Expression>
 *                        |  <GroupingExpression>
 *                        |  <Identifier> <GroupedExpression>
 *                        |  <Identifier>
 *                        |  <ValueExpression>
 *
 */
std::optional<ISL::Parser::AST::ExpressionPtr> ISL::Parser::parseExpression(TokenListIter& iter,
                                                             const TokenListIter& end)
{
    Common::Log::debug("Parsing Expression.");
    std::stringstream ss;
    for(auto it = iter; it != end; ++it)
        ss << "[" << it->getTokenType() << "] => ";
    ss << " DONE";
    ISL::Common::Log::debug(ss.str());

    std::vector<Lexer::TokenVariant> output;
    std::stack<Lexer::OperatorToken> operators;

    std::stack<std::string> function_calls;

    // True == In function call
    // False == Not in function call
    std::stack<int> paren_state;

    Lexer::TokenVariant last_token;
    for(size_t idx = 0; iter != end; ++iter, ++idx) {
        std::visit([&](auto&& token) {
             using T = std::decay_t<decltype(token)>;

             if constexpr(std::is_same_v<T, Lexer::ValueToken>) {
                 output.push_back(token);
             // } else if( is a function???) {
             } else if constexpr(std::is_same_v<T, Lexer::OperatorToken>) {
                 if(token.getTokenType() == Lexer::TokenType::TOK_START_OPERATOR) {
                     // Note: if we see anything that isn't an operator followed
                     //  by a '(', then it is most likely a function call
                     //  <Binary/Unary/( Operator> ( ... ==> Non-function call
                     //  Alternatively, if we see ')(' or <Identifier>(,
                     //   then it is a function call
                     //  If the previous token was not an identifier, then that
                     //   means we are most likely in a situation where an
                     //   expression returns a function that we want to call

                     bool is_func_call = std::visit([](auto&& token) -> bool
                     {
                         using T = std::decay_t<decltype(token)>;

                         return std::is_same_v<T, Lexer::IdentifierToken> ||
                                token.getTokenType() == Lexer::TokenType::TOK_END_OPERATOR;
                     }, last_token);

                     if(is_func_call) {
                         using namespace std::string_literals;
                         Common::Log::debug("Detected a function call for '"s +
                                            std::visit([](auto&& t) -> std::string {
                                                if constexpr(std::is_same_v<std::decay_t<decltype(t)>, Lexer::IdentifierToken>)
                                                    return t.getIdentifier();
                                                else
                                                    return "<UNKNOWN NAME>";
                                            }, last_token) + "'");

                         std::visit([&](auto&& t) {
                             using T = std::decay_t<decltype(t)>;

                             if constexpr(std::is_same_v<T, Lexer::IdentifierToken>) {
                                 output.pop_back();
                                 function_calls.push(t.getIdentifier());
                             } else {
                                 // TODO: How do we denote that we have reached
                                 //  the start of the function argument list if
                                 //  we don't have a function name/identifier?
                             }
                         }, last_token);

                         output.push_back(Lexer::FuncArgEndToken());
                         paren_state.push(output.size());
                     } else {
                         paren_state.push(-1);
                     }

                     operators.push(token);
                 } else if(token.getTokenType() == Lexer::TokenType::TOK_END_OPERATOR) {
                     while(!operators.empty() &&
                           operators.top().getTokenType() != Lexer::TokenType::TOK_START_OPERATOR)
                     {
                         output.push_back(operators.top());
                         operators.pop();
                     }
                     // Pop once more for the '('
                     operators.pop();

                     // Are we actually currently in a function?
                     //  If so, then make sure to put the function call identifier back on the stack
                     if(paren_state.top() > 0) {
                         // But, make sure that it is a special token, so that it
                         //  doesn't get confused with normal identifiers
                         output.push_back(Lexer::FuncArgStartToken(function_calls.top()));
                         function_calls.pop();
                     }

                     paren_state.pop();

                     // Actually, is this even necessary to check???
                     if(!operators.empty() &&
                        operators.top().getTokenType() == Lexer::TokenType::TOK_END_OPERATOR)
                     {
                         // TODO: Mismatched parens. Throw an error
                         operators.pop();
                     }
                 } else {

                     int precedence = token.getOperator().getPrecedence();

                     while(!operators.empty() &&
                           ((operators.top().getOperator().getPrecedence() > precedence) ||
                            (operators.top().getOperator().getPrecedence() == precedence &&
                             token.getOperator().getAssociativity() == Common::Associativity::LEFT_TO_RIGHT)) &&
                           operators.top().getTokenType() != Lexer::TokenType::TOK_START_OPERATOR)
                     {
                         output.push_back(operators.top());
                         operators.pop();
                     }
                     operators.push(token);
                 }
             } else {
                 if(token.getTokenType() == Lexer::TokenType::TOK_ARG_SEPERATOR) {
                     if(paren_state.top() < 0) {
                         // TODO: Raise error
                     }

                     Common::Log::debug("Argument seperator found. Acting like )");
                     while(!operators.empty() &&
                           operators.top().getTokenType() != Lexer::TokenType::TOK_START_OPERATOR)
                     {
                         output.push_back(operators.top());
                         operators.pop();
                     }
                 } else {
                     // Any old identifier
                     output.push_back(token);
                 }
            }

            last_token = token;
        }, *iter.raw());

    }

    while(!operators.empty()) {
        output.push_back(operators.top());
        operators.pop();
    }

    return parseExpressionImpl(output.begin(), output.end());
}

auto ISL::Parser::parseIdentifier(TokenListIter& iter, const TokenListIter& end)
    -> std::optional<AST::IdentifierExpressionPtr>
{
    return parseIdentifierImpl(*iter.raw());
}

auto ISL::Parser::parseValue(TokenListIter& iter, const TokenListIter& end)
    -> std::optional<AST::ConstantExpressionPtr>
{
    return std::visit([](auto&& value) -> std::optional<AST::ConstantExpressionPtr> {
        using T = std::decay_t<decltype(value)>;
        if constexpr(std::is_same_v<T, Lexer::ValueToken>) {
            return parseValue(value.getValueType());
        } else {
            return std::nullopt;
        }
    }, *iter.raw());
}

auto ISL::Parser::parseValue(const Common::BuiltinType& value)
    -> std::optional<AST::ConstantExpressionPtr>
{
    return std::visit([&](auto&& value)
        -> std::optional<AST::ConstantExpressionPtr>
    {
        using V = std::decay_t<decltype(value)>;

        if constexpr(std::is_same_v<V, Common::BoolType>) {
            return std::make_unique<AST::BoolExpression>(value);
        } else if constexpr(std::is_same_v<V, Common::CharType>) {
            return std::make_unique<AST::CharacterExpression>(value);
        // } else if constexpr(std::is_same_v<V, Common::UCharType>) {
        } else if constexpr(std::is_same_v<V, Common::IntType>) {
            return std::make_unique<AST::IntegerExpression>(value);
        // } else if constexpr(std::is_same_v<V, Common::UIntType>) {
        } else if constexpr(std::is_same_v<V, Common::FloatType>) {
            return std::make_unique<AST::FloatExpression>(value);
        } else if constexpr(std::is_same_v<V, Common::DoubleType>) {
            return std::make_unique<AST::DoubleExpression>(value);
        } else if constexpr(std::is_same_v<V, Common::StringType>) {
            return std::make_unique<AST::StringExpression>(value);
        } else {
            Common::Log::error("Unhandled type!");
            error();
            return std::nullopt;
        }
    }, value);
}

auto ISL::Parser::parseTypeExpression(TokenListIter iter,
                                      TokenListIter end)
    -> std::optional<AST::TypeExpressionPtr>
{
    AST::TypeExpressionPtr type = nullptr;
    std::deque<AST::TypeExpressionPtr> types;

    for(auto i = iter; i != end; ++i) {
        if(!std::visit([&](auto&& token) -> bool {
            using T = std::decay_t<decltype(token)>;

            if constexpr(std::is_same_v<T, Lexer::IdentifierToken>) {
                const auto& iden = token.getIdentifier();
                if(iden == "typeof") {
                    Common::Log::debug("typeof expression!");
                    if((i + 1)->getTokenType() != Lexer::TokenType::TOK_START_OPERATOR)
                        return false;

                    size_t p = 1;
                    auto end_paren = std::find_if((i + 2).raw(), end.raw(), [&p](auto& tok) {
                        p += std::visit([](auto&& t) {
                                if(t.getTokenType() == Lexer::TokenType::TOK_START_OPERATOR)
                                    return 1;
                                else if(t.getTokenType() == Lexer::TokenType::TOK_END_OPERATOR)
                                    return -1;
                                else
                                    return 0;
                             }, tok);

                        if(p == 0)
                           return true;
                        else
                           return false;
                    });

                    auto next = i + 1;
                    AST::ExpressionPtr typeof_expr;
                    if(auto expr = parseExpression(next, end_paren); expr)
                        typeof_expr = std::move(expr.value());
                    else
                        typeof_expr = std::make_unique<AST::EmptyExpression>();

                    // type = std::make_unique<AST::TypeOfExpression>(std::move(typeof_expr),
                    //                                                std::move(type));
                    types.push_front(std::make_unique<AST::TypeOfExpression>(std::move(typeof_expr)));
                } else {
                    AST::IdentifierExpressionPtr iden_expr;
                    if(auto expr = parseIdentifierImpl(token); expr) {
                        iden_expr = std::move(expr.value());
                    } else {
                        // TODO: Error checking
                    }

                    // type = std::make_unique<AST::SimpleTypeExpression>(std::move(iden_expr),
                    //                                                    std::move(type));
                    types.push_front(std::make_unique<AST::SimpleTypeExpression>(std::move(iden_expr)));
                }
            } else {
                Common::Log::error("Type Expressions that are not typeof expressions may only be formed of identifiers.");
                return false;
            }

            return true;
        }, *i.raw()))
        {
            break;
        }
    }

    if(types.empty()) {
        return std::nullopt;
    }
    type = std::move(types.back());
    types.pop_back();
    for(auto* last_type = type.get(); !types.empty(); types.pop_back()) {
        last_type->setSubType(std::move(types.back()));
        last_type = last_type->getSubType().get();
    }

    if(type != nullptr)
        return type;
    else
        return std::nullopt;
}

auto ISL::Parser::parseParameterExpression(TokenListIter& iter,
                                           const TokenListIter& end,
                                           bool require_param_names)
    -> std::optional<AST::ParameterExpressionPtr>
{
    if(require_param_names && std::distance(iter, end) <= 1) {
        Common::Log::error("Parameters need at least 2 tokens");
        return std::nullopt;
    }

    {
        std::stringstream ss;
        ss << "Parameter is: ";
        for(auto it = iter; it != end; ++it)
            std::visit([&ss](auto&& t) {
                using T = std::decay_t<decltype(t)>;

                ss << "[" << t.getTokenType() << "]";
                if constexpr(std::is_same_v<T, ISL::Lexer::FuncArgEndToken>) {
                    ss << "(" << t.getParameterCount() << " params)";
                } else if constexpr(std::is_same_v<T, ISL::Lexer::OperatorToken>) {
                    ss << "(" << t.getOperator() << ")";
                } else if constexpr(std::is_same_v<T, ISL::Lexer::IdentifierToken>) {
                    ss << "(" << t.getIdentifier() << ")";
                } else if constexpr(std::is_same_v<T, ISL::Lexer::ValueToken>) {
                    std::visit([&ss](auto&& v) {
                        ss << "(" << v << ")";
                    }, t.getValueType());
                }

                ss << " => ";
            }, *it.raw());
        ss << " DONE";
        ISL::Common::Log::debug(ss.str());
    }

    if(require_param_names) {
        TokenListIter last_token(iter);
        for(; last_token != end && std::distance(last_token, end) > 1; ++last_token);

        AST::TypeExpressionPtr type;
        if(auto t = parseTypeExpression(iter, last_token); t) {
            type = std::move(t.value());
        } else {
            Common::Log::error("Failed to parse type.");
            return std::nullopt;
        }

        AST::IdentifierExpressionPtr iden;
        if(auto iden_opt = parseIdentifier(last_token, end); iden_opt)
            iden = std::move(iden_opt.value());
        else {
            Common::Log::error("Failed to parse identifier");
            return std::nullopt;
        }

        iter = end;

        return std::make_unique<AST::ParameterExpression>(std::move(type),
                                                          std::move(iden));
    } else {
        AST::TypeExpressionPtr type;
        if(auto t = parseTypeExpression(iter, end); t) {
            type = std::move(t.value());
        } else {
            Common::Log::error("Failed to parse type.");
            return std::nullopt;
        }

        AST::IdentifierExpressionPtr iden = std::make_unique<AST::IdentifierExpression>("");

        iter = end;

        return std::make_unique<AST::ParameterExpression>(std::move(type),
                                                          std::move(iden));

    }
}

auto ISL::Parser::parseParameterExpression(TokenListIter& iter,
                                           const TokenListIter& end)
    -> std::optional<AST::ParameterExpressionPtr>
{
    return parseParameterExpression(iter, end, true);
}

