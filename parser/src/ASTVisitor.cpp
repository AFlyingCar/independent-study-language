#include "ASTVisitor.h"

#define DEF_VISIT_EXPRESSION(EXPR)                                       \
    bool ISL::Parser::ASTVisitor::visit##EXPR (const AST:: EXPR &) { \
        return true;                                                 \
    }

#define DEF_TRAVERSE_EXPRESSION(EXPR, BODY)                                           \
    bool ISL::Parser::ASTVisitor::traverse##EXPR (const std::unique_ptr<AST:: EXPR > & expr) { \
        if(visit##EXPR (*expr)) {                                                 \
            BODY                                                                  \
            return true;                                                          \
        }                                                                         \
        return false;                                                             \
    }

#define TRAVERSE_SUB_EXPR(EXPR, VALUE, NAME)        \
    do {                                            \
        traverseSubExprStart(NAME);                 \
        if(!traverse##EXPR ( VALUE )) return false; \
        traverseSubExprEnd(NAME);                   \
    } while(0)

#define TRAVERSE_SUB_EXPR_GROUP(EXPR, GROUP, NAME)       \
    do {                                                 \
        m_group_sizes.push(GROUP .size());               \
        traverseSubExprGroupStart(NAME);                 \
        size_t i = 0;                                    \
        m_traversal_state.push({i, GROUP});              \
        for(auto&& e : GROUP ) {                         \
            traverseSubExprStart(std::to_string(i + 1)); \
            if(!traverse##EXPR ( e )) return false;      \
            traverseSubExprEnd(std::to_string(i + 1));   \
            ++i;                                         \
        }                                                \
        m_group_sizes.pop();                             \
        m_traversal_state.pop();                         \
        traverseSubExprGroupEnd(NAME);                   \
    } while(0)

bool ISL::Parser::ASTVisitor::traverseAST(const std::vector<AST::ExpressionPtr>& expressions)
{
    size_t i = 0;
    m_traversal_state.push({i, expressions});
    for(auto&& expr : expressions) {
        TRAVERSE_SUB_EXPR(Expression, expr, std::to_string(i + 1));
        ++i;
    }
    m_traversal_state.pop();

    traversalFinished();

    return true;
}

bool ISL::Parser::ASTVisitor::traverseExpression(const AST::ExpressionPtr& expr) {
#define X(EXPR)                                              \
    if(auto* e = dynamic_cast<AST:: EXPR*>(expr.get()); e) { \
        auto ep = std::unique_ptr<AST:: EXPR>(e);            \
        auto v = traverse##EXPR ( ep );                      \
        ep.release();                                        \
        return v;                                            \
    } else

    AST_EXPRESSIONS()
        return false;

#undef X

    return true;
}

bool ISL::Parser::ASTVisitor::visitExpression(const AST::Expression& expr) {
#define X(EXPR)                                         \
    if(auto* e = dynamic_cast<const AST:: EXPR*>(&expr); e) { \
        auto v = visit##EXPR ( *e );                    \
        return v;                                       \
    } else

    AST_EXPRESSIONS()
        return false;

#undef X

    return true;
}

DEF_TRAVERSE_EXPRESSION(ConstantExpression, { })
DEF_TRAVERSE_EXPRESSION(IdentifierExpression, { })

DEF_TRAVERSE_EXPRESSION(BinaryExpression, { })

DEF_TRAVERSE_EXPRESSION(UnaryExpression, { })

DEF_TRAVERSE_EXPRESSION(CallExpression, { })

DEF_TRAVERSE_EXPRESSION(WhileExpression, {
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(DoWhileExpression, {
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(IfExpression, {
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(ElseIfExpression, {
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(ElseExpression,{
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(ForExpression, {
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(FunctionDefinitionExpression, {
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(ImportExpression, { })

DEF_TRAVERSE_EXPRESSION(ExternExpression, { })

DEF_TRAVERSE_EXPRESSION(LoopExpression, {
    TRAVERSE_SUB_EXPR_GROUP(Expression, expr->getBody(), "body");
})

DEF_TRAVERSE_EXPRESSION(ReturnExpression, { })

DEF_TRAVERSE_EXPRESSION(StructureExpression, {
    // TODO
#if 0
    TRAVERSE_SUB_EXPR_GROUP(VariableExpression, expr->getMembers(), "members");
#endif
})

DEF_TRAVERSE_EXPRESSION(VariableExpression, { })

bool ISL::Parser::ASTVisitor::traverseTypeExpression(const std::unique_ptr<AST::TypeExpression>& expr) {
    bool res = false;
    if(auto* e = dynamic_cast<AST::SimpleTypeExpression*>(expr.get()); e) {
        auto ep = std::unique_ptr<AST::SimpleTypeExpression>(e);
        res = traverseSimpleTypeExpression(ep);
        ep.release();
    } else if(auto* e = dynamic_cast<AST::TypeOfExpression*>(expr.get()); e) {
        auto ep = std::unique_ptr<AST::TypeOfExpression>(e);
        res = traverseTypeOfExpression(ep);
        ep.release();
    }

    return res;
}

DEF_TRAVERSE_EXPRESSION(SimpleTypeExpression, {
    TRAVERSE_SUB_EXPR(IdentifierExpression, expr->getTypeIdentifier(), "iden");
})

DEF_TRAVERSE_EXPRESSION(TypeOfExpression, {
    TRAVERSE_SUB_EXPR(Expression, expr->getExpression(), "expr");
})

DEF_TRAVERSE_EXPRESSION(EmptyExpression, { })

DEF_TRAVERSE_EXPRESSION(ParameterExpression, { })

DEF_VISIT_EXPRESSION(ConstantExpression)
DEF_VISIT_EXPRESSION(BinaryExpression)
DEF_VISIT_EXPRESSION(UnaryExpression)
DEF_VISIT_EXPRESSION(CallExpression)
DEF_VISIT_EXPRESSION(WhileExpression)
DEF_VISIT_EXPRESSION(DoWhileExpression)
DEF_VISIT_EXPRESSION(IfExpression)
DEF_VISIT_EXPRESSION(ElseIfExpression)
DEF_VISIT_EXPRESSION(ElseExpression)
DEF_VISIT_EXPRESSION(ForExpression)
DEF_VISIT_EXPRESSION(FunctionDefinitionExpression)
DEF_VISIT_EXPRESSION(ImportExpression)
DEF_VISIT_EXPRESSION(ExternExpression)
DEF_VISIT_EXPRESSION(LoopExpression)
DEF_VISIT_EXPRESSION(ReturnExpression)
DEF_VISIT_EXPRESSION(StructureExpression)
DEF_VISIT_EXPRESSION(VariableExpression)
DEF_VISIT_EXPRESSION(IdentifierExpression)
DEF_VISIT_EXPRESSION(SimpleTypeExpression)
DEF_VISIT_EXPRESSION(TypeOfExpression)
DEF_VISIT_EXPRESSION(EmptyExpression)
DEF_VISIT_EXPRESSION(ParameterExpression)

bool ISL::Parser::ASTVisitor::visitTypeExpression(const AST::TypeExpression& expr) {
    bool res = false;
    if(auto* e = dynamic_cast<const AST::SimpleTypeExpression*>(&expr); e) {
        res = visitSimpleTypeExpression(*e);
    } else if(auto* e = dynamic_cast<const AST::TypeOfExpression*>(&expr); e) {
        res = visitTypeOfExpression(*e);
    }

    return res;
}

void ISL::Parser::ASTVisitor::traversalFinished() { }

void ISL::Parser::ASTVisitor::traverseSubExprStart(const std::string&) { }
void ISL::Parser::ASTVisitor::traverseSubExprEnd(const std::string&) { }

void ISL::Parser::ASTVisitor::traverseSubExprGroupStart(const std::string&) { }
void ISL::Parser::ASTVisitor::traverseSubExprGroupEnd(const std::string&) { }

size_t ISL::Parser::ASTVisitor::getSizeOfGroup() const {
    return m_group_sizes.empty() ? 0 : m_group_sizes.top();
}

auto ISL::Parser::ASTVisitor::getTraversalState() const -> const TraversalState&
{
    return m_traversal_state.top();
}

