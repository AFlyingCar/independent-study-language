
#include "details/ScopeHandling.h"

#include "Logger.h"

auto ISL::Parser::details::getInserterFunctions()
    -> std::stack<InserterFunctionCallbackType>&
{
    static std::stack<InserterFunctionCallbackType> inserterFunction;

    return inserterFunction;
}


void ISL::Parser::details::setExpressionInserterFunction(const InserterFunctionCallbackType& callback)
{
    Common::Log::debug("setExpressionInserterFunction()");

    bool has_been_called = false;
    getInserterFunctions().push([callback, has_been_called](ISL::Parser::AST::ExpressionPtr& expr) mutable -> bool
    {
        if(has_been_called) {
            return callback(expr);
        } else {
            has_been_called = true;

            // Handle the case where this is the first time we are getting
            //  called, but we need to be placed in the preceding scope
            if(!getInserterFunctions().empty()) {
                auto t = getInserterFunctions().top();
                getInserterFunctions().pop();
                auto res = getInserterFunctions().top()(expr);
                getInserterFunctions().push(t);
                return res;
            }

            return false;
        }
    });
}

