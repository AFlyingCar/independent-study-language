
#include "ast/LoopExpression.h"

ISL::Parser::AST::LoopExpression::LoopExpression(): m_body()
{ }

auto ISL::Parser::AST::LoopExpression::getBody() const
    -> const std::vector<ExpressionPtr>&
{
    return m_body;
}

auto ISL::Parser::AST::LoopExpression::getBody()
    -> std::vector<ExpressionPtr>&
{
    return m_body;
}

void ISL::Parser::AST::LoopExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;
    stream << "\"type\": \"loop\"," << std::endl;

    stream << "\"body\": [\n";
    auto i = 0;
    for(auto&& expr : m_body) {
        expr->toJSON(stream);
        ++i;
        if(i < m_body.size())
            stream << ",\n";
    }
    stream << "\n]\n";

    stream << std::endl << "}" << std::endl;
}


