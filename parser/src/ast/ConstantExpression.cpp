
#include "ast/ConstantExpression.h"

/**
 * @brief Constructs the ConstantExpression
 *
 * @param value The value.
 */
ISL::Parser::AST::ConstantExpression::ConstantExpression(const Common::BuiltinType& value):
    m_value(value)
{ }

/**
 * @brief Gets the value.
 *
 * @return The value.
 */
ISL::Common::BuiltinType ISL::Parser::AST::ConstantExpression::getValue() const {
    return m_value;
}

void ISL::Parser::AST::ConstantExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"" << Common::typeToString(m_value) << "\"," << std::endl;
    stream << "\"value\": " << m_value << std::endl;

    stream << "}" << std::endl;
}
