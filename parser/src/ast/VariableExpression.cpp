#include "ast/VariableExpression.h"

ISL::Parser::AST::VariableExpression::VariableExpression(TypeExpressionPtr&& type_expr,
                                                         IdentifierExpressionPtr&& var_iden,
                                                         ExpressionPtr&& val):
    m_type_expr(std::move(type_expr)),
    m_var_iden(std::move(var_iden)),
    m_val(std::move(val))
{ }

auto ISL::Parser::AST::VariableExpression::getTypeExpression() const
    -> const TypeExpressionPtr&
{
    return m_type_expr;
}

auto ISL::Parser::AST::VariableExpression::getVarIdentifier() const
    -> const IdentifierExpressionPtr&
{
    return m_var_iden;
}

auto ISL::Parser::AST::VariableExpression::getValueExpr() const
    -> const ExpressionPtr&
{
    return m_val;
}

void ISL::Parser::AST::VariableExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"var\"," << std::endl;

    stream << "\"type_expr\": ";
    m_type_expr->toJSON(stream);

    stream << ",\n\"var_iden\": ";
    m_var_iden->toJSON(stream);

    stream << ",\n\"value\": ";
    m_val->toJSON(stream);

    stream << "}" << std::endl;
}

