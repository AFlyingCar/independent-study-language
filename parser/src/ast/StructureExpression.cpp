#include "ast/StructureExpression.h"

ISL::Parser::AST::StructureExpression::StructureExpression(IdentifierExpressionPtr&& iden):
    m_iden(std::move(iden)),
    m_members()
{ }

auto ISL::Parser::AST::StructureExpression::getIdentifier() const -> const IdentifierExpressionPtr&
{
    return m_iden;
}

auto ISL::Parser::AST::StructureExpression::getMembers() const -> const std::vector<VariableExpressionPtr>&
{
    return m_members;
}

auto ISL::Parser::AST::StructureExpression::getMembers() -> std::vector<VariableExpressionPtr>&
{
    return m_members;
}

void ISL::Parser::AST::StructureExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"struct_def\"," << std::endl;
    stream << "\"iden\": ";
    m_iden->toJSON(stream);

    stream << ",\n\"members\": [\n";
    int i = 0;
    for(auto&& expr : m_members) {
        expr->toJSON(stream);

        ++i;
        if(i < m_members.size())
            stream << ",\n";
    }
    stream << "\n]\n";

    stream << std::endl << "}" << std::endl;
}

