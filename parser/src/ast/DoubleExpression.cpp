
#include "ast/DoubleExpression.h"

/**
 * @brief Constructs the DoubleExpression
 *
 * @param value The value.
 */
ISL::Parser::AST::DoubleExpression::DoubleExpression(double value):
    ConstantExpression(value)
{ }
