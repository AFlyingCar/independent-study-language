
#include "ast/BoolExpression.h"

/**
 * @brief Constructs a BoolExpression
 *
 * @param value The value
 */
ISL::Parser::AST::BoolExpression::BoolExpression(bool value):
	ConstantExpression(value)
{ }
