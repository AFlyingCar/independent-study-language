#include "ast/FunctionDefinitionExpression.h"

ISL::Parser::AST::FunctionDefinitionExpression::FunctionDefinitionExpression(IdentifierExpressionPtr&& func_iden,
                                                                             std::vector<ParameterExpressionPtr>&& param_list,
                                                                             TypeExpressionPtr&& return_type):
    m_func_iden(std::move(func_iden)),
    m_parameter_list(std::move(param_list)),
    m_return_type(std::move(return_type)),
    m_body()
{ }

auto ISL::Parser::AST::FunctionDefinitionExpression::getFunctionIdentifier() const
    -> const IdentifierExpressionPtr&
{
    return m_func_iden;
}

auto ISL::Parser::AST::FunctionDefinitionExpression::getParameterList() const
    -> const std::vector<ParameterExpressionPtr>&
{
    return m_parameter_list;
}

auto ISL::Parser::AST::FunctionDefinitionExpression::getReturnType() const
    -> const TypeExpressionPtr&
{
    return m_return_type;
}

auto ISL::Parser::AST::FunctionDefinitionExpression::getBody() const
    -> const std::vector<ExpressionPtr>&
{
    return m_body;
}

auto ISL::Parser::AST::FunctionDefinitionExpression::getBody()
    -> std::vector<ExpressionPtr>&
{
    return m_body;
}

void ISL::Parser::AST::FunctionDefinitionExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"func_def\"," << std::endl;
    stream << "\"iden\": ";
    m_func_iden->toJSON(stream);

    stream << ",\n\"param_list\": [\n";
    auto i = 0;
    for(auto&& expr : m_parameter_list) {
        expr->toJSON(stream);

        ++i;
        if(i < m_parameter_list.size())
            stream << ",\n";
    }

    stream << "],\n\"return_type\": ";
    m_return_type->toJSON(stream);

    stream << ",\n\"body\": [\n";
    i = 0;
    for(auto&& expr : m_body) {
        expr->toJSON(stream);

        ++i;
        if(i < m_body.size())
            stream << ",\n";
    }
    stream << "\n]\n";

    stream << std::endl << "}" << std::endl;
}

