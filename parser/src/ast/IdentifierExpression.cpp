
#include "ast/IdentifierExpression.h"

ISL::Parser::AST::IdentifierExpression::IdentifierExpression(const std::string& identifier):
    m_identifier(identifier)
{ }

const std::string& ISL::Parser::AST::IdentifierExpression::getIdentifier() const {
    return m_identifier;
}

void ISL::Parser::AST::IdentifierExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"identifier\"," << std::endl;
    stream << "\"iden\": \"" << m_identifier << "\"" << std::endl;

    stream << "}" << std::endl;
}

