
#include "ast/ForExpression.h"

ISL::Parser::AST::ForExpression::ForExpression(VariableExpressionPtr&& var_expr,
                                               ExpressionPtr&& cond_expr,
                                               ExpressionPtr&& inc_expr):
    m_var_expr(std::move(var_expr)),
    m_cond_expr(std::move(cond_expr)),
    m_inc_expr(std::move(inc_expr)),
    m_body()
{ }

auto ISL::Parser::AST::ForExpression::getVarExpression() const
    -> const VariableExpressionPtr&
{
    return m_var_expr;
}

auto ISL::Parser::AST::ForExpression::getCondExpression() const
    -> const ExpressionPtr&
{
    return m_cond_expr;
}

auto ISL::Parser::AST::ForExpression::getIncExpression() const
    -> const ExpressionPtr&
{
    return m_inc_expr;
}

auto ISL::Parser::AST::ForExpression::getBody() const
    -> const std::vector<ExpressionPtr>&
{
    return m_body;
}

auto ISL::Parser::AST::ForExpression::getBody()
    -> std::vector<ExpressionPtr>&
{
    return m_body;
}

void ISL::Parser::AST::ForExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"for\"," << std::endl;
    stream << "\"var_expr\": ";
    m_var_expr->toJSON(stream);

    stream << ",\n\"cond_expr\": ";
    m_cond_expr->toJSON(stream);

    stream << ",\n\"inc_expr\": ";
    m_inc_expr->toJSON(stream);

    stream << ",\n\"body\": [\n";
    auto i = 0;
    for(auto&& expr : m_body) {
        expr->toJSON(stream);

        ++i;
        if(i < m_body.size())
            stream << ",\n";
    }
    stream << "\n]\n";

    stream << std::endl << "}" << std::endl;
}

