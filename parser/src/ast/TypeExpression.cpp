
#include "ast/TypeExpression.h"

ISL::Parser::AST::TypeExpression::TypeExpression(): m_sub_type(nullptr) {
}

ISL::Parser::AST::TypeExpression::TypeExpression(TypeExpressionPtr&& sub_type):
    m_sub_type(std::move(sub_type))
{ }

auto ISL::Parser::AST::TypeExpression::getSubType() const
    -> const TypeExpressionPtr&
{
    return m_sub_type;
}

auto ISL::Parser::AST::TypeExpression::getSubType() -> TypeExpressionPtr& {
    return m_sub_type;
}

void ISL::Parser::AST::TypeExpression::setSubType(TypeExpressionPtr&& sub_type)
{
    m_sub_type = std::move(sub_type);
}

ISL::Parser::AST::SimpleTypeExpression::SimpleTypeExpression(IdentifierExpressionPtr&& type_iden):
    TypeExpression(), m_type_iden(std::move(type_iden))
{ }

ISL::Parser::AST::SimpleTypeExpression::SimpleTypeExpression(IdentifierExpressionPtr&& type_iden,
                                                             TypeExpressionPtr&& sub_type):
    TypeExpression(std::forward<TypeExpressionPtr>(sub_type)),
    m_type_iden(std::move(type_iden))
{ }

auto ISL::Parser::AST::SimpleTypeExpression::getTypeIdentifier() const
    -> const IdentifierExpressionPtr&
{
    return m_type_iden;
}

void ISL::Parser::AST::SimpleTypeExpression::toJSON(std::ostream& stream) {
    stream << "{";

    stream << "\"type\": \"SimpleType\",\n";
    stream << "\"type_value\": \"" << m_type_iden->getIdentifier() << "\"\n";
    if(getSubType() != nullptr) {
        stream << ",\"sub_type\": ";
        getSubType()->toJSON(stream);
        stream << ",\n";
    }

    stream << "}";
}

ISL::Parser::AST::TypeOfExpression::TypeOfExpression(ExpressionPtr&& expr):
    TypeExpression(), m_expr(std::move(expr))
{ }

ISL::Parser::AST::TypeOfExpression::TypeOfExpression(ExpressionPtr&& expr,
                                                     TypeExpressionPtr&& sub_type):
    TypeExpression(std::forward<TypeExpressionPtr>(sub_type)),
    m_expr(std::move(expr))
{ }

auto ISL::Parser::AST::TypeOfExpression::getExpression() const
    -> const ExpressionPtr&
{
    return m_expr;
}

void ISL::Parser::AST::TypeOfExpression::toJSON(std::ostream& stream) {
    stream << "{";

    stream << "\"type\": \"TypeOf\",\n";
    stream << "\"type_value\": ";
    m_expr->toJSON(stream);
    if(getSubType() != nullptr) {
        stream << "\"sub_type\": ";
        getSubType()->toJSON(stream);
        stream << ",\n";
    }

    stream << "\n}";
}

