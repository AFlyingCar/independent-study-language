#include "ast/BinaryExpression.h"

ISL::Parser::AST::BinaryExpression::BinaryExpression(const Common::Operator& op,
                                                     ExpressionPtr lhs,
                                                     ExpressionPtr rhs):
    m_operator(op),
    m_lhs(std::move(lhs)),
    m_rhs(std::move(rhs))
{ }

auto ISL::Parser::AST::BinaryExpression::getOperator() const
    -> const Common::Operator&
{
    return m_operator;
}

auto ISL::Parser::AST::BinaryExpression::getLHS() const
    -> const ExpressionPtr&
{
    return m_lhs;
}

auto ISL::Parser::AST::BinaryExpression::getRHS() const
    -> const ExpressionPtr&
{
    return m_rhs;
}

auto ISL::Parser::AST::BinaryExpression::getOperator() -> Common::Operator& {
    return m_operator;
}

auto ISL::Parser::AST::BinaryExpression::getLHS() -> ExpressionPtr& {
    return m_lhs;
}

auto ISL::Parser::AST::BinaryExpression::getRHS() -> ExpressionPtr& {
    return m_rhs;
}

void ISL::Parser::AST::BinaryExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"BinaryExpr\"," << std::endl;
    stream << "\"lhs\": ";
    m_lhs->toJSON(stream);

    stream << ",\n\"rhs\": ";
    m_rhs->toJSON(stream);

    stream << ",\n\"op\": \"";
    stream << m_operator << "\"";

    stream << std::endl << "}" << std::endl;
}

