#include "ast/ExternExpression.h"

ISL::Parser::AST::ExternExpression::ExternExpression(ParameterExpressionPtr&& var):
    m_symbol(std::move(var))
{ }

ISL::Parser::AST::ExternExpression::ExternExpression(FunctionDefinitionExpressionPtr&& func):
    m_symbol(std::move(func))
{ }

auto ISL::Parser::AST::ExternExpression::getSymbol() const
    -> const ExternVariant&
{
    return m_symbol;
}

void ISL::Parser::AST::ExternExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"extern\"" << std::endl;

    stream << "}" << std::endl;
}

