#include "ast/ParameterExpression.h"

ISL::Parser::AST::ParameterExpression::ParameterExpression(TypeExpressionPtr&& type_iden,
                                                           IdentifierExpressionPtr&& var_iden):
    m_type_iden(std::move(type_iden)),
    m_var_iden(std::move(var_iden))
{ }

auto ISL::Parser::AST::ParameterExpression::getTypeIdentifier() const
    -> const TypeExpressionPtr&
{
    return m_type_iden;
}

auto ISL::Parser::AST::ParameterExpression::getVarIdentifier() const
    -> const IdentifierExpressionPtr&
{
    return m_var_iden;
}


void ISL::Parser::AST::ParameterExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"param\"," << std::endl;

    stream << "\"type_iden\": ";
    m_type_iden->toJSON(stream);

    stream << ",\n\"var_iden\": ";
    m_var_iden->toJSON(stream);

    stream << "}" << std::endl;
}

