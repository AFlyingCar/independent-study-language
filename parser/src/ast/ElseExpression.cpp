
#include "ast/ElseExpression.h"

ISL::Parser::AST::ElseExpression::ElseExpression(): m_body()
{ }

auto ISL::Parser::AST::ElseExpression::getBody() const
    -> const std::vector<ExpressionPtr>&
{
    return m_body;
}

auto ISL::Parser::AST::ElseExpression::getBody()
    -> std::vector<ExpressionPtr>&
{
    return m_body;
}

void ISL::Parser::AST::ElseExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;
    stream << "\"type\": \"else\"," << std::endl;

    stream << "\"body\": [\n";
    auto i = 0;
    for(auto&& expr : m_body) {
        expr->toJSON(stream);
        ++i;
        if(i < m_body.size())
            stream << ",\n";
    }
    stream << "\n]\n";

    stream << std::endl << "}" << std::endl;
}

