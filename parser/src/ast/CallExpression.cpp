
#include "ast/CallExpression.h"

ISL::Parser::AST::CallExpression::CallExpression(const std::string& identifier,
                                                 ArgumentList&& args):
    m_arguments(std::move(args)),
    m_identifier(identifier)
{
}

auto ISL::Parser::AST::CallExpression::getArgumentList() const
    -> const ArgumentList&
{
    return m_arguments;
}

const std::string& ISL::Parser::AST::CallExpression::getIdentifier() const {
    return m_identifier;
}

void ISL::Parser::AST::CallExpression::toJSON(std::ostream& stream) {
    stream << "{\n";

    stream << "\"type\": \"Function\",\n";
    stream << "\"iden\": \"" << getIdentifier() << "\",\n";
    stream << "\"args\": [\n";

    size_t i = 0;
    for(auto& arg : m_arguments) {
        arg->toJSON(stream);

        if(i != m_arguments.size() - 1)
            stream << ",";
        ++i;
    }

    stream << "]\n";

    stream << "}";
}

