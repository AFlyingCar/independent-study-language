
#include "ast/CharacterExpression.h"

/**
 * @brief Constructs the CharacterExpression
 *
 * @param value The value.
 */
ISL::Parser::AST::CharacterExpression::CharacterExpression(char value):
    ConstantExpression(value)
{ }
