
#include "ast/UnaryExpression.h"

ISL::Parser::AST::UnaryExpression::UnaryExpression(const Common::Operator& op,
                                                   ExpressionPtr operand):
    m_operator(op), m_operand(std::move(operand))
{ }

auto ISL::Parser::AST::UnaryExpression::getOperator() const
    -> const Common::Operator&
{
    return m_operator;
}

auto ISL::Parser::AST::UnaryExpression::getOperand() const
    -> const ExpressionPtr&
{
    return m_operand;
}

void ISL::Parser::AST::UnaryExpression::toJSON(std::ostream& stream) {
    stream << "{";

    stream << "\"type\": \"UnaryExpr\",\n";

    stream << "\"rhs\": ";
    m_operand->toJSON(stream);

    stream << ",\n\"op\": \"" << m_operator << "\"\n";

    stream << "}";
}

