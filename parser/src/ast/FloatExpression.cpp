
#include "ast/FloatExpression.h"

/**
 * @brief Constructs the FloatExpression
 *
 * @param value The value.
 */
ISL::Parser::AST::FloatExpression::FloatExpression(float value):
    ConstantExpression(value)
{ }
