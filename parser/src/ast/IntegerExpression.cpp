
#include "ast/IntegerExpression.h"

/**
 * @brief Constructs the IntegerExpression
 *
 * @param value The value.
 */
ISL::Parser::AST::IntegerExpression::IntegerExpression(int value):
    ConstantExpression(value)
{ }

