#include "ast/ImportExpression.h"

ISL::Parser::AST::ImportExpression::ImportExpression(ConstantExpressionPtr&& value):
    m_value(std::move(value))
{ }

auto ISL::Parser::AST::ImportExpression::getValue() const
    -> const ConstantExpressionPtr&
{
    return m_value;
}

void ISL::Parser::AST::ImportExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"import\"" << std::endl;

    stream << ",\n\"value\": ";
    m_value->toJSON(stream);

    stream << "}" << std::endl;
}

