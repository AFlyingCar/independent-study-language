
#include "ast/WhileExpression.h"

ISL::Parser::AST::WhileExpression::WhileExpression(ExpressionPtr&& cond_expr):
    m_cond_expr(std::move(cond_expr)),
    m_body()
{ }

auto ISL::Parser::AST::WhileExpression::getCondExpression() const
    -> const ExpressionPtr&
{
    return m_cond_expr;
}

auto ISL::Parser::AST::WhileExpression::getBody() const
    -> const std::vector<ExpressionPtr>&
{
    return m_body;
}

auto ISL::Parser::AST::WhileExpression::getBody()
    -> std::vector<ExpressionPtr>&
{
    return m_body;
}

void ISL::Parser::AST::WhileExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;
    stream << "\"type\": \"while\"," << std::endl;

    stream << "\"cond_expr\": ";
    m_cond_expr->toJSON(stream);

    stream << ",\n\"body\": [\n";
    auto i = 0;
    for(auto&& expr : m_body) {
        expr->toJSON(stream);
        ++i;
        if(i < m_body.size())
            stream << ",\n";
    }
    stream << "\n]\n";

    stream << std::endl << "}" << std::endl;
}

