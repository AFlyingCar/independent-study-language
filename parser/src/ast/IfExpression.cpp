
#include "ast/IfExpression.h"

ISL::Parser::AST::IfExpression::IfExpression(ExpressionPtr&& cond_expr):
    m_cond_expr(std::move(cond_expr)),
    m_body()
{ }

auto ISL::Parser::AST::IfExpression::getCondExpression() const
    -> const ExpressionPtr&
{
    return m_cond_expr;
}

auto ISL::Parser::AST::IfExpression::getBody() const
    -> const std::vector<ExpressionPtr>&
{
    return m_body;
}

auto ISL::Parser::AST::IfExpression::getBody()
    -> std::vector<ExpressionPtr>&
{
    return m_body;
}

void ISL::Parser::AST::IfExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;
    stream << "\"type\": \"if\"," << std::endl;

    stream << "\"cond_expr\": ";
    m_cond_expr->toJSON(stream);

    stream << ",\n\"body\": [\n";
    auto i = 0;
    for(auto&& expr : m_body) {
        expr->toJSON(stream);
        ++i;
        if(i < m_body.size())
            stream << ",\n";
    }
    stream << "\n]\n";

    stream << std::endl << "}" << std::endl;
}

