#include "ast/ReturnExpression.h"

ISL::Parser::AST::ReturnExpression::ReturnExpression(ExpressionPtr&& expr):
    m_expr(std::move(expr))
{ }

auto ISL::Parser::AST::ReturnExpression::getExpr() const -> const ExpressionPtr&
{
    return m_expr;
}

void ISL::Parser::AST::ReturnExpression::toJSON(std::ostream& stream) {
    stream << "{" << std::endl;

    stream << "\"type\": \"return\"" << std::endl;

    stream << ",\n\"expr\": ";
    m_expr->toJSON(stream);

    stream << "}" << std::endl;
}

