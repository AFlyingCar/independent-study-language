
#include "JSONASTPrinter.h"

std::string ISL::Parser::JSONASTPrinter::formatStartOfTraversal() {
    in_group.push(true);
    return "[";
}

std::string ISL::Parser::JSONASTPrinter::formatEndOfTraversal() {
    in_group.pop();
    return "]";
}

std::string ISL::Parser::JSONASTPrinter::formatSeperator() {
    return ",";
}

std::string ISL::Parser::JSONASTPrinter::formatNewLine() {
    return "\n";
}

std::string ISL::Parser::JSONASTPrinter::formatPropertyKey(const std::string& key)
{
    if(!in_group.top() && !key.empty()) {
        using namespace std::string_literals;
        return "\""s + key + "\":";
    } else {
        return "";
    }
}

std::string ISL::Parser::JSONASTPrinter::formatPropertyValue(const std::string& value) {
    return value;
}

std::string ISL::Parser::JSONASTPrinter::formatExprStart(const std::string&) {
    in_group.push(false);
    return "{";
}

std::string ISL::Parser::JSONASTPrinter::formatExprEnd(const std::string&) {
    in_group.pop();
    return "}";
}

std::string ISL::Parser::JSONASTPrinter::formatGroupStart(const std::string&) {
    in_group.push(true);
    return "[";
}

std::string ISL::Parser::JSONASTPrinter::formatGroupEnd(const std::string&) {
    in_group.pop();
    return "]";
}

