
#include "ErrorCount.h"

static std::size_t errors = 0;

std::size_t ISL::Parser::getErrorCount() {
    return errors;
}

void  ISL::Parser::error() {
    ++errors;
}

