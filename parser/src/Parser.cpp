
#include "Parser.h"

#include "Logger.h"
#include "Utility.h"

#include "ParseExpressions.h"
#include "ParseKeywordStatements.h"
#include "ParseVariableExpression.h"
#include "details/ScopeHandling.h"
#include "ErrorCount.h"

#include <algorithm>

std::map<std::string, ISL::Parser::ParserFunctorType>
    ISL::Parser::KEYWORD_INFO_MAPPING =
{
    { "return", parseReturnStatement },
    { "import", parseImportStatement },
    { "extern", parseExternStatement },

    { "for", parseForStatement },
    { "endfor", parseEndBlockStatement },

    { "function", parseFunctionStatement },
    { "endfunction", parseEndBlockStatement },

    { "if", parseIfStatement },
    { "elseif", parseElseIfStatement },
    { "else", parseElseStatement },
    { "endif", parseEndBlockStatement },

    { "loop", parseLoopStatement },
    { "endloop", parseEndBlockStatement },

    { "while", parseWhileStatement },
    { "endwhile", parseEndBlockStatement },

    { "dowhile", parseDoWhileStatement },
    { "enddowhile", parseEndBlockStatement },

    { "structure", parseStructureStatement },
    { "endstructure", parseEndBlockStatement },

/*
    // TODO
    { "enumeration", parseEnumerationStatement },
    { "endenumeration", parseEndBlockStatement }
*/
};

/**
 * @details <KeywordExpression> ::= dowhile ( <Expression> )
 *                               |  enddowhile;
 *                               |  enumeration <Identifier>
 *                               |  endenumeration
 */
auto ISL::Parser::parseKeywordStatement(TokenListIter& iter,
                                        const TokenListIter& end) -> AST::ExpressionPtr
{
    return std::make_unique<AST::EmptyExpression>();
}

/**
 * @brief Parses a single statement
 * @details <Statement> ::= <Expression>;
 *                       |  <KeywordExpression>;
 */
auto ISL::Parser::parseStatementImpl(TokenListIter& iter,
                                     const TokenListIter& end)
    -> AST::ExpressionPtr
{
    Common::Log::debug("Beginning parsing of single statement.");

    const auto& start_token = *iter.raw();

    using namespace std::string_literals;

    return std::visit([&](auto&& token) -> AST::ExpressionPtr {
        using T = std::decay_t<decltype(token)>;

        if constexpr(std::is_same_v<T, Lexer::IdentifierToken>) {
            const auto& identifier = token.getIdentifier();

            if(auto var_expr = parseVariableExpression(iter, end); var_expr != nullptr) {
                return var_expr;
            }

            Common::Log::debug("Token is an identifier ("s + identifier + ")");
            if(KEYWORD_INFO_MAPPING.count(identifier))
            {
                Common::Log::debug("Token is a keyword.");
                ++iter;
                return KEYWORD_INFO_MAPPING.at(identifier)(iter, end);
            }
            else
            {
                Common::Log::debug("Token is some identifier.");
                if(auto expr = parseExpression(iter, end); expr) {
                    return std::move(expr.value());
                } else {
                    error();
                    return std::make_unique<AST::EmptyExpression>();
                }
            }
        } else {
            Common::Log::debug("Token is an expression.");
            if(auto expr = parseExpression(iter, end); expr) {
                return std::move(expr.value());
            } else {
                error();
                return std::make_unique<AST::EmptyExpression>();
            }
        }
    }, start_token);
}

/**
 * @brief Finds a single statement from the token stream.
 */
auto ISL::Parser::parseStatement(TokenListIter iter, const TokenListIter& end)
    -> std::pair<AST::ExpressionPtr, TokenListIter>
{
    TokenListIter iter_es = iter;

    // Basic, naive parenthesis counting to handle things like ForStatements
    size_t paren_count = 0;

    Common::Log::debug("Beginning to search for bounds of single statement.");
    bool is_es = false;
    for(; iter_es != end && !is_es; ++iter_es) {
        auto res = std::visit([&paren_count](auto&& tok) {
            Common::Log::debug(tok.getSubstr());

            switch(tok.getTokenType()) {
                case Lexer::TokenType::TOK_EOF:
                    return -1;
                case Lexer::TokenType::TOK_END_STATEMENT:
                    Common::Log::debug("End of statement found.");
                    return paren_count > 0 ? 0 : 1;
                case Lexer::TokenType::TOK_START_OPERATOR:
                case Lexer::TokenType::TOK_START_CONSTRAINTS:
                    ++paren_count;
                    return 0;
                case Lexer::TokenType::TOK_END_CONSTRAINTS:
                case Lexer::TokenType::TOK_END_OPERATOR:
                    --paren_count;
                default:
                    return 0;
            }
        }, *iter_es.raw());

        switch(res) {
            case -1:
                // If a token is EOF, then that's not a problem unless we have
                //   a partial statement
                if(iter != iter_es) {
                    Common::Log::error("Unexpected EOF before end of statement.");
                    error();
                }
                return std::make_pair(std::make_unique<AST::EmptyExpression>(),
                                      iter_es);
            case 1: {
                auto current_errors = getErrorCount();
                auto statement = parseStatementImpl(iter, iter_es);

                // Make sure that if there were any errors that we just skip
                //  this entire statement and continue trying to parse
                if(current_errors != getErrorCount()) {
                    iter = iter_es + 1; // Don't forget to skip the ';' too
                }

                return std::make_pair(std::move(statement), iter_es);
            }
            default:
                break;
        }
    }

    Common::Log::error("Unexpected EOF.");
    error();
    return std::make_pair(std::make_unique<AST::EmptyExpression>(),
                          iter_es);
}

auto ISL::Parser::parse(const std::vector<Lexer::TokenVariant>& tokens)
    -> std::vector<AST::ExpressionPtr>
{
    std::vector<AST::ExpressionPtr> all_statements;

    details::getInserterFunctions().push([](AST::ExpressionPtr&) { return false; });

    for(TokenListIter it = tokens.begin(); it != tokens.end(); ++it) {
        auto errors = getErrorCount();
        auto [statement, end_statement] = parseStatement(it, tokens.end());

        if(errors < getErrorCount())
            return std::vector<AST::ExpressionPtr>();

        if(!details::getInserterFunctions().top()(statement))
            all_statements.push_back(std::move(statement));

        if(it == tokens.end())
            break;
        it = end_statement;
    }

    return all_statements;
}

