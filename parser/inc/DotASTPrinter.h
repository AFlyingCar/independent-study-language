#ifndef DOT_AST_PRINTER_H
#define DOT_AST_PRINTER_H

#include "ASTPrinter.h"

namespace ISL::Parser {
    class DotASTPrinter: public ASTPrinter {
        public:
            DotASTPrinter(std::ostream&);

        protected:
            virtual std::string formatProperty(const std::string&,
                                               const std::string&) override;

            virtual std::string formatStartOfTraversal() override;
            virtual std::string formatEndOfTraversal() override;
            virtual std::string formatSeperator() override;
            virtual std::string formatNewLine() override;
            virtual std::string formatPropertyKey(const std::string&) override;
            virtual std::string formatPropertyValue(const std::string&) override;
            virtual std::string formatExprStart(const std::string&) override;
            virtual std::string formatExprEnd(const std::string&) override;
            virtual std::string formatGroupStart(const std::string&) override;
            virtual std::string formatGroupEnd(const std::string&) override;

            virtual std::string indent() const override;

        private:
            struct Node {
                std::string label;
                std::vector<std::pair<std::string, std::string>> attribs;

                bool is_table;

                size_t parent;
            };

            std::string formatAttributes(const Node&) const;

            std::vector<Node> m_nodes;
            std::stack<size_t> m_parents;
            std::stack<size_t> m_groups;
            size_t m_current_node = 0;
    };
}

#endif

