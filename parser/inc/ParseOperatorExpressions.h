
#ifndef PARSE_OPERATOR_EXPRESSIONS_H
#define PARSE_OPERATOR_EXPRESSIONS_H

#include "ast/Expression.h"

#include "Token.h"

namespace ISL::Parser {
    AST::ExpressionPtr parseUnaryOperator(AST::ExpressionPtr,
                                          const Lexer::OperatorToken&);
    AST::ExpressionPtr parseBinaryOperator(AST::ExpressionPtr, AST::ExpressionPtr,
                                           const Lexer::OperatorToken&);
}

#endif

