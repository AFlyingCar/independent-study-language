
#ifndef JSON_AST_PRINTER_H
#define JSON_AST_PRINTER_H

#include "ASTPrinter.h"

namespace ISL::Parser {
    class JSONASTPrinter: public ASTPrinter {
        public:
            using ASTPrinter::ASTPrinter;

        protected:
            virtual std::string formatStartOfTraversal() override;
            virtual std::string formatEndOfTraversal() override;
            virtual std::string formatSeperator() override;
            virtual std::string formatNewLine() override;
            virtual std::string formatPropertyKey(const std::string&) override;
            virtual std::string formatPropertyValue(const std::string&) override;
            virtual std::string formatExprStart(const std::string&) override;
            virtual std::string formatExprEnd(const std::string&) override;
            virtual std::string formatGroupStart(const std::string&) override;
            virtual std::string formatGroupEnd(const std::string&) override;

        private:
            std::stack<bool> in_group;
    };
}

#endif

