
#ifndef PARSE_EXPRESSIONS_H
#define PARSE_EXPRESSIONS_H

#include <optional>

#include "ast/Expression.h"
#include "ast/IdentifierExpression.h"
#include "ast/ParameterExpression.h"
#include "ast/TypeExpression.h"
#include "ast/ConstantExpression.h"

#include "TokenList.h"

namespace ISL::Parser {
    std::optional<AST::ConstantExpressionPtr> parseValue(const Common::BuiltinType&);
    std::optional<AST::ConstantExpressionPtr> parseValue(TokenListIter&, const TokenListIter&);
    // AST::ExpressionPtr parseVariableExpression(TokenListIter&, const TokenListIter&);
    std::optional<AST::ExpressionPtr> parseExpression(TokenListIter&, const TokenListIter&);
    std::optional<AST::IdentifierExpressionPtr> parseIdentifierImpl(const Lexer::TokenVariant&);
    std::optional<AST::IdentifierExpressionPtr> parseIdentifier(TokenListIter&,
                                                                const TokenListIter&);

    std::optional<AST::TypeExpressionPtr> parseTypeExpression(TokenListIter, TokenListIter);
    std::optional<AST::ParameterExpressionPtr> parseParameterExpression(TokenListIter&,
                                                                        const TokenListIter&,
                                                                        bool = true);
    std::optional<AST::ParameterExpressionPtr> parseParameterExpression(TokenListIter&,
                                                                        const TokenListIter&);
}

#endif

