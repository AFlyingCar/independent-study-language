
#ifndef SCOPE_HANDLING_H
#define SCOPE_HANDLING_H

#include <stack>
#include <functional>

#include "ast/Expression.h"

namespace ISL::Parser::details {

    using InserterFunctionCallbackType = std::function<bool(ISL::Parser::AST::ExpressionPtr&)>;

    std::stack<InserterFunctionCallbackType>& getInserterFunctions();

    void setExpressionInserterFunction(const InserterFunctionCallbackType&);

    template<typename T>
    static void setExpressionInserterFunction(T& container) {
#if 0
        bool has_been_called = false;
        getInserterFunctions().push([&container, has_been_called](ISL::Parser::AST::ExpressionPtr& expr) mutable -> bool
        {
            if(has_been_called) {
                container.push_back(std::move(expr));
                return true;
            } else {
                has_been_called = true;
                return false;
            }
        });
#else
        setExpressionInserterFunction([&container](ISL::Parser::AST::ExpressionPtr& expr) {
            container.push_back(std::move(expr));
            return true;
        });
#endif
    }
}

#endif

