#ifndef AST_PRINTER_H
#define AST_PRINTER_H

#include <ostream>
#include <map>
#include <stack>

#include "ASTVisitor.h"

#define SPECIAL_CASE_EXPRESSIONS()  \
    X(IdentifierExpression)         \
    X(ConstantExpression)           \
    X(BinaryExpression)             \
    X(UnaryExpression)              \
    X(CallExpression)               \
    X(WhileExpression)              \
    X(DoWhileExpression)            \
    X(IfExpression)                 \
    X(ElseIfExpression)             \
    X(ElseExpression)               \
    X(ForExpression)                \
    X(FunctionDefinitionExpression) \
    X(ImportExpression)             \
    X(ExternExpression)             \
    X(LoopExpression)               \
    X(ReturnExpression)             \
    X(StructureExpression)          \
    X(VariableExpression)           \
    X(SimpleTypeExpression)         \
    X(TypeOfExpression)

namespace ISL::Parser {
    class ASTPrinter: public ASTVisitor {
        public:
            ASTPrinter(std::ostream&);

            virtual bool traverseAST(const std::vector<AST::ExpressionPtr>&) override;

#define X(EXPR) virtual bool visit##EXPR(const AST:: EXPR &) override;
            SPECIAL_CASE_EXPRESSIONS()
#undef X

            virtual void traversalFinished() override;

        protected:
            virtual std::string formatProperty(const std::string&,
                                               const std::string&);

            virtual std::string formatStartOfTraversal() = 0;
            virtual std::string formatEndOfTraversal() = 0;
            virtual std::string formatSeperator() = 0;
            virtual std::string formatNewLine() = 0;
            virtual std::string formatPropertyKey(const std::string&) = 0;
            virtual std::string formatPropertyValue(const std::string&) = 0;
            virtual std::string formatExprStart(const std::string&) = 0;
            virtual std::string formatExprEnd(const std::string&) = 0;
            virtual std::string formatQuote();
            virtual std::string formatEscapedQuote();
            virtual std::string formatGroupStart(const std::string&) = 0;
            virtual std::string formatGroupEnd(const std::string&) = 0;

            virtual void traverseSubExprStart(const std::string&) override;
            virtual void traverseSubExprEnd(const std::string&) override;

            virtual void traverseSubExprGroupStart(const std::string&) override;
            virtual void traverseSubExprGroupEnd(const std::string&) override;

            virtual std::string indent() const;

            bool isInGroup() const;
            size_t getGroupSize() const;

        private:
            size_t m_depth = 1;

            std::stack<bool> m_in_group;
            size_t m_group_count;

            std::stack<size_t> m_sub_expr_count;
            std::string m_current_text;

            std::ostream& m_out;
    };
}

#endif

