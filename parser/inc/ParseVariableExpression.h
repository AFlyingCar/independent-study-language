
#ifndef PARSE_VARIABLE_EXPRESSION_H
#define PARSE_VARIABLE_EXPRESSION_H

#include <optional>

#include "ast/VariableExpression.h"
#include "TokenList.h"

namespace ISL::Parser {
    AST::VariableExpressionPtr parseVariableExpression(TokenListIter,
                                                       TokenListIter);
}

#endif

