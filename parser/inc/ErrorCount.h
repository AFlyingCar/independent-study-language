#ifndef PARSER_ERROR_COUNT_H
#define PARSER_ERROR_COUNT_H

#include <cctype> // size_t

namespace ISL::Parser {
    std::size_t getErrorCount();

    void error();
}

#endif

