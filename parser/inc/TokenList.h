
#ifndef TOKEN_LIST_H
#define TOKEN_LIST_H

#include <vector>
#include "Token.h"

namespace ISL::Parser {
    using TokenList = std::vector<Lexer::TokenVariant>;
    using RawTokenListIter = TokenList::const_iterator;

    struct TokenListIter {
        public:
            //------------------------------------------------------------------
            // These aliases allow us to satisfy the LegacyInputIterator concept
            //------------------------------------------------------------------
            using difference_type = size_t;
            using value_type = Lexer::Token;
            using pointer = value_type*;
            using reference = value_type&;
            using iterator_category = std::input_iterator_tag;

            TokenListIter(RawTokenListIter);
            TokenListIter(const TokenListIter&);

            TokenListIter& operator=(const TokenListIter&);

            TokenListIter operator+(int) const;

            bool operator==(const TokenListIter&) const;
            bool operator!=(const TokenListIter&) const;

            TokenListIter operator++();
            TokenListIter& operator++(int);

            const Lexer::Token& operator*() const;

            const Lexer::Token* operator->() const;

            RawTokenListIter raw();

        private:
            RawTokenListIter m_iter;
    };
}

#endif

