#ifndef PARSER_H
#define PARSER_H

#include <functional>
#include <optional>
#include <vector>
#include <string>
#include <map>

#include "Token.h"

#include "ast/Expression.h"
#include "ast/IdentifierExpression.h"

#include "TokenList.h"

namespace ISL::Parser {
    using KeywordInfoType = int;

    using ParserReturnType = std::optional<AST::ExpressionPtr>;
    using ParserFunctorType = std::function<AST::ExpressionPtr(TokenListIter&,
                                                               const TokenListIter&)>;

    AST::ExpressionPtr parsingFailure(TokenListIter&, const TokenListIter&);

    // Level 2 parsers

    AST::ExpressionPtr parseKeywordStatement(TokenListIter&, const TokenListIter&);

    // Level 1 parsers

    AST::ExpressionPtr parseStatementImpl(TokenListIter&, const TokenListIter&);
    std::pair<AST::ExpressionPtr, TokenListIter>
        parseStatement(TokenListIter, const TokenListIter&);
    std::vector<AST::ExpressionPtr> parse(const TokenList&);

    // keyword -> parseFunction
    extern std::map<std::string, ParserFunctorType> KEYWORD_INFO_MAPPING;
}

#endif

