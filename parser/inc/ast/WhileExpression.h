
#ifndef WHILE_EXPRESSION_H
#define WHILE_EXPRESSION_H

#include "Expression.h"

#include <vector>

namespace ISL::Parser::AST {
    class WhileExpression: public Expression {
        public:
            WhileExpression(ExpressionPtr&&);

            const ExpressionPtr& getCondExpression() const;

            const std::vector<ExpressionPtr>& getBody() const;
            std::vector<ExpressionPtr>& getBody();

            virtual void toJSON(std::ostream&) override;

        private:
            ExpressionPtr m_cond_expr;

            std::vector<ExpressionPtr> m_body;
    };
}

#endif

