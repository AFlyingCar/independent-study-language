
#ifndef FOR_EXPRESSION_H
#define FOR_EXPRESSION_H

#include <vector>

#include "Expression.h"

#include "VariableExpression.h"

namespace ISL::Parser::AST {
    class ForExpression: public Expression {
        public:
            ForExpression(VariableExpressionPtr&&, ExpressionPtr&&, ExpressionPtr&&);

            const VariableExpressionPtr& getVarExpression() const;
            const ExpressionPtr& getCondExpression() const;
            const ExpressionPtr& getIncExpression() const;

            const std::vector<ExpressionPtr>& getBody() const;
            std::vector<ExpressionPtr>& getBody();

            virtual void toJSON(std::ostream&) override;

        private:
            VariableExpressionPtr m_var_expr;
            ExpressionPtr m_cond_expr;
            ExpressionPtr m_inc_expr;

            std::vector<ExpressionPtr> m_body;
    };
}

#endif

