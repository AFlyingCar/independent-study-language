#ifndef CALL_EXPRESSION_H
#define CALL_EXPRESSION_H

#include <memory>
#include <vector>

#include "Expression.h"

namespace ISL::Parser::AST {
    class CallExpression: public Expression {
        public:
            using ArgumentList = std::vector<ExpressionPtr>;

            CallExpression(const std::string&, ArgumentList&&);

            const ArgumentList& getArgumentList() const;
            const std::string& getIdentifier() const;

            virtual void toJSON(std::ostream&) override;

        private:
            ArgumentList m_arguments;
            std::string m_identifier;
    };
}

#endif

