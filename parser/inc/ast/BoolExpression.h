#ifndef BOOL_EXPRESSION_H
#define BOOL_EXPRESSION_H

#include "ConstantExpression.h"

namespace ISL::Parser::AST {
    class BoolExpression: public ConstantExpression {
        public:
            BoolExpression(bool);
    };
}

#endif

