#ifndef STRING_EXPRESSION_H
#define STRING_EXPRESSION_H

#include <string>

#include "ConstantExpression.h"

namespace ISL::Parser::AST {
    class StringExpression: public ConstantExpression {
        public:
            StringExpression(const std::string&);
    };
}

#endif

