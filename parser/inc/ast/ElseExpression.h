#ifndef ELSE_EXPRESSION_H
#define ELSE_EXPRESSION_H

#include "Expression.h"

#include <vector>

namespace ISL::Parser::AST {
    class ElseExpression: public Expression {
        public:
            ElseExpression();

            const std::vector<ExpressionPtr>& getBody() const;
            std::vector<ExpressionPtr>& getBody();

            virtual void toJSON(std::ostream&) override;

        private:
            std::vector<ExpressionPtr> m_body;
    };
}

#endif

