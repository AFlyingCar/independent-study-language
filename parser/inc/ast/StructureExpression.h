
#ifndef STRUCTURE_EXPRESSION_H
#define STRUCTURE_EXPRESSION_H

#include <vector>
#include <string>

#include "Expression.h"
#include "IdentifierExpression.h"
#include "VariableExpression.h"

namespace ISL::Parser::AST {
    class StructureExpression: public Expression {
        public:
            StructureExpression(IdentifierExpressionPtr&&);

            const IdentifierExpressionPtr& getIdentifier() const;

            const std::vector<VariableExpressionPtr>& getMembers() const;
            std::vector<VariableExpressionPtr>& getMembers();

            virtual void toJSON(std::ostream&) override;

        private:
            IdentifierExpressionPtr m_iden;

            std::vector<VariableExpressionPtr> m_members;
    };
}

#endif

