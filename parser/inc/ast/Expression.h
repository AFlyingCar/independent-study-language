/**
 * @file Expression.h
 * @author Tyler Robbins
 * 
 * @brief Defines the base Expression class.
 */

#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <memory>
#include <ostream>

namespace ISL::Parser::AST {
    /**
     * @brief The base class of all Expressions
     */
    class Expression {
        public:
            virtual ~Expression() = default;

            virtual void toJSON(std::ostream&) = 0;
    };

    class EmptyExpression: public Expression {
        public:
            virtual void toJSON(std::ostream&) override;
    };

    using ExpressionPtr = std::unique_ptr<AST::Expression>;
}

#endif

