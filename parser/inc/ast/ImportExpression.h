#ifndef IMPORT_EXPRESSION_H
#define IMPORT_EXPRESSION_H

#include "Expression.h"
#include "ConstantExpression.h"

namespace ISL::Parser::AST {
    class ImportExpression: public Expression {
        public:
            ImportExpression(ConstantExpressionPtr&&);

            const ConstantExpressionPtr& getValue() const;

            virtual void toJSON(std::ostream&) override;

        private:
            ConstantExpressionPtr m_value;
    };

    using ImportExpressionPtr = std::unique_ptr<ImportExpression>;
}

#endif

