#ifndef CONSTANT_EXPRESSION_H
#define CONSTANT_EXPRESSION_H

#include "Expression.h"

#include "Types.h"

namespace ISL::Parser::AST {
    class ConstantExpression: public Expression {
        public:
            ConstantExpression(const Common::BuiltinType&);

            Common::BuiltinType getValue() const;

            virtual void toJSON(std::ostream&) override;
        private:
            Common::BuiltinType m_value;
    };

    using ConstantExpressionPtr = std::unique_ptr<ConstantExpression>;
}

#endif


