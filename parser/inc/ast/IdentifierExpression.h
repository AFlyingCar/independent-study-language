#ifndef IDENTIFIER_EXPRESSION_H
#define IDENTIFIER_EXPRESSION_H

#include <string>

#include "Expression.h"

namespace ISL::Parser::AST {
    class IdentifierExpression: public Expression {
        public:
            IdentifierExpression(const std::string&);

            const std::string& getIdentifier() const;

            virtual void toJSON(std::ostream&) override;

        private:
            std::string m_identifier;
    };

    using IdentifierExpressionPtr = std::unique_ptr<IdentifierExpression>;
}

#endif

