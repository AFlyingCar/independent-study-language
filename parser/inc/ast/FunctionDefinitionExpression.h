
#ifndef FUNCTION_DEFINITION_EXPRESSION_H
#define FUNCTION_DEFINITION_EXPRESSION_H

#include "Expression.h"
#include "IdentifierExpression.h"
#include "ParameterExpression.h"
#include "TypeExpression.h"

#include <vector>

namespace ISL::Parser::AST {
    class FunctionDefinitionExpression: public Expression {
        public:
            FunctionDefinitionExpression(IdentifierExpressionPtr&&,
                                         std::vector<ParameterExpressionPtr>&&,
                                         TypeExpressionPtr&&);

            const IdentifierExpressionPtr& getFunctionIdentifier() const;
            const std::vector<ParameterExpressionPtr>& getParameterList() const;
            const TypeExpressionPtr& getReturnType() const;

            const std::vector<ExpressionPtr>& getBody() const;
            std::vector<ExpressionPtr>& getBody();

            virtual void toJSON(std::ostream&) override;

        private:
            IdentifierExpressionPtr m_func_iden;
            std::vector<ParameterExpressionPtr> m_parameter_list;
            TypeExpressionPtr m_return_type;

            std::vector<ExpressionPtr> m_body;
    };

    using FunctionDefinitionExpressionPtr = std::unique_ptr<FunctionDefinitionExpression>;
}

#endif

