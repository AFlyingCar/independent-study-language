#ifndef BINARY_EXPRESSION_H
#define BINARY_EXPRESSION_H

#include <memory>

#include "Expression.h"

#include "operator/Operator.h"

namespace ISL::Parser::AST {
    class BinaryExpression: public Expression {
        public:
            BinaryExpression(const Common::Operator&,
                             ExpressionPtr,
                             ExpressionPtr);

            const Common::Operator& getOperator() const;
            const ExpressionPtr& getLHS() const;
            const ExpressionPtr& getRHS() const;

            // So that the expression can be torn down if needed
            Common::Operator& getOperator();
            ExpressionPtr& getLHS();
            ExpressionPtr& getRHS();

            virtual void toJSON(std::ostream&) override;

        private:
            Common::Operator m_operator;

            ExpressionPtr m_lhs;
            ExpressionPtr m_rhs;
    };
}

#endif

