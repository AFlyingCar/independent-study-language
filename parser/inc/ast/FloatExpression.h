#ifndef FLOAT_EXPRESSION_H
#define FLOAT_EXPRESSION_H

#include "ConstantExpression.h"

namespace ISL::Parser::AST {
    class FloatExpression: public ConstantExpression {
        public:
            FloatExpression(float);
    };
}

#endif

