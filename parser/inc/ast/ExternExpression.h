#ifndef EXTERN_EXPRESSION_H
#define EXTERN_EXPRESSION_H

#include <variant>

#include "ParameterExpression.h"
#include "FunctionDefinitionExpression.h"

namespace ISL::Parser::AST {
    class ExternExpression: public Expression {
        public:
            using ExternVariant = std::variant<ParameterExpressionPtr, FunctionDefinitionExpressionPtr>;

            ExternExpression(ParameterExpressionPtr&& var_expr);
            ExternExpression(FunctionDefinitionExpressionPtr&& var_expr);

            virtual void toJSON(std::ostream&) override;

            const ExternVariant& getSymbol() const;

        private:
            ExternVariant m_symbol;
    };

    using ExternExpressionPtr = std::unique_ptr<ExternExpression>;
}

#endif

