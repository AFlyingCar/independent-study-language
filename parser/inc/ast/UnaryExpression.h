#ifndef UNARY_EXPRESSION_H
#define UNARY_EXPRESSION_H

#include <memory>

#include "operator/Operator.h"

#include "Expression.h"

namespace ISL::Parser::AST {
    class UnaryExpression: public Expression {
        public:
            UnaryExpression(const Common::Operator&,
                            ExpressionPtr);

            const Common::Operator& getOperator() const;
            const ExpressionPtr& getOperand() const;

            virtual void toJSON(std::ostream&) override;

        private:
            Common::Operator m_operator;

            ExpressionPtr m_operand;
    };
}

#endif

