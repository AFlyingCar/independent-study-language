#ifndef PARAMETER_EXPRESSION_H
#define PARAMETER_EXPRESSION_H

#include "Expression.h"
#include "IdentifierExpression.h"
#include "TypeExpression.h"

namespace ISL::Parser::AST {
    class ParameterExpression: public Expression {
        public:
            ParameterExpression(TypeExpressionPtr&&, IdentifierExpressionPtr&&);

            const TypeExpressionPtr& getTypeIdentifier() const;
            const IdentifierExpressionPtr& getVarIdentifier() const;

            virtual void toJSON(std::ostream&) override;

        private:
            TypeExpressionPtr m_type_iden;
            IdentifierExpressionPtr m_var_iden;
    };

    using ParameterExpressionPtr = std::unique_ptr<ParameterExpression>;
}

#endif

