#ifndef RETURN_EXPRESSION_H
#define RETURN_EXPRESSION_H

#include "Expression.h"

namespace ISL::Parser::AST {
    class ReturnExpression: public Expression {
        public:
            ReturnExpression(ExpressionPtr&&);

            const ExpressionPtr& getExpr() const;

            virtual void toJSON(std::ostream&) override;

        private:
            ExpressionPtr m_expr;
    };

    using ReturnExpressionPtr = std::unique_ptr<ReturnExpression>;
}

#endif

