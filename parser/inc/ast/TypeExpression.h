
#ifndef TYPE_EXPRESSION_H
#define TYPE_EXPRESSION_H

#include <memory>

#include "Expression.h"

#include "IdentifierExpression.h"

namespace ISL::Parser::AST {
    class TypeExpression;

    using TypeExpressionPtr = std::unique_ptr<TypeExpression>;

    class TypeExpression: public Expression {
        public:
            TypeExpression();
            TypeExpression(TypeExpressionPtr&&);

            const TypeExpressionPtr& getSubType() const;
            TypeExpressionPtr& getSubType();

            void setSubType(TypeExpressionPtr&&);

        private:
            TypeExpressionPtr m_sub_type;
    };

    class SimpleTypeExpression: public TypeExpression {
        public:
            SimpleTypeExpression(IdentifierExpressionPtr&&);
            SimpleTypeExpression(IdentifierExpressionPtr&&, TypeExpressionPtr&&);

            const IdentifierExpressionPtr& getTypeIdentifier() const;

            virtual void toJSON(std::ostream&) override;

        private:
            IdentifierExpressionPtr m_type_iden;
    };

    class TypeOfExpression: public TypeExpression {
        public:
            TypeOfExpression(ExpressionPtr&&);
            TypeOfExpression(ExpressionPtr&&, TypeExpressionPtr&&);

            const ExpressionPtr& getExpression() const;

            virtual void toJSON(std::ostream&) override;

        private:
            ExpressionPtr m_expr;
    };
}

#endif

