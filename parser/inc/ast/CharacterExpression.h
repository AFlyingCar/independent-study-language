#ifndef CHARACTER_EXPRESSION_H
#define CHARACTER_EXPRESSION_H

#include "ConstantExpression.h"

namespace ISL::Parser::AST {
    class CharacterExpression: public ConstantExpression {
        public:
            CharacterExpression(char);
    };
}

#endif

