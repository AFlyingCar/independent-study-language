#ifndef LOOP_EXPRESSION_H
#define LOOP_EXPRESSION_H

#include "Expression.h"

#include <vector>

namespace ISL::Parser::AST {
    class LoopExpression: public Expression {
        public:
            LoopExpression();

            const std::vector<ExpressionPtr>& getBody() const;
            std::vector<ExpressionPtr>& getBody();

            virtual void toJSON(std::ostream&) override;

        private:
            std::vector<ExpressionPtr> m_body;
    };
}

#endif

