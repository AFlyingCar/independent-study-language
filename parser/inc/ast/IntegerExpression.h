#ifndef INTEGER_EXPRESSION_H
#define INTEGER_EXPRESSION_H

#include "ConstantExpression.h"

namespace ISL::Parser::AST {
    class IntegerExpression: public ConstantExpression {
        public:
            IntegerExpression(int);
    };
}

#endif

