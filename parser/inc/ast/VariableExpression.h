#ifndef VARIABLE_EXPRESSION_H
#define VARIABLE_EXPRESSION_H

#include "Expression.h"
#include "IdentifierExpression.h"
#include "TypeExpression.h"

namespace ISL::Parser::AST {
    class VariableExpression: public Expression {
        public:
            VariableExpression(TypeExpressionPtr&&, IdentifierExpressionPtr&&,
                               ExpressionPtr&&);

            const TypeExpressionPtr& getTypeExpression() const;
            const IdentifierExpressionPtr& getVarIdentifier() const;
            const ExpressionPtr& getValueExpr() const;

            virtual void toJSON(std::ostream&) override;

        private:
            TypeExpressionPtr m_type_expr;
            IdentifierExpressionPtr m_var_iden;
            ExpressionPtr m_val;
    };

    using VariableExpressionPtr = std::unique_ptr<VariableExpression>;
}

#endif

