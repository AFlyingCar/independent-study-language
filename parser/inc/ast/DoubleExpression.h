#ifndef DOUBLE_EXPRESSION_H
#define DOUBLE_EXPRESSION_H

#include "ConstantExpression.h"

namespace ISL::Parser::AST {
    class DoubleExpression: public ConstantExpression {
        public:
            DoubleExpression(double);
    };
}

#endif

