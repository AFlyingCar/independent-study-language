
#ifndef AST_VISITOR_H
#define AST_VISITOR_H

#include <stack>

#include "ast/ConstantExpression.h"
#include "ast/BinaryExpression.h"
#include "ast/UnaryExpression.h"
#include "ast/CallExpression.h"

#include "ast/DoWhileExpression.h"
#include "ast/WhileExpression.h"

#include "ast/IfExpression.h"
#include "ast/ElseIfExpression.h"
#include "ast/ElseExpression.h"
#include "ast/ForExpression.h"
#include "ast/LoopExpression.h"

#include "ast/ConstantExpression.h"
#include "ast/IdentifierExpression.h"
#include "ast/Expression.h"
#include "ast/FunctionDefinitionExpression.h"
#include "ast/ImportExpression.h"
#include "ast/ExternExpression.h"
#include "ast/ReturnExpression.h"
#include "ast/StructureExpression.h"
#include "ast/VariableExpression.h"
#include "ast/ParameterExpression.h"
// #include "ast/EnumerationExpression.h"

#define AST_EXPRESSIONS()           \
    X(ConstantExpression)           \
    X(IdentifierExpression)         \
    X(BinaryExpression)             \
    X(UnaryExpression)              \
    X(CallExpression)               \
    X(WhileExpression)              \
    X(DoWhileExpression)            \
    X(IfExpression)                 \
    X(ElseIfExpression)             \
    X(ElseExpression)               \
    X(ForExpression)                \
    X(FunctionDefinitionExpression) \
    X(ImportExpression)             \
    X(ExternExpression)             \
    X(LoopExpression)               \
    X(ReturnExpression)             \
    X(StructureExpression)          \
    X(VariableExpression)           \
    X(SimpleTypeExpression)         \
    X(TypeOfExpression)             \
    X(EmptyExpression)              \
    X(ParameterExpression)

namespace ISL::Parser {
    class ASTVisitor {
        public:
            using AbstractSyntaxTree = std::vector<AST::ExpressionPtr>;
            using TraversalState = std::pair<const size_t&, const AbstractSyntaxTree&>;

            virtual ~ASTVisitor() = default;

            virtual bool traverseAST(const AbstractSyntaxTree&);

#define X(EXPR) virtual bool traverse##EXPR (const std::unique_ptr<AST:: EXPR > &);
            AST_EXPRESSIONS()
#undef X

#define X(EXPR) virtual bool visit##EXPR (const AST:: EXPR &);
            AST_EXPRESSIONS()
#undef X

            // Handle these seperately
            virtual bool traverseExpression(const std::unique_ptr<AST::Expression>&);
            virtual bool visitExpression(const AST::Expression&);

            virtual bool traverseTypeExpression(const std::unique_ptr<AST::TypeExpression>&);
            virtual bool visitTypeExpression(const AST::TypeExpression&);

            virtual void traversalFinished();

        protected:
            bool visit(AST::ExpressionPtr);

            virtual void traverseSubExprStart(const std::string&);
            virtual void traverseSubExprEnd(const std::string&);

            virtual void traverseSubExprGroupStart(const std::string&);
            virtual void traverseSubExprGroupEnd(const std::string&);

            size_t getSizeOfGroup() const;

            const TraversalState& getTraversalState() const;
        private:
            std::stack<size_t> m_group_sizes;
            std::stack<TraversalState> m_traversal_state;
    };
}

#endif

