
#ifndef PARSE_KEYWORD_STATEMENTS_H
#define PARSE_KEYWORD_STATEMENTS_H

#include "TokenList.h"
#include "ast/Expression.h"

namespace ISL::Parser {
    AST::ExpressionPtr parseReturnStatement(TokenListIter&, const TokenListIter&);
    AST::ExpressionPtr parseImportStatement(TokenListIter&, const TokenListIter&);
    AST::ExpressionPtr parseExternStatement(TokenListIter&, const TokenListIter&);

    AST::ExpressionPtr parseEndBlockStatement(TokenListIter&, const TokenListIter&);

    AST::ExpressionPtr parseForStatement(TokenListIter&, const TokenListIter&);

    AST::ExpressionPtr parseIfStatement(TokenListIter&, const TokenListIter&);
    AST::ExpressionPtr parseElseIfStatement(TokenListIter&, const TokenListIter&);
    AST::ExpressionPtr parseElseStatement(TokenListIter&, const TokenListIter&);

    AST::ExpressionPtr parseLoopStatement(TokenListIter&, const TokenListIter&);

    AST::ExpressionPtr parseWhileStatement(TokenListIter&, const TokenListIter&);
    AST::ExpressionPtr parseDoWhileStatement(TokenListIter&, const TokenListIter&);

    AST::ExpressionPtr parseFunctionStatement(TokenListIter&, const TokenListIter&);

    AST::ExpressionPtr parseStructureStatement(TokenListIter&, const TokenListIter&);

#if 0
    AST::ExpressionPtr parseEnumerationStatement(TokenListIter&, const TokenListIter&);
#endif
}

#endif

