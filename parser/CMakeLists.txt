cmake_minimum_required(VERSION 3.0)

set(SOURCES
    src/Parser.cpp
    src/TokenList.cpp
    src/ErrorCount.cpp
    src/ParseExpressions.cpp
    src/ParseKeywordStatements.cpp
    src/ParseVariableExpression.cpp
    src/ParseOperatorExpressions.cpp

    src/details/ScopeHandling.cpp

    src/ast/Expression.cpp
    src/ast/CallExpression.cpp
    src/ast/ConstantExpression.cpp
    src/ast/StringExpression.cpp
    src/ast/BinaryExpression.cpp
    src/ast/UnaryExpression.cpp
    src/ast/BoolExpression.cpp
    src/ast/CharacterExpression.cpp
    src/ast/IntegerExpression.cpp
    src/ast/FloatExpression.cpp
    src/ast/DoubleExpression.cpp
    src/ast/IdentifierExpression.cpp
    src/ast/VariableExpression.cpp
    src/ast/TypeExpression.cpp
    src/ast/ForExpression.cpp
    src/ast/IfExpression.cpp
    src/ast/ElseIfExpression.cpp
    src/ast/ElseExpression.cpp
    src/ast/FunctionDefinitionExpression.cpp
    src/ast/ParameterExpression.cpp
    src/ast/ReturnExpression.cpp
    src/ast/ImportExpression.cpp
    src/ast/ExternExpression.cpp
    src/ast/LoopExpression.cpp
    src/ast/WhileExpression.cpp
    src/ast/DoWhileExpression.cpp
    src/ast/StructureExpression.cpp

    src/ASTVisitor.cpp
    src/ASTPrinter.cpp
    src/JSONASTPrinter.cpp
    src/DotASTPrinter.cpp
)

add_library(isl_parser STATIC ${SOURCES})

target_include_directories(isl_parser PUBLIC inc/)
target_link_libraries(isl_parser PRIVATE isl_common isl_lexer)


