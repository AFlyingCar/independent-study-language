\documentclass{article}

\usepackage{listings}
\usepackage{color}
\usepackage[left=1cm,top=1cm,right=2cm,bottom=1cm,nohead,nofoot]{geometry}
\usepackage{pdfpages}
\usepackage{multirow}

\title{Language Grammar Specification}
\date{\today}
\author{%Garrett Conti \and
    Tyler Robbins}

\definecolor{gray}{rgb}{0.9,0.9,0.9}
\definecolor{blue}{rgb}{0.0,0.0,0.5}
\definecolor{lime}{rgb}{0.0,0.5,0.0}
\definecolor{green}{rgb}{0.0,0.5,0.0}
\definecolor{orange}{rgb}{1.0,0.33,0}

\pagenumbering{gobble}

\lstset{
    backgroundcolor=\color{gray},
    basicstyle=\ttfamily,
    columns=fixed,
    commentstyle=\color{blue},
    stringstyle=\color{green},
    %firstnumber=1,
    %numbers=left,
    keywordstyle=\color{orange},
    frame=trbl,
    showstringspaces=false,
    showspaces=false,
    showtabs=false,
}

\lstdefinelanguage{IndepStudLang}
{
    morekeywords={
        cast,
        sizeof,
        typeof,
        import,
        return,
        break,
        continue,
% SCOPED
        function,
        endfunction,
        if,
        elseif,
        else,
        endif,
        for,
        endfor,
        while,
        endwhile,
        dowhile,
        enddowhile,
        loop,
        endloop,
        structure,
        endstructure,
        struct,
        endstruct,
        enum,
        endenum,
        enumeration,
        endenumeration,
% TYPES
        void,
        int,
        float,
        bool,
        char,
        uint,
        uchar,
        String,
        double,
        Pointer,
        List,
        Any,
        Structure,
        Struct,
        Enumeration,
        Enum,
        % Type,
        const,
% VALUES
        true,
        false,
        null,
    },
    morecomment=[l]{//},
    morecomment=[s]{/*}{*/},
    morestring=[b]"
}

\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                 SYLLABUS                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \includepdf[pages=-]{build/Syllabus.pdf}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               LANGUAGE SPEC                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \maketitle

    \setlength\parindent{0pt}

    \section{Builtin Types}
        \begin{tabular}{|lll|}\hline
            \texttt{bool} & \texttt{void} & \texttt{Pointer} \\
            \texttt{char} & \texttt{uchar} & \texttt{List} \\
            \texttt{int} &  \texttt{uint} &  \texttt{String}\\
            \texttt{float} & \texttt{double} & \texttt{Any} \\\hline
        \end{tabular}\\

    \section{TypeExpression}
        A type can be any builtin type or a user defined structure or enum.

        Complex types can be created by specifying the complex modifier followed by the base type:
\begin{lstlisting}[language=IndepStudLang]
// Translates to int*** in C
Pointer Pointer Pointer int myPtr = null;
\end{lstlisting}

    \subsection{Function Pointers}
\begin{lstlisting}[language=IndepStudLang]
Pointer (float, float) -> int myFunctionPointer = myFunction;
\end{lstlisting}

        Oh no...
\begin{lstlisting}[language=IndepStudLang]
// Equivalent to:
// int myFunc(int,int)(float,float)();
function myFunc(int, int) -> Pointer (float, float) -> Pointer () -> int;
endfunction;
\end{lstlisting}

    \subsection{Lists}
\begin{lstlisting}[language=IndepStudLang]
// Equivalent to std::list<int> myList{5, 4, 3, 2, 1}; in C++
List int myList = (5, 4, 3, 2, 1);
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                  VALUES                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \section{ValueExpresion}

    \subsection{Value}
        Anything which results in a value. Escape sequences are supported.
        Note that \lstinline{Pointer} is not included in this list, as it is
        intended as a return-type from built-in/C-family functions only, and
        has no usage on its own.

\begin{lstlisting}
int matches Regex -?[0-9]+
uint matches Regex [0-9]+
double matches Regex [0-9]+\.[0-9]*
float matches Regex [0-9]+\.[0-9]*f
bool matches Regex (true|false)
char matches Regex -?('.+'|[0-9]+)
uchar matches Regex ('.+'|[0-9]+)
String matches Regex ".+"
List matches Regex \[ (ValueExpresion,)* \]
\end{lstlisting}

        Examples:
\begin{lstlisting}[language=IndepStudLang]
-5 // int
5 // uint
3.4 // double
3.4f // float
true // bool
-'c' // char
'c' // uchar
"String" // String
(5, 4, 3, 2, 1) // List
\end{lstlisting}

    \subsection{Operators}
        All C operators (except anything dealing with addresses/pointers) are supported.

        \begin{tabular}{|c|c|l|c|}\hline
            Precedence          & Operator                       & \multicolumn{1}{|c|}{Description}              & Associativity \\
            \hline
            \multirow{2}{*}{1}  & $++$, $--$                     & Suffix/Postfix increment and decrement         & \multirow{2}{*}{Left to right} \\
                                & $FunctionName()$               & Function Call                                  & \\
%                               & []                             & Array subscripting                             & \\
            \hline
            \multirow{5}{*}{2}  & $++$, $--$                     & Prefix increment and decrement                 & \multirow{5}{*}{Right to left} \\
                                & $+$, $-$                       & Unary plus and minus                           & \\
                                & $!$, \~{}                       & Logical NOT and Bitwise NOT                   & \\
                                & cast($type$,$ValueExpression$) & Type cast                                      & \\
                                & sizeof($type$)                 & Size of a given type, or number of enum values & \\
                                & typeof($ValueExpression$)      & Type of the given expression.                  & \\
            \hline
            3                   & $*$, $/$, $\%$                 & Multiplication, division, and remainder        & \multirow{11}{*}{Left to right} \\
            \cline{1-3}
            4                   & $+$, $-$                       & Addition and subtraction                       & \\
            \cline{1-3}
            5                   & $<<$, $>>$                     & Bitwise left and right shifts                  & \\
            \cline{1-3}
            \multirow{2}{*}{6}  & $<$, $<=$                      & Less than and Less than or equal to            & \\
                                & $>$, $>=$                      & Greater than and Greater than or equal to      & \\
            \cline{1-3}
            7                   & $==$, $!=$                     & Equal to and Not equal to                      & \\
            \cline{1-3}
            8                   & $\&$                           & Bitwise AND                                    & \\
            \cline{1-3}
            9                   & $\hat{}$                       & Bitwise XOR (exclusive OR)                     & \\
            \cline{1-3}
            10                  & $|$                            & Bitwise OR                                     & \\
            \cline{1-3}
            11                  & $\&\&$                         & Logical AND                                    & \\
            \cline{1-3}
            12                  & $||$                           & Logical OR                                     & \\
            \cline{1-3}
            13                  & $()$                           & List initialization                            & \\
            \hline
            \multirow{5}{*}{14} & $=$                            & Simple Assignment                              & \multirow{5}{*}{Right to left} \\
                                & $+=$, $-=$                     & Assignment by sum or by difference             & \\
                                & $*=$, $/=$, $\%=$              & Assignment by product, division, and remainder & \\
                                & $<<=$, $>>=$                   & Assignment by bitwise left and right shifts    & \\
                                & $\&=$, $\hat{}=$, $|=$, \~{}=  & Assignment by bitwise AND, XOR, OR, and NOT    & \\
            \hline
        \end{tabular}

    \subsection{Casting and SizeOf}
        \lstinline[language=IndepStudLang]{cast(Type, ValueExpression)} is an operator which results in an $Expression$ whose type is $Type$ and whose value is the result of $ValueExpression$. If the types are not directly compatible (for example, if casting from \lstinline[language=IndepStudLang]{float} to \lstinline[language=IndepStudLang]{int}), then some minor value conversions may be done.

        \lstinline[language=IndepStudLang]{sizeof(Type)} is an operator which results in a $uint$ value. If the given $Type$ is an $Enumeration$, then the resulting $Value$ is the number of $Enumeration$ elements available, otherwise, it is the size of that type in bytes, as specified by the ANSI C specification.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                STATEMENTS                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \section{Statement}
        A $Statement$ is a single executable or meta-executable line of code which either performs an operation at runtime or which sets up hints for the compiler about how the generated code should be structured and arranged. All $Statement$s must end in a ``$;$" (ANSI 0x3b) character.

        Most $Statement$s are used in pairs, with one $Statement$ being used to start a section, and another $Statement$ being used to end that same section. As such, many of the following sections will refer to both paired $Statement$s at the same time, as they cannot be used independently. In the case of nesting $Statement$ scopes, each new $Statement$ scope must be closed with the matching closing $Statement$ before the previously opened $Statement$ can be closed in turn. If this rule is followed, then a $Statement$ is considered to be on the same ``level" as its matching closing $Statement$.

    \subsection{Value}
        A $ValueStatement$, different from a $Value$, is a $Statement$ which consists only of a $ValueExpression$.

    \subsection{Variables}
        A $Variable$ is a $Statement$ which places the result of some $ValueExpression$ into a section of memory, given some token which follows the rules of $UserDefinedToken$s.
        If a $Variable$ of the given user-defined name already exists (referred to from now on as Variable Shadowing), or if the name is a reserved keyword, then an error is issued.

        A $Variable$ must always be of the following form:
\begin{lstlisting}[language=IndepStudLang]
Type varName = ValueExpression;
\end{lstlisting}
        Where $Type$ may be any $Builtin-Type$ or previously-declared user-defined type, and $ValueExpression$ must be a valid $ValueExpression$ according to the rules in the $Expressions$ section. $Variable$s must be initialized with a $ValueExpression$ directly at their declaration point. $Variable$s may also be declared at any point in a scope.

    \subsection{Functions}
        A $Function$ defines a scope containing executable code which may be invoked and executed at runtime, with a fixed number of parameters being given as arguments. A $Function$ may take 0 or more arguments, which must be specified as a comma, seperated list of the following form: \lstinline[language=IndepStudLang]{Type UserDefinedName}, where $Type$ may be any $Builtin-Type$ or previously-declared user-defined type, and where $UserDefinedName$ may be any token which follows the rules of $UserDefinedToken$s. No two arguments may share a $UserDefinedToken$ and arguments may not be a reserved keyword, however they may Shadow Variables in an outer scope.

    $Function$s must also specify a $Type$ to return, which is the type of the $CallExpression$ that is used to invoke this function. If $Type$ is not \lstinline[language=IndepStudLang]{void}, then all possible paths this $Function$ may take from $Conditional$s must result in a $Return$.
\begin{lstlisting}[language=IndepStudLang]
function FunctionName(Type paramName) -> Type;
    // Additional Statements
endfunction;
\end{lstlisting}

    \subsection{Return}
        A $Return$ is a $Statement$ which jumps to the nearest \lstinline[language=IndepStudLang]{endfunction} $Statement$.
        When used in a $Function$ which does not specify its return type as \lstinline[language=IndepStudLang]{void}, a $ValueExpression$ is required whose Type matches the specified return type of the current $Function$.
\begin{lstlisting}[language=IndepStudLang]
return 5 + 5;
\end{lstlisting}

    \subsection{Conditionals}
        A $Conditional$ defines a branching scope of code. Each $If$ and $ElseIf$ must be given a $ValueExpression$ whose type is \lstinline[language=IndepStudLang]{bool}.

        If the $ValueExpression$ results in a $Value$ of \lstinline[language=IndepStudLang]{false}, then execution will jump to the next $ElseIf$ or $EndIf$ on the same level as the current $Statement$.
        If the $ValueExpression$ results in a $Value$ of \lstinline[language=IndepStudLang]{true} however, then all $Statement$s between the current $Statement$ and the matching $ElseIf$ or $EndIf$ will be executed. Upon reaching the matching $ElseIf$ or $EndIf$, execution will then jump to the first $EndIf$ on the same level as this $Statement$.
\begin{lstlisting}[language=IndepStudLang]
if(<Expression>);
    // Additional Statements
elseif(<Expression>);
    // Additional Statements
else;
    // Additional Statements
endif;
\end{lstlisting}

    \subsection{Loops}
        A $Loop$ defines a repeating scope of code. There are 4 different types of $LoopStatement$s, the \lstinline[language=IndepStudLang]{loop}, \lstinline[language=IndepStudLang]{while}, \lstinline[language=IndepStudLang]{dowhile}, and \lstinline[language=IndepStudLang]{for}.

        % A \lstinline[language=IndepStudLang]{loop}-style loop is the most simple type of loop.

        Each $Loop$ may contain a $Break$ or a $Continue$ $Statement$. If a $BreakStatement$ is encountered, then execution will jump to the nearest $LoopEnd$ $Statement$ on the same level of the current $Loop$. If a $ContinueStatement$ is encountered, then execution will jump back to the previous $LoopStatement$ on the same level.
\begin{lstlisting}[language=IndepStudLang]
loop;
    // Additional Statements
endloop;

while(<Expression>);
    // Additional Statements
endwhile;

dowhile(<Expression>);
    // Additional Statements
enddowhile;

for(<VariableStatement>; <Expression>; <Expression>);
    // Additional Statements
endfor;
\end{lstlisting}

    \subsection{User Defined Types}
\begin{lstlisting}[language=IndepStudLang]
// or 'struct'
structure UserStructureName;
    VariableStatement;
    // ...
endstructure;
\end{lstlisting}

\begin{lstlisting}[language=IndepStudLang]
// or 'enum'
enumeration UserEnumName;
    Name, // Starts at 0
    Name2, // 1
    // ...
endenumeration;
\end{lstlisting}
        Enumerations are of the same size as an int.

        They may be accessed with \lstinline[language=IndepStudLang]{UserEnumName:Name}.

%NOTE: Should we wrap included functions in a file namespace? utils::swap;
%NOTE: Or should we make it optional? import namespace "utils.isl";
    \subsection{Importing Files}
        Will jump into the given file as if it were a function which returns at the end.
\begin{lstlisting}[language=IndepStudLang]
import "/Path/To/Filename";
\end{lstlisting}
        Ex: \lstinline[language=IndepStudLang]{import "myfile.ext";}

    \subsection{External Symbols}
        Will mark that a given symbol can be found externally, and whose definition may be found at link time. The syntax is the beginning of a symbol definition, but followed by the \lstinline[language=IndepStudLang]{extern} keyword.
\begin{lstlisting}[language=IndepStudLang]
extern $Statement$;
\end{lstlisting}
    Ex: \lstinline[language=IndepStudLang]{extern function foo(int, Pointer) -> void;}
    Ex: \lstinline[language=IndepStudLang]{extern int bar;}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              MISCELLANEOUS                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \section{Miscellaneous}
        \subsection{Comments}
            Any text between \lstinline{//} and the end of the current line, or
            between \lstinline{/*} and the immediate next \lstinline{*/} is
            treated as a comment and ignored.

        \subsection{UserDefinedToken}
            User Defined Tokens must match the following regex: \lstinline{[a-zA-Z_]+[a-zA-Z0-9_]*}.

        \subsection{Constraints}
            Can be used to place additional constraints on a variable.
            If an expression is to be used, then \lstinline{x} is to be used in the constraint expression as a substitute for this variable.
            Multiple constraints can be added, and separated by commas.
            Violating a constraint will lead to a compiler error

\begin{lstlisting}[language=IndepStudLang]
// const
int constVarName[const] = someValue;

// Type constraining
Any varName[int] = someValue; // Requires that this type is an int
Any varName[!int] = someValue; // Requires that this type is not an int

// Meta-type constraining

// Requires that this type be either a Structure or an enum
Any varName[Structure, Enum] = MyEnum:MY_VALUE;

// Restricting this value between 2 and 10
Type varName[x < 10, x > 2 /*, ... */] = value;

// Restricting the length of a String and List
String varName[Count(x) < 10] = "foo spam";
List int varName[Count(x) < 10] = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

// Restricting the input to be between 0 and 100 inclusive
function cantorPairing(int k1[0 <= x <= 100], int k2[0 <= x <= 100]) -> uint;
    return .5 * (k1 + k2) * (k1 + k2 + 1) + k2;
endfunction;
\end{lstlisting}

\setlength\parindent{0pt}
    \subsection{Builtin Functions}
\lstinline[language=IndepStudLang]{function Get(Any value[List, String, Pointer], uint index) -> Any;} \\
    \indent Gets a new Value from the given Value, offset by \lstinline{index}. \\
\lstinline[language=IndepStudLang]{function Set(Any value[List, String, Pointer], uint index, Any element) -> void;} \\
    \indent Sets the data at the given $Value$ \lstinline{value}, offset by \lstinline{index}, to the given value \lstinline{element}. \\
\lstinline[language=IndepStudLang]{function Allocate(uint size) -> Pointer void;} \\
    \indent Allocates \lstinline{size} bytes of space. \\
\lstinline[language=IndepStudLang]{function Free(Pointer void p) -> void;} \\
    \indent Deallocates space to which \lstinline{p} points. If \lstinline{p} is \lstinline[language=IndepStudLang]{null}, there is no effect. If it is a pointer returned by calloc. \\
\lstinline[language=IndepStudLang]{function Count(Any value[List, String, Pointer]) -> uint;} \\
    \indent Gets the number of elements in the given value \\
\lstinline[language=IndepStudLang]{function ToString(Any value[!Pointer, !List, !String]) -> String;} \\
    \indent Converts the given value to a String. If given an enum, it will return the name of the enum constant. \\
\lstinline[language=IndepStudLang]{function Add(Any value[List, String], Any element) -> void;} \\
    \indent Adds the given \lstinline{element} to the given \lstinline{value}, which may be either a \lstinline[language=IndepStudLang]{List}, or a \lstinline[language=IndepStudLang]{String}. The $Type$ of \lstinline{element} must match the $SubType$ that \lstinline{value} was declared with. \\
\lstinline[language=IndepStudLang]{function Remove(Any value[List, String], uint index) -> void;} \\
    \indent Removes the element at the given \lstinline{index} from the given \lstinline{value}, which may be either a \lstinline[language=IndepStudLang]{List}, or a \lstinline[language=IndepStudLang]{String}. \\
\lstinline[language=IndepStudLang]{function Clear(Any value[List, String]) -> void;} \\
    \indent Clears all elements from the given \lstinline{value}, which may be either a \lstinline[language=IndepStudLang]{List}, or a \lstinline[language=IndepStudLang]{String}. \\
\lstinline[language=IndepStudLang]{function GetMember(Structure structValue, String memberName) -> Any;} \\
    \indent Gets the member from the given \lstinline[language=IndepStudLang]{Structure structValue}, whose name matches the given \lstinline[language=IndepStudLang]{String memberName}. \\
\lstinline[language=IndepStudLang]{function SetMember(Structure structValue, String memberName, Any value) -> void;} \\
    \indent Sets the member of the given \lstinline[language=IndepStudLang]{Structure structValue}, whose name matches the given \lstinline[language=IndepStudLang]{String memberName}, to the given \lstinline[language=IndepStudLang]{value}. The $Type$ of \lstinline[language=IndepStudLang]{value} must match the type the requested member was declared with, or a compiler error will be issued. \\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                 EXAMPLES                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \pagebreak
    \section{Examples}
        \subsection{Variable Declaration}
\begin{lstlisting}[language=IndepStudLang]
int myInt = -5;
float myFloat = 0.5;
\end{lstlisting}

        \subsection{Casting}
\begin{lstlisting}[language=IndepStudLang]
float f = 0.5f;
int i = cast(int, f); // i == 0
\end{lstlisting}

        \subsection{FizzBuzz}
\begin{lstlisting}[language=IndepStudLang]
import "stdio.isl";

for(int i = 0; i < 100; ++i);
    bool printnumber = true;
    if(i % 3 == 0);
        printf("Fizz");
        printnumber = false;
    endif;

    if(i % 5 == 0);
        printf("Buzz");
        printnumber = false;
    endif;

    if(printnumber);
        printf("%d", i);
    endif;
endfor;
\end{lstlisting}

    \subsection{IsPrime}
\begin{lstlisting}[language=IndepStudLang]
import "stdio.isl";

function isPrime(int n) -> bool;
    if(n == 1);
        return true;
    else;
        for(int i = 2; i < n; ++i);
            if(n % i == 0 && i != n);
                return false;
            endif;
        endfor;
    endif;

    return true;
endfunction;

printf("%d", isPrime(1)); // 1
printf("%d", isPrime(2)); // 1
printf("%d", isPrime(3)); // 1
printf("%d", isPrime(4)); // 0
printf("%d", isPrime(5)); // 1
printf("%d", isPrime(6)); // 0

\end{lstlisting}

\pagebreak
    \subsection{RecursiveBubbleSort}
\begin{lstlisting}[language=IndepStudLang]
import "utils.isl";

function bubbleSort(List int unsortedList, uint start) -> void
    if (Count(unsortedList) == 1);
        return;
    endif;

    for(uint i = start; i < Count(unsortedList) - 1; ++i);
        if(Get(unsortedList, i) > Get(unsortedList, i + 1));
            swap(unsortedList, i, i + 1);
        endif;
    endfor;

    bubbleSort(unsortedList, start + 1);
endfunction
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             COMPARISON TABLE                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    \includepdf[pages=-]{build/ComparisonsTable.pdf}

\end{document}
