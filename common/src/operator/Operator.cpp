/*!*****************************************************************************
 * @file   Operator.cpp
 * @author Tyler Robbins
 * 
 * @brief Defines operations that can be done on an operator.
 ******************************************************************************/

#include "operator/Operator.h"
#include "operator/Precedences.h"
#include "operator/Associativities.h"

/**
 * @brief Constructs an Operator
 * 
 * @param op The type of operator this object should represent.
 */
ISL::Common::Operator::Operator(const OperatorType& op):
    m_type(op)
{
}

/**
 * @brief Gets the type of operator this object represents.
 * 
 * @return The type of operator this object represents.
 */
const ISL::Common::OperatorType& ISL::Common::Operator::getOperatorType() const {
    return m_type;
}

/**
 * @brief Compares equality of this operator with another.
 * 
 * @param other The other operator to compare against.
 *
 * @return true if this operator is equal to other, false otherwise
 */
bool ISL::Common::Operator::operator==(const Operator& other) const {
    return m_type == other.getOperatorType();
}

/**
 * @brief Compares inequality of this operator with another.
 * 
 * @param other The other operator to compare against.
 *
 * @return true if this operator is not equal to other, false otherwise
 */
bool ISL::Common::Operator::operator!=(const Operator& other) const {
    return !((*this) == other);
}

int ISL::Common::Operator::getPrecedence() const {
    return getPrecedenceOf(m_type);
}

ISL::Common::Associativity ISL::Common::Operator::getAssociativity() const {
    return getAssociativityOf(m_type);
}

/**
 * @brief Outputs the given operator to the given ostream.
 * 
 * @param os The stream to output to.
 * @param op The operator to output.
 *
 * @return os after outputted to.
 */
std::ostream& operator<<(std::ostream& os, const ISL::Common::Operator& op) {
    os << op.getOperatorType();
    return os;
}

