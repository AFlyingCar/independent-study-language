#include "operator/Associativities.h"

auto ISL::Common::getAssociativityOf(const OperatorType& op_type) -> Associativity
{
    switch(op_type) {
        case OperatorType::INC: // Postfix
        case OperatorType::DEC:
        case OperatorType::NEGATIVE:
        case OperatorType::POSITIVE:
        case OperatorType::MUL:
        case OperatorType::DIV:
        case OperatorType::MOD:
        case OperatorType::ADD:
        case OperatorType::SUB:
        case OperatorType::BLSHIFT:
        case OperatorType::BRSHIFT:
        case OperatorType::LESS:
        case OperatorType::LESS_EQ:
        case OperatorType::GREATER:
        case OperatorType::GREATER_EQ:
        case OperatorType::EQUAL:
        case OperatorType::NOT_EQUAL:
        case OperatorType::BAND:
        case OperatorType::BXOR:
        case OperatorType::BOR:
        case OperatorType::LAND:
        case OperatorType::LOR:
        case OperatorType::PAREN_START:
        case OperatorType::PAREN_END:
            return Associativity::LEFT_TO_RIGHT;
        // case OperatorType::INC: // Prefix
        // case OperatorType::DEC:
        // case OperatorType::POSITIVE:
        // case OperatorType::NEGATIVE:
        case OperatorType::LNOT:
        case OperatorType::BNOT:
        case OperatorType::CAST:
        case OperatorType::TYPEOF:
        case OperatorType::SIZEOF:
        case OperatorType::ASSIGN:
        case OperatorType::ADD_ASSIGN:
        case OperatorType::SUB_ASSIGN:
        case OperatorType::MUL_ASSIGN:
        case OperatorType::DIV_ASSIGN:
        case OperatorType::MOD_ASSIGN:
        case OperatorType::LSHIFT_ASSIGN:
        case OperatorType::RSHIFT_ASSIGN:
        case OperatorType::BAND_ASSIGN:
        case OperatorType::BXOR_ASSIGN:
        case OperatorType::BOR_ASSIGN:
        case OperatorType::BNOT_ASSIGN:
            return Associativity::RIGHT_TO_LEFT;
        default:;
    }
}

