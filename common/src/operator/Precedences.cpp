
#include "operator/Precedences.h"

int ISL::Common::getPrecedenceOf(const OperatorType& op_type) {
    switch(op_type) {
        case OperatorType::INC: // Postfix
        case OperatorType::DEC:
            return 1;
        // case OperatorType::INC: // Prefix
        // case OperatorType::DEC:
        // case OperatorType::POSITIVE:
        // case OperatorType::NEGATIVE:
        case OperatorType::LNOT:
        case OperatorType::BNOT:
        case OperatorType::CAST:
        case OperatorType::TYPEOF:
        case OperatorType::SIZEOF:
            return 2;
        case OperatorType::MUL:
        case OperatorType::DIV:
        case OperatorType::MOD:
            return 3;
        case OperatorType::ADD:
        case OperatorType::SUB:
            return 4;
        case OperatorType::BLSHIFT:
        case OperatorType::BRSHIFT:
            return 5;
        case OperatorType::LESS:
        case OperatorType::LESS_EQ:
        case OperatorType::GREATER:
        case OperatorType::GREATER_EQ:
            return 6;
        case OperatorType::EQUAL:
        case OperatorType::NOT_EQUAL:
            return 7;
        case OperatorType::BAND:
            return 8;
        case OperatorType::BXOR:
            return 9;
        case OperatorType::BOR:
            return 10;
        case OperatorType::LAND:
            return 11;
        case OperatorType::LOR:
            return 12;
        case OperatorType::ADD_ASSIGN:
        case OperatorType::SUB_ASSIGN:
        case OperatorType::MUL_ASSIGN:
        case OperatorType::DIV_ASSIGN:
        case OperatorType::MOD_ASSIGN:
        case OperatorType::LSHIFT_ASSIGN:
        case OperatorType::RSHIFT_ASSIGN:
        case OperatorType::BAND_ASSIGN:
        case OperatorType::BXOR_ASSIGN:
        case OperatorType::BOR_ASSIGN:
        case OperatorType::BNOT_ASSIGN:
            return 14;
        case OperatorType::ASSIGN:
        case OperatorType::PAREN_START:
        case OperatorType::PAREN_END:
            return 0;
        default:
            return -1;
    }
}

