
#include "operator/OperatorTypes.h"
#include "Preprocessor.h"

std::ostream& operator<<(std::ostream& os, const ISL::Common::OperatorType& opt) {
    switch(opt) {
#define X(VALUE) case ISL::Common::OperatorType::VALUE: os << STR(VALUE); break;
        OPERATOR_ENTRIES
#undef X
    }
    return os;
}

