
#include "Utility.h"

#include <cctype>
#include <iterator>
#include <algorithm>

std::string ISL::Common::toLower(const std::string& s) {
    std::string s2;
    std::transform(s.begin(), s.end(), std::back_inserter(s2),
                   (int (*) (int))std::tolower);
    return s2;
}

bool ISL::Common::isOctDigit(int i) {
    return std::isdigit(i) && i != '8' && i != '9';
}

