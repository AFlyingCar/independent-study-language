
#include <iostream>

#include <unistd.h>

#include "Logger.h"
#include "Settings.h"

/**
 * @brief Checks if we can display ANSI escape codes through stdout.
 * @details Specifically, checks if the STDOUT file is a TTY.
 *
 * @return True if we can display ANSI through stdout, false otherwise.
 */
static bool isAnsiEnabledStdout() {
    static bool ansi_enabled = isatty(fileno(stdout));

    return ansi_enabled;
}

/**
 * @brief Checks if we can display ANSI escape codes through stderr.
 * @details Specifically, checks if the STDERR file is a TTY.
 *
 * @return True if we can display ANSI through stderr, false otherwise.
 */
static bool isAnsiEnabledStderr() {
    static bool ansi_enabled = isatty(fileno(stderr));

    return ansi_enabled;
}

void ISL::Common::Log::error(const std::string& message, bool newline) {
    std::cerr << (isAnsiEnabledStderr() ? "\33[31m" : "")
              << message
              << (isAnsiEnabledStderr() ? "\33[0m" : "")
              << (newline ? "\n" : "");
    std::cerr.flush();
}

void ISL::Common::Log::warn(const std::string& message, bool newline) {
    std::cerr << (isAnsiEnabledStderr() ? "\33[33m" : "")
              << message
              << (isAnsiEnabledStderr() ? "\33[0m" : "")
              << (newline ? "\n" : "");
    std::cerr.flush();
}

void ISL::Common::Log::info(const std::string& message, bool newline) {
    if(!getOptions().quiet) {
        std::cout << (isAnsiEnabledStdout() ? "\33[37m" : "")
                  << message
                  << (isAnsiEnabledStdout() ? "\33[0m" : "")
                  << (newline ? "\n" : "");
        std::cout.flush();
    }
}

void ISL::Common::Log::debug(const std::string& message, bool newline) {
    if(getOptions().verbose) {
        std::cout << (isAnsiEnabledStdout() ? "\33[34m" : "")
                  << message
                  << (isAnsiEnabledStdout() ? "\33[0m" : "")
                  << (newline ? "\n" : "");
        std::cout.flush();
    }
}

