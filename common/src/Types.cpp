
#include "Types.h"

#include <sstream>

std::string ISL::Common::typeToString(const BuiltinType& value) {
    return std::visit([](auto&& v) {
        using T = std::decay_t<decltype(v)>;

        if constexpr(std::is_same_v<T, BoolType>) {
            return "bool";
        } else if constexpr(std::is_same_v<T, CharType>) {
            return "char";
        } else if constexpr(std::is_same_v<T, UCharType>) {
            return "uchar";
        } else if constexpr(std::is_same_v<T, IntType>) {
            return "int";
        } else if constexpr(std::is_same_v<T, UIntType>) {
            return "uint";
        } else if constexpr(std::is_same_v<T, FloatType>) {
            return "float";
        } else if constexpr(std::is_same_v<T, DoubleType>) {
            return "double";
        } else if constexpr(std::is_same_v<T, StringType>) {
            return "string";
        } else {
            return "<UNKNOWN>";
        }
    }, value);
}

std::ostream& operator<<(std::ostream& stream, const ISL::Common::BuiltinType& value) {
    std::visit([&stream](auto&& v) {
        using T = std::decay_t<decltype(v)>;

        if constexpr(std::is_same_v<T, ISL::Common::BoolType>) {
            if(v) {
                stream << "true";
            } else {
                stream << "false";
            }
        } else if constexpr(std::is_same_v<T, ISL::Common::CharType>  ||
                            std::is_same_v<T, ISL::Common::UCharType> ||
                            std::is_same_v<T, ISL::Common::IntType>   ||
                            std::is_same_v<T, ISL::Common::UIntType>  ||
                            std::is_same_v<T, ISL::Common::FloatType> ||
                            std::is_same_v<T, ISL::Common::DoubleType>)
        {
            stream << [](auto&& v) {
                std::stringstream ss;
                ss << v;
                return ss;
                }(v).str();
        } else { // strings or otherwise
            stream << '"' << v << '"';
        }
    }, value);

    return stream;
}

