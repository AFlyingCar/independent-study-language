
#include <filesystem>
#include <vector>

namespace ISL::Common {
    /**
     * @brief All options for the program
     */
    struct Options {
        //! All files that are to be proccessed
        std::vector<std::filesystem::path> file_list;

        //! If verbose output should be enabled
        bool verbose = false;

        //! If non-error output should be suppressed
        bool quiet = false;
    };

    Options& getOptions();
}

