#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H

#define STR(X) #X

#define PRIM_CAT(X, Y) X ## Y
#define CAT(X, Y) PRIM_CAT(X, Y)

#endif

