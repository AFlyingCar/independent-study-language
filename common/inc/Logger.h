
#ifndef LOGGER_H
#define LOGGER_H

#include <string>

namespace ISL::Common::Log {
    void error(const std::string&, bool = true);
    void warn(const std::string&, bool = true);
    void info(const std::string&, bool = true);
    void debug(const std::string&, bool = true);
}

#endif

