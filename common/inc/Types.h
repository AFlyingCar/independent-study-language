
#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H

#include <cstdint>
#include <variant>
#include <string>

namespace ISL::Common {
    using VoidType = void;
    using BoolType = bool;
    using CharType = char;
    using UCharType = unsigned char;
    using IntType = std::int32_t;
    using UIntType = std::uint32_t;
    using FloatType = float;
    using DoubleType = double;
    using StringType = std::string;
    using PointerType = std::intptr_t;

    using BuiltinType = std::variant<BoolType, CharType, UCharType, IntType,
                                     UIntType, FloatType, DoubleType,
                                     StringType>;

    std::string typeToString(const BuiltinType&);
}

std::ostream& operator<<(std::ostream&, const ISL::Common::BuiltinType&);

#endif

