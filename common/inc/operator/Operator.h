/*!*****************************************************************************
 * @file   Operator.h
 * @author Tyler Robbins
 * 
 * @brief Declares how operators are represented at a high level.
 ******************************************************************************/

#ifndef OPERATOR_H
#define OPERATOR_H

#include "OperatorTypes.h"

#include <ostream> // std::ostream

#include "Associativities.h"

namespace ISL::Common {
    /**
     * @brief Defines the Operator class
     */
    class Operator {
        public:
            Operator(const OperatorType&);
            Operator(const Operator&) = default;

            const OperatorType& getOperatorType() const;

            bool operator==(const Operator&) const;
            bool operator!=(const Operator&) const;

            int getPrecedence() const;
            Associativity getAssociativity() const;

        private:
            //! The type of operator that this particular object represents
            OperatorType m_type;
    };
};

std::ostream& operator<<(std::ostream&, const ISL::Common::Operator&);

#endif

