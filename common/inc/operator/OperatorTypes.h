#ifndef OPERATOR_TYPES_H
#define OPERATOR_TYPES_H

#include "OperatorTypesEntries.h"
#include <ostream>

namespace ISL::Common {
    enum class OperatorType {
#define X(VALUE) VALUE ,
        OPERATOR_ENTRIES
#undef X
    };
}

std::ostream& operator<<(std::ostream&, const ISL::Common::OperatorType&);

#endif

