/*!*****************************************************************************
 * @file   OperatorTypesEntries.h
 * @author Tyler Robbins
 *
 * @brief Declares every type of operator using the X-macro pattern.
 ******************************************************************************/

#ifndef OPERATOR_TYPE_ENTRIES_H
#define OPERATOR_TYPE_ENTRIES_H

//! A macro for every function-like operator (operators that look like functions)
#define OPERATOR_FUNCTION_LIKE_ENTRIES \
    X(CAST)                            \
    X(TYPEOF)                          \
    X(SIZEOF)

#define OPERATOR_P1_ENTRIES \
    /* -----------------*/  \
    /* PRECEDENCE 1, LR */  \
    /* -----------------*/  \
    X(INC)                  \
    X(DEC)

#define OPERATOR_UNARY_ENTRIES \
    X(POSITIVE)                \
    X(NEGATIVE)

#define OPERATOR_BINARY_ENTRIES \
    X(LNOT) /* Logical Not */   \
    X(BNOT) /* Bitwise Not */   \
                                \
    /* -----------------*/      \
    /* PRECEDENCE 3, LR */      \
    /* -----------------*/      \
    X(MUL)                      \
    X(DIV)                      \
    X(MOD)                      \
                                \
    /* -----------------*/      \
    /* PRECEDENCE 4, LR */      \
    /* -----------------*/      \
    X(ADD)                      \
    X(SUB)                      \
                                \
    /* -----------------*/      \
    /* PRECEDENCE 5, LR */      \
    /* -----------------*/      \
    X(BLSHIFT)                  \
    X(BRSHIFT)                  \
                                \
    /* -----------------*/      \
    /* PRECEDENCE 6, LR */      \
    /* -----------------*/      \
    X(LESS)                     \
    X(LESS_EQ)                  \
    X(GREATER)                  \
    X(GREATER_EQ)               \
                                \
    /* -----------------*/      \
    /* PRECEDENCE 7, LR */      \
    /* -----------------*/      \
    X(EQUAL)                    \
    X(NOT_EQUAL)                \
                                \
    /* -----------------*/      \
    /* PRECEDENCE 8, LR */      \
    /* -----------------*/      \
    X(BAND)                     \
                                \
    /* -----------------*/      \
    /* PRECEDENCE 9, LR */      \
    /* -----------------*/      \
    X(BXOR)                     \
                                \
    /* ------------------*/     \
    /* PRECEDENCE 10, LR */     \
    /* ------------------*/     \
    X(BOR)                      \
                                \
    /* ------------------*/     \
    /* PRECEDENCE 11, LR */     \
    /* ------------------*/     \
    X(LAND)                     \
                                \
    /* ------------------*/     \
    /* PRECEDENCE 12, LR */     \
    /* ------------------*/     \
    X(LOR)                      \
                                \
    /* ------------------*/     \
    /* PRECEDENCE 14, RL */     \
    /* ------------------*/     \
    X(ASSIGN)                   \
    X(ADD_ASSIGN)               \
    X(SUB_ASSIGN)               \
    X(MUL_ASSIGN)               \
    X(DIV_ASSIGN)               \
    X(MOD_ASSIGN)               \
    X(LSHIFT_ASSIGN)            \
    X(RSHIFT_ASSIGN)            \
    X(BAND_ASSIGN)              \
    X(BXOR_ASSIGN)              \
    X(BOR_ASSIGN)               \
    X(BNOT_ASSIGN)

//! A macro for every operator type
#define OPERATOR_ENTRIES           \
    OPERATOR_P1_ENTRIES            \
    OPERATOR_UNARY_ENTRIES         \
                                   \
    /* -----------------*/         \
    /* PRECEDENCE 2, RL */         \
    /* -----------------*/         \
    OPERATOR_FUNCTION_LIKE_ENTRIES \
                                   \
    OPERATOR_BINARY_ENTRIES        \
                                   \
    /* ------------------*/        \
    /*   MISCELLANEOUS   */        \
    /* ------------------*/        \
    X(PAREN_START)                 \
    X(PAREN_END)

#endif
