#ifndef ASSOCIATIVITIES_H
#define ASSOCIATIVITIES_H

#include "OperatorTypes.h"

namespace ISL::Common {
    enum class Associativity {
        LEFT_TO_RIGHT,
        RIGHT_TO_LEFT
    };

    Associativity getAssociativityOf(const OperatorType&);
}

#endif

