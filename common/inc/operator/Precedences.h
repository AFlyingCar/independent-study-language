
#ifndef PRECEDENCES_H
#define PRECEDENCES_H

#include "OperatorTypes.h"

namespace ISL::Common {
    int getPrecedenceOf(const OperatorType&);
}

#endif

