#ifndef UTILITY_H
#define UTILITY_H

#include <string>
#include <vector>
#include <memory>

namespace ISL::Common {
    std::string toLower(const std::string&);
    bool isOctDigit(int); // Will only return true for: 0 1 2 3 4 5 6 7

    template<typename T>
    std::vector<T> subvec(const std::vector<T>& vec, size_t start, size_t rstart)
    {
        auto first = vec.cbegin() + start;
        auto last = vec.cend() - rstart;

        return std::vector<T>(first, last);
    }

    /**
     * @brief Performs the equivalent of a dynamic_cast on ptr.
     * @details If successful, the passed in ptr becomse invalidated.
     *
     * @par Code found here: https://stackoverflow.com/a/36120483 and here:
     *                       https://stackoverflow.com/a/26377517
     * 
     * @tparam ResultType The type to cast to.
     * @tparam FromType The type to cast from.
     * @param ptr The pointer to cast.
     *
     * @return A new unique_ptr containing the result of a dynamic_cast of the 
     *         the pointer stored in ptr to ResultType, or nullptr if the cast
     *         fails.
     */
    template<typename ResultType, typename FromType>
    auto unique_dynamic_cast(std::unique_ptr<FromType>&& ptr) {
        if(ResultType* cast = dynamic_cast<ResultType*>(ptr.get()); cast) {
            std::unique_ptr<ResultType> result(cast);
            ptr.release();
            return result;
        }

        return std::unique_ptr<ResultType>(nullptr);
    }
}

#endif

