
" Vim Syntax file for the Independent Study Language

if exists("b:current_syntax")
    finish
endif

" All keywords
syn keyword islBasicLanguageKeywords null true false cast sizeof return import extern break continue

syn keyword islBasicTypes bool void Pointer char uchar List int uint
syn keyword islBasicTypes String float double Any Structure Enum

syn keyword islBookendKeywords function endfunction if
syn keyword islBookendKeywords elseif else endif loop endloop while endwhile
syn keyword islBookendKeywords dowhile enddowhile for endfor structure
syn keyword islBookendKeywords endstructure enumeration endenumeration

" Any integer
syn match islNumber '\d\+'
syn match islNumber '[-+]\d\+'

" Any float
syn match islNumber '[-+]\d\+\.\d*'

" TODO: Do we want to support todo as a highlighted thing in comments?
syn match islComment "//.*$"
syn match islComment "/\*.*\*/"

" Set up regions for folding
syn region islDescBlock start="function" end="endfunction" fold transparent

" Different ways if statements can fold
syn region islDescBlock start="if" end="elseif" fold transparent
syn region islDescBlock start="if" end="else" fold transparent
syn region islDescBlock start="if" end="endif" fold transparent
syn region islDescBlock start="elseif" end="else" fold transparent
syn region islDescBlock start="elseif" end="endif" fold transparent
syn region islDescBlock start="else" end="endif" fold transparent

syn region islDescBlock start="loop" end="endloop" fold transparent
syn region islDescBlock start="while" end="endwhile" fold transparent
syn region islDescBlock start="dowhile" end="enddowhile" fold transparent
syn region islDescBlock start="for" end="endfor" fold transparent
syn region islDescBlock start="structure" end="endstructure" fold transparent
syn region islDescBlock start="enumeration" end="endenumeration" fold transparent

syn region islString start='"' end='"'

let b:current_syntax = "isl"

hi def link islComment Comment

hi def link islBasicLanguageKeywords Statement
hi def link islBookendKeywords Statement

hi def link islBasicTypes Type

hi def link islString Constant
hi def link islNumber Constant

