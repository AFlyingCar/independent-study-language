
#include <iostream>
#include <fstream>
#include <sstream>

#include "docopt.h"

#include "Tokenizer.h"
#include "Logger.h"
#include "Settings.h"

static const char LEXER_USAGE[] = 
R"(Independent Study Language (ISL) Lexer

    Usage:
      lexer <filenames>... [-v | --verbose | -q | --quiet] [-o <output_file>]

    Options:
      -h --help                                Show this message
      -v --verbose                             Use verbose output
      -q --quiet                               Use verbose output
      -o <output_file> --output=<output_file>  Specify the output file.
)";

int main(int argc, char** argv) {
    using namespace std::string_literals;

    // std::map<std::string, docopt::value>
    auto args = docopt::docopt(LEXER_USAGE, { argv + 1, argv + argc }, true,
                               "Independent Study Language (ISL) Lexer");

    // Should throw if this somehow _isn't_ a string
    std::vector<std::string> filenames = args.at("<filenames>").asStringList();

    ISL::Common::getOptions().verbose = args.at("--verbose").asBool();
    ISL::Common::getOptions().quiet = args.at("--quiet").asBool();

    
    std::string output_filename;
    if(auto opt = args.at("--output"); opt)
        output_filename = opt.asString();

    ISL::Common::Log::debug("lexer_frontend invoked with the following arguments:");
    std::stringstream arg_data;
    for(auto& arg : args) {
        arg_data << arg.first << "=" << arg.second << std::endl;
    }
    ISL::Common::Log::debug(arg_data.str());

    std::ofstream out_file;
    if(!output_filename.empty()) {
        if(out_file.open(output_filename); !out_file) {
            ISL::Common::Log::error("Failed to open output file \""s +
                                    output_filename + "\"");
            return 1;
        }
    }

    size_t num_frontend_errors = 0;
    for(auto& filename : filenames) {
        std::ifstream file(filename);

        if(file) {
            ISL::Common::Log::debug("Successfully opened \""s + filename + "\"");

            for(auto&& token : ISL::Lexer::tokenize(file)) {
                std::visit([&](auto&& value) {
                    using T = std::decay_t<decltype(value)>;

                    std::stringstream outstream;
                    outstream << value.getTokenType();

                    if constexpr(std::is_same_v<T, ISL::Lexer::IdentifierToken>) {
                        outstream << "=" << value.getIdentifier();
                    } else if constexpr(std::is_same_v<T, ISL::Lexer::ValueToken>) {
                        std::visit([&outstream](auto&& arg) { outstream << "=" << arg; },
                                   value.getValueType());
                    } else if constexpr(std::is_same_v<T, ISL::Lexer::OperatorToken>) {
                        outstream << "=" << value.getOperator();
                    }

                    if(out_file) {
                        out_file << outstream.str() << std::endl;
                    } else {
                        ISL::Common::Log::info(outstream.str());
                    }
                }, token);
            }
        } else {
            ISL::Common::Log::error("Failed to open file \""s + filename + "\"");
            ++num_frontend_errors;
        }
    }

    if(size_t total_errors = num_frontend_errors + ISL::Lexer::getErrorCount();
       total_errors > 0)
    {
        ISL::Common::Log::error(std::to_string(total_errors) + " errors generated.");
        return 1;
    }

    return 0;
}

