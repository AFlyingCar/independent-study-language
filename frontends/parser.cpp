
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>

#include "docopt.h"

#include "Parser.h"
#include "Logger.h"
#include "Settings.h"

#include "TokenTypes.h"
#include "Tokenizer.h"
#include "TokenizerError.h"

#include "ErrorCount.h"

#include "JSONASTPrinter.h"
#include "DotASTPrinter.h"

enum class OutputFormat {
    DOT,
    JSON
} output_format = OutputFormat::JSON;

static const char PARSER_USAGE[] = 
R"(Independent Study Language (ISL) Parser

    Usage:
      parser <filename> [-v | --verbose | -q | --quiet ] [ --dot | --json ]

    Options:
      -h --help     Show this message
      -v --verbose  Use verbose output
      -q --quiet    Use verbose output
      --dot         Output in dot format
      --json        Output in JSON format
)";

namespace ISL::Frontend::Parser {
    int read(std::istream& input_stream) {
        try {
            auto token_stream = ISL::Lexer::tokenize(input_stream);
            std::vector<Lexer::TokenVariant> tokens(token_stream.begin(),
                                                    token_stream.end());

            auto statements = ISL::Parser::parse(tokens);

            if(auto errors = ISL::Parser::getErrorCount(); errors > 0) {
                ISL::Common::Log::error(std::to_string(errors) + " errors generated.");
                return 1;
            }

#if 0
            std::cout << "[" << std::endl;

            size_t i = 0;
            for(auto&& statement : statements) {
                statement->toJSON(std::cout);

                if(i != statements.size() - 1)
                    std::cout << ',' << std::endl;
                ++i;
            }
            std::cout << "]" << std::endl;
#else
            switch(output_format)  {
                case OutputFormat::DOT:
                    ISL::Parser::DotASTPrinter(std::cout).traverseAST(statements);
                    break;
                case OutputFormat::JSON:
                    ISL::Parser::JSONASTPrinter(std::cout).traverseAST(statements);
                    break;
            }
#endif

            return 0;
        } catch(const ISL::Lexer::TokenizerError& e) {
            ISL::Common::Log::error(e.what());
            ISL::Common::Log::error(std::to_string(ISL::Parser::getErrorCount()) + " errors generated.");
            return 1;
        }
    }
}

int main(int argc, char** argv) {
    using namespace std::string_literals;

    auto args = docopt::docopt(PARSER_USAGE, { argv + 1, argv + argc }, true,
                               "Independent Study Language (ISL) Parser");

    std::string token_filename = args.at("<filename>").asString();

    ISL::Common::getOptions().verbose = args.at("--verbose").asBool();
    ISL::Common::getOptions().quiet = args.at("--quiet").asBool();

    if(args.at("--dot").asBool()) {
        output_format = OutputFormat::DOT;
    } else if(args.at("--json").asBool()) {
        output_format = OutputFormat::JSON;
    }

    ISL::Common::Log::debug("parser_frontend invoked with the following arguments:");
    std::stringstream arg_data;
    for(auto& arg : args) {
        arg_data << arg.first << "=" << arg.second << std::endl;
    }
    ISL::Common::Log::debug(arg_data.str());

    if(token_filename == "-") {
        return ISL::Frontend::Parser::read(std::cin);
    } else {
        std::filesystem::path token_file_path(token_filename);

        if(std::filesystem::exists(token_file_path)) {
            std::ifstream file(token_file_path);
            return ISL::Frontend::Parser::read(file);
        } else {
            ISL::Common::Log::error("File not found: "s + token_filename);
            return 1;
        }
    }

    return 0;
}

