
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>

#include "docopt.h"

#include "Parser.h"
#include "Logger.h"
#include "Settings.h"

#include "Tokenizer.h"

#include "ErrorCount.h"

#include "IRVisitor.h"
#include "CodeGen.h"

static const char CODEGEN_USAGE[] = 
R"(Independent Study Language (ISL) CodeGen

    Usage:
      codegen <filename> [-v | --verbose | -q | --quiet ] [ -o <output_filename> | --output=<output_filename> ] [ -i <import_prefix,...> | --import=<import_prefix,...> ] [ --dot | --json ]

    Options:
      -h --help                        Show this message
      -v --verbose                     Use verbose output
      -q --quiet                       Use verbose output
      -o --output <output_filename>    Specify where to write the output executable
      -i --import <import_prefix,...>  Add an additional prefix to the import search path.
)";

namespace ISL::Frontend::CodeGen {
    int read(std::istream& input_stream, const std::string& name) {

        return 0;
    }
}

int main(int argc, char** argv) {
    using namespace std::string_literals;

    auto args = docopt::docopt(CODEGEN_USAGE, { argv + 1, argv + argc }, true,
                               "Independent Study Language (ISL) CodeGen");

    std::string token_filename = args.at("<filename>").asString();

    ISL::Common::getOptions().verbose = args.at("--verbose").asBool();
    ISL::Common::getOptions().quiet = args.at("--quiet").asBool();

    std::string output_file;
    std::filesystem::path output_path;

    if(args.at("--output")) {
        output_file = args.at("--output").asString();
    }

    if(output_file.empty()) {
        output_path = "a.ll";
    } else {
        output_path = std::filesystem::absolute(output_file);
    }

    std::vector<std::filesystem::path> import_prefixes;
    if(args.at("--import")) {
        auto&& import_prefixes_raw = args.at("--import").asStringList();

        for(auto&& prefix : import_prefixes_raw) {
            import_prefixes.push_back(std::filesystem::absolute(prefix));
        }
    }

    ISL::Common::Log::debug("codegen_frontend invoked with the following arguments:");
    std::stringstream arg_data;
    for(auto& arg : args) {
        arg_data << arg.first << "=" << arg.second << std::endl;
    }
    ISL::Common::Log::debug(arg_data.str());

    if(token_filename == "-") {
        if(output_file == "-") {
            ISL::CodeGen::genCode("", "<STDIN>", std::cin, std::cout, import_prefixes);
        } else {
            std::ofstream ofile(output_path);
            ISL::CodeGen::genCode("", "<STDIN>", std::cin, ofile, import_prefixes);
        }
    } else {
        std::filesystem::path token_file_path(token_filename);

        if(std::filesystem::exists(token_file_path)) {
            std::ifstream file(token_file_path);
            if(output_file == "-") {
                ISL::CodeGen::genCode(token_filename, token_filename, file, std::cout, import_prefixes);
            } else {
                std::ofstream ofile(output_path);
                ISL::CodeGen::genCode(token_filename, token_filename, file, ofile, import_prefixes);
            }
            return 0;
        } else {
            ISL::Common::Log::error("File not found: "s + token_filename);
            return 1;
        }
    }

    return 0;
}

