/*!*****************************************************************************
 * @file   TokenTypeEntries.h * @author Tyler Robbins
 * 
 * @brief Declares every token type using the X-macro pattern.
 ******************************************************************************/

#ifndef TOKEN_TYPES_ENTRIES
#define TOKEN_TYPES_ENTRIES

#define TOKEN_KEYWORD_ENTRIES                                               \
    /* --------------------------------------------------------*/           \
    /* Keywords (not counting builtin-types or builtin-values) */           \
    /* --------------------------------------------------------*/           \
    X(TOK_FUNCTION)                                                         \
    X(TOK_ENDFUNCTION)                                                      \
                                                                            \
    /* Conditionals */                                                      \
    X(TOK_IF)                                                               \
    X(TOK_ELSEIF)                                                           \
    X(TOK_ELSE)                                                             \
    X(TOK_ENDIF)                                                            \
                                                                            \
    /* Loops */                                                             \
    X(TOK_LOOP)                                                             \
    X(TOK_ENDLOOP)                                                          \
                                                                            \
    X(TOK_WHILE)                                                            \
    X(TOK_ENDWHILE)                                                         \
                                                                            \
    X(TOK_DOWHILE)                                                          \
    X(TOK_ENDDOWHILE)                                                       \
                                                                            \
    X(TOK_FOR)                                                              \
    X(TOK_ENDFOR)                                                           \
                                                                            \
    /* Others */                                                            \
    X(TOK_STRUCTURE)                                                        \
    X(TOK_ENDSTRUCTURE)                                                     \
                                                                            \
    X(TOK_ENUMERATION)                                                      \
    X(TOK_ENDENUMERATION)                                                   \
                                                                            \
    X(TOK_RETURN)                                                           \
    X(TOK_IMPORT)                                                           \
    X(TOK_EXTERN)                                                           \
                                                                            \
    /* -------------------------*/                                          \
    /* Keywords (builtin-types) */                                          \
    /* -------------------------*/                                          \
    X(TOK_TYPE_BOOL)                                                        \
    X(TOK_TYPE_CHAR)                                                        \
    X(TOK_TYPE_UCHAR)                                                       \
    X(TOK_TYPE_INT)                                                         \
    X(TOK_TYPE_UINT)                                                        \
    X(TOK_TYPE_FLOAT)                                                       \
    X(TOK_TYPE_DOUBLE)                                                      \
    X(TOK_TYPE_VOID)                                                        \
    X(TOK_TYPE_POINTER)                                                     \
    X(TOK_TYPE_LIST)                                                        \
    X(TOK_TYPE_STRING)                                                      \
    X(TOK_TYPE_ANY)                                                         \
                                                                            \
    /* --------------------------*/                                         \
    /* Keywords (builtin-values) */                                         \
    /* --------------------------*/                                         \
    X(TOK_NULL)

#define TOKEN_FUNCTION_LIKE_OPERATOR_ENTRIES                                \
    X(TOK_CAST)                                                             \
    X(TOK_SIZEOF)                                                           \
    X(TOK_TYPEOF)

#define TOKEN_ENTRIES                                                       \
    /* End of File */                                                       \
    X(TOK_EOF)                                                              \
                                                                            \
    TOKEN_KEYWORD_ENTRIES                                                   \
                                                                            \
    /* -------*/                                                            \
    /* Values */                                                            \
    /* -------*/                                                            \
    X(TOK_BOOLEAN)                                                          \
    X(TOK_CHARACTER)                                                        \
    X(TOK_INTEGER)                                                          \
    X(TOK_FLOAT)                                                            \
    X(TOK_DOUBLE)                                                           \
    X(TOK_STRING)                                                           \
                                                                            \
    /* ----------*/                                                         \
    /* Operators */                                                         \
    /* ----------*/                                                         \
    X(TOK_SIMPLE_OPERATOR)                                                  \
                                                                            \
    /* For operators that "surround" their operands (List, call, etc...) */ \
    X(TOK_START_OPERATOR)                                                   \
    X(TOK_END_OPERATOR)                                                     \
    TOKEN_FUNCTION_LIKE_OPERATOR_ENTRIES                                    \
                                                                            \
    /* ---------*/                                                          \
    /* FUNCTIONS*/                                                          \
    /* ---------*/                                                          \
    X(TOK_FUNC_ARROW)                                                       \
    X(TOK_FUNC_ARG_END)  /* Used when a function is called. Only used by    \
                            parser */                                       \
    X(TOK_FUNC_ARG_START)                                                   \
                                                                            \
    /* User Identifier */                                                   \
    X(TOK_IDENTIFIER)                                                       \
                                                                            \
    /* Miscellaneous */                                                     \
    X(TOK_END_STATEMENT)                                                    \
    X(TOK_ARG_SEPERATOR)                                                    \
    X(TOK_START_CONSTRAINTS)                                                \
    X(TOK_END_CONSTRAINTS)

#endif
