#ifndef TOKEN_TYPES_H
#define TOKEN_TYPES_H

#include "TokenTypesEntries.h"

#include <ostream>

namespace ISL::Lexer {
    /**
     * @brief The type of tokens that can be found in a file
     */
    enum class TokenType {
#define X(ENTRY) ENTRY,
        TOKEN_ENTRIES
#undef X
        NUM_TOKENS
    };
}

std::ostream& operator<<(std::ostream&, const ISL::Lexer::TokenType&);

#endif

