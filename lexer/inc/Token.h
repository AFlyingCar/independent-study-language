/*!*****************************************************************************
 * @file   Token.h
 * @author Tyler Robbins
 * 
 * @brief Declares how tokens are represented at a high level.
 ******************************************************************************/

#ifndef TOKEN_H
#define TOKEN_H

#include <variant>
#include <string>

#include "TokenTypes.h"
#include "operator/Operator.h"
#include "Types.h"

namespace ISL::Lexer {
    /**
     * @brief The base token type. Used to represent miscellaneous tokens.
     */
    class Token {
        public:
            Token() = default;
            Token(const TokenType&);
            Token(const Token&) = default;

            const TokenType& getTokenType() const;

            size_t getLineNumber() const;
            const std::string& getLine() const;
            const std::string& getSubstr() const;

            virtual bool operator==(const Token&) const;
            virtual bool operator!=(const Token&) const;

        protected:
            void setLineNumber(size_t);
            void setLine(const std::string&);
            void setSubstr(const std::string&);

        private:
            //! The type of token this Token represents
            TokenType m_type;

            // For error-reporting
            size_t m_line_number;
            std::string m_line;
            std::string m_tok_substr;

        friend class TokenStreamIter;
    };

    /**
     * @brief A Token that represents an identifier
     */
    class IdentifierToken: public Token {
        public:
            IdentifierToken(const TokenType&, const std::string&);
            IdentifierToken(const IdentifierToken&) = default;

            const std::string& getIdentifier() const;

            virtual bool operator==(const IdentifierToken&) const;
            virtual bool operator!=(const IdentifierToken&) const;
            
        private:
            //! The identifier that this Token is referring to
            std::string m_identifier;
    };

    /**
     * A Token that represents a value
     */
    class ValueToken: public Token {
        public:
            //! The types of values that this ValueToken can hold
            using ValueType = Common::BuiltinType;

            ValueToken(const TokenType&, ValueType&&);
            ValueToken(const ValueToken&) = default;

            const ValueType& getValueType() const;

            virtual bool operator==(const ValueToken&) const;
            virtual bool operator!=(const ValueToken&) const;
        private:
            //! The value that this Token is referring to
            ValueType m_value;
    };

    /**
     * A Token that represents an operator
     */
    class OperatorToken: public Token {
        public:
            OperatorToken(const TokenType&, Common::Operator&&);
            OperatorToken(const OperatorToken&) = default;

            const Common::Operator& getOperator() const;

            virtual bool operator==(const OperatorToken&) const;
            virtual bool operator!=(const OperatorToken&) const;
        private:
            //! The operator this Token refers to
            Common::Operator m_operator;
    };

    /**
     * A Token that represents one end of a function's argument list.
     */
    class FuncArgEndToken: public Lexer::Token {
        public:
            FuncArgEndToken(const FuncArgEndToken&) = default;
            FuncArgEndToken();

            size_t& getParameterCountRef();
            size_t getParameterCount() const;

        private:
            size_t m_parameter_count;
    };

    class FuncArgStartToken: public Lexer::Token {
        public:
            FuncArgStartToken(const FuncArgStartToken&) = default;
            FuncArgStartToken(const std::string&);

            const std::string& getFunctionName() const;

        private:
            std::string m_func_name;
    };

    //! A variant of every type of Token class, so that they can be used more easily
    using TokenVariant = std::variant<Token, IdentifierToken, ValueToken, OperatorToken, FuncArgEndToken, FuncArgStartToken>;
}

#endif

