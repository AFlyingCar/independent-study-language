/*!*****************************************************************************
 * @file Tokenizer.h
 * @brief Declares how to use the Tokenizer
 * @author Tyler Robbins
 ******************************************************************************/

#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <istream>
#include <string>
#include <queue>
#include <map>

#include "Token.h"
#include "TokenTypes.h"

#include "Utility.h"
#include "Preprocessor.h"

namespace ISL::Lexer {
    //! X-macro
#define X(VALUE) { Common::toLower(&STR(VALUE)[4]), TokenType::VALUE },

    // NOTE: There are going to be some useless entries

    //! Defines a mapping of strings to tokens
    const std::map<std::string, TokenType> TOKEN_TEXT_MAP = {
        TOKEN_KEYWORD_ENTRIES
    };

    //! Defines a mapping of strings to operator tokens that look like functions
    const std::map<std::string, TokenType> TOKEN_FUNCTION_LIKE_OPERATOR_MAP = {
        TOKEN_FUNCTION_LIKE_OPERATOR_ENTRIES
    };
#undef X

    //! X-macro
#define X(VALUE) { Common::toLower(STR(VALUE)), Common::OperatorType::VALUE },
    //! Defines a mapping of strings to operators that look like functions
    const std::map<std::string, Common::OperatorType> OPERATOR_FUNCTION_LIKE_MAP = {
        OPERATOR_FUNCTION_LIKE_ENTRIES
    };
#undef X

    //! String to represent the simplest of operator
    const std::string SIMPLE_OPERATORS = "+-*/%<>=!&^|~";

    //! Defines a mapping of operator patterns to OperatorType
    const std::map<std::string, Common::OperatorType> OPERATOR_MAP = {
        { "++",  Common::OperatorType::INC },
        { "--",  Common::OperatorType::DEC },

        { "!",   Common::OperatorType::LNOT },
        { "~",   Common::OperatorType::BNOT },

        { "*",   Common::OperatorType::MUL },
        { "/",   Common::OperatorType::DIV },
        { "%",   Common::OperatorType::MOD },
        { "+",   Common::OperatorType::ADD },
        { "-",   Common::OperatorType::SUB },

        { "<<",  Common::OperatorType::BLSHIFT },
        { ">>",  Common::OperatorType::BRSHIFT },
        { "<",   Common::OperatorType::LESS },
        { "<=",  Common::OperatorType::LESS_EQ },
        { ">",   Common::OperatorType::GREATER },
        { ">=",  Common::OperatorType::GREATER_EQ },

        { "==",  Common::OperatorType::EQUAL },
        { "!=",  Common::OperatorType::NOT_EQUAL },

        { "&&",  Common::OperatorType::BAND },

        { "^",   Common::OperatorType::BXOR },

        { "|",   Common::OperatorType::BOR },

        { "&&",  Common::OperatorType::LAND },
        { "||",  Common::OperatorType::LOR },

        { "=",   Common::OperatorType::ASSIGN },
        { "+=",  Common::OperatorType::ADD_ASSIGN },
        { "-=",  Common::OperatorType::SUB_ASSIGN },
        { "*=",  Common::OperatorType::MUL_ASSIGN },
        { "/=",  Common::OperatorType::DIV_ASSIGN },
        { "%=",  Common::OperatorType::MOD_ASSIGN },
        { "<<=", Common::OperatorType::LSHIFT_ASSIGN },
        { ">>=", Common::OperatorType::RSHIFT_ASSIGN },
        { "&=",  Common::OperatorType::BAND_ASSIGN },
        { "^=",  Common::OperatorType::BXOR_ASSIGN },
        { "|=",  Common::OperatorType::BOR_ASSIGN },
        { "~=",  Common::OperatorType::BNOT_ASSIGN }
    };

    class TokenStream;

    /**
     * @brief An iterator for tokens
     */
    class TokenStreamIter {
        public:
            //! The maximum size of buffered text from the file
            const static size_t MAX_BUFFER_SIZE = 1024;

            //------------------------------------------------------------------
            // These aliases allow us to satisfy the LegacyInputIterator concept
            //------------------------------------------------------------------
            using difference_type = size_t;
            using value_type = TokenVariant;
            using pointer = value_type*;
            using reference = value_type&;
            using iterator_category = std::input_iterator_tag;

            TokenStreamIter(TokenStream&, TokenVariant&&);
            TokenStreamIter(TokenStream&);

            TokenStreamIter& operator++();
            TokenStreamIter& operator++(int);

            TokenVariant operator*() const;

            bool operator==(const TokenStreamIter&) const;
            bool operator!=(const TokenStreamIter&) const;

        protected:
            char getNextCharacter();
            void putbackCharacter();

        private:
            void parseNextToken();

            //! The TokenStream that this Iter is iterating over
            TokenStream& m_owning_stream;

            //! The last token we parsed
            TokenVariant m_last_token;

            std::queue<TokenVariant> m_tokens_queue;

            //! The buffer to store text from the input
            char m_buffer[MAX_BUFFER_SIZE];

            //! The current location in the buffer
            size_t m_buffer_idx;

            //! The total number of characters in the buffer
            size_t m_total_chars_in_buffer;

            //! The current line number
            size_t m_line_number;

            //! The current line
            std::string m_line;
    };

    /**
     * @brief A class for holding a stream of ISL tokens
     */
    class TokenStream {
        public:
            TokenStream(std::istream&);

            TokenStream(const TokenStream&) = delete;
            TokenStream(TokenStream&&) = delete;

            TokenStreamIter begin();
            TokenStreamIter end();

            std::istream& getStream();

        private:
            //! The input stream
            std::istream& m_stream;
    };

    TokenStream tokenize(std::istream&);
    size_t getErrorCount();
}

#endif

