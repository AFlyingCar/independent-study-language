
#include "Tokenizer.h"

#include <iostream>
#include <sstream>
#include <cctype> // std::isalpha, std::isalnum

#include "Logger.h"

#include "operator/Operator.h"

#include "TokenizerError.h"

//! The total number of errors that ocurred while tokenizing
static size_t num_errors = 0;

/**
 * @brief Constructs a TokenStreamIter
 *
 * @param tsp The owning TokenStream
 * @param lasttok The last token to set
 */
ISL::Lexer::TokenStreamIter::TokenStreamIter(TokenStream& tsp,
                                             TokenVariant&& lasttok):
    m_owning_stream(tsp),
    m_last_token(lasttok),
    m_buffer(),
    m_buffer_idx(MAX_BUFFER_SIZE),
    m_total_chars_in_buffer(0),
    m_line_number(1),
    m_line()
{ }

/**
 * @brief Constructs a TokenStreamIter
 *
 * @param tsp The owning TokenStream
 */
ISL::Lexer::TokenStreamIter::TokenStreamIter(TokenStream& tsp):
    m_owning_stream(tsp),
    m_last_token(Token()), // Doesn't matter what this one is, since we are
    m_buffer(),            //  overwriting it immediately
    m_buffer_idx(MAX_BUFFER_SIZE),
    m_total_chars_in_buffer(0),
    m_line_number(1),
    m_line()
{
    parseNextToken();
}

/**
 * @brief Increments this iterator to look at the next token
 *
 * @return A reference to this iterator
 */
ISL::Lexer::TokenStreamIter& ISL::Lexer::TokenStreamIter::operator++() {
    if(m_tokens_queue.empty()) {
        parseNextToken();
    } else {
        m_last_token = m_tokens_queue.front();
        m_tokens_queue.pop();
    }

    return *this;
}

/**
 * @brief Increments this iterator to look at the next token
 *
 * @return A reference to this iterator
 */
ISL::Lexer::TokenStreamIter& ISL::Lexer::TokenStreamIter::operator++(int) {
    if(m_tokens_queue.empty()) {
        parseNextToken();
    } else {
        m_last_token = m_tokens_queue.front();
        m_tokens_queue.pop();
    }

    return *this;
}

/**
 * @brief Gets the latest token
 *
 * @return The last parsed token.
 */
ISL::Lexer::TokenVariant ISL::Lexer::TokenStreamIter::operator*() const {
    return m_last_token;
}

/**
 * @brief Compares this iterator with another
 *
 * @param rhs The iterator to compare against
 *
 * @return Returns true if this iterator's token matches rhs
 */
bool ISL::Lexer::TokenStreamIter::operator==(const TokenStreamIter& rhs) const {
    return m_last_token == rhs.m_last_token;
}

/**
 * @brief Compares this iterator with another
 *
 * @param rhs The iterator to compare against
 *
 * @return Returns true if this iterator's token does not match rhs
 */
bool ISL::Lexer::TokenStreamIter::operator!=(const TokenStreamIter& rhs) const {
    return m_last_token != rhs.m_last_token;
}

/**
 * @brief Gets the next character in the buffer
 * @details If the buffer is empty, or if m_buffer_idx is at the end of the
 *          buffer, then the buffer is refilled from the stream
 *
 * @return The next character in the sequence.
 */
char ISL::Lexer::TokenStreamIter::getNextCharacter() {
    // If we are at the maximum size of the buffer, make sure that we refill it
    //  and reset the index
    if(m_buffer_idx >= MAX_BUFFER_SIZE) {
        auto& stream = m_owning_stream.getStream();
        m_buffer_idx = 0;
        stream.get(m_buffer, MAX_BUFFER_SIZE, EOF);

        // The total number of characters in the buffer is however many we last
        //  read
        m_total_chars_in_buffer = stream.gcount();

        // Output debugging information
        using namespace std::string_literals;
        Common::Log::debug("Read in "s + std::to_string(m_total_chars_in_buffer)
                           + " characters into buffer:");
        Common::Log::debug("\"\"\"");
        Common::Log::debug(m_buffer);
        Common::Log::debug("\"\"\"");
    }

    // If our index is currently at the total amount we last read (rather than
    //  at the maximum size of the buffer), then just return EOF
    if(m_buffer_idx == m_total_chars_in_buffer)
        return EOF;
    else {
        m_line += m_buffer[m_buffer_idx];
        return m_buffer[m_buffer_idx++];
    }
}

/**
 * @brief Puts the character back into the buffer
 */
void ISL::Lexer::TokenStreamIter::putbackCharacter() {
    if(!m_line.empty())
        m_line.pop_back();

    if(m_buffer_idx > 0)
        --m_buffer_idx;
}

/**
 * @brief Finds the next token in the stream
 */
void ISL::Lexer::TokenStreamIter::parseNextToken() {
    using namespace std::string_literals;
    // auto& stream = m_owning_stream.getStream();

    // Keep going until we've reached the end of this section of "spaces"
    auto last = ' ';
    for(; std::isspace(last); last = getNextCharacter()) {
        if(last == '\r' || last == '\n') {
            m_line = "";
            ++m_line_number;
        }
    }

    // The string we store the current token in (as it is read)
    std::string token_string;

    // Check for comments
    // Note: comments technically both do and don't count as a token, as they
    //  can only occur outside of a token, but are not returned as one
    // Also, comments can happen multiple times in a row, so we have to make
    //  sure to keep checking for them even after we finish parsing one
    for(;last == '/';) {
        char orig = last;
        last = getNextCharacter();

        if(last == '/') {
            // If we see "//", then walk until the first \n character
            while(last != '\n' && last != '\r') {
                last = getNextCharacter();
                ++m_line_number;
            }

            // Go ahead and re-run the space catching algorithm
            for(; std::isspace(last); last = getNextCharacter()) {
            }
        } else if(last == '*') {
            // If we see "/*" then walk until the matching "*/"
            char prev = last;
            for(last = getNextCharacter(); !(prev == '*' && last == '/');
                prev = last, last = getNextCharacter())
            {
                if(last == EOF)
                    throw TokenizerError("Expected */ before EOF");
            }

            // Go ahead and re-run the space catching algorithm
            for(last = getNextCharacter(); std::isspace(last);
                last = getNextCharacter())
            {
            }
        } else {
            putbackCharacter();
            last = orig;
            break;
        }
    }

    char first = last;

    if(std::isalpha(last)) { // Identifier
        for(; std::isalnum(last) || last == '_'; last = getNextCharacter())
            token_string += last;
        /*if(TOKEN_TEXT_MAP.count(token_string) != 0) {
            m_last_token = Token(TOKEN_TEXT_MAP.at(token_string));
        } else */if(TOKEN_FUNCTION_LIKE_OPERATOR_MAP.count(token_string) != 0) {
            m_last_token = OperatorToken(TOKEN_FUNCTION_LIKE_OPERATOR_MAP.at(token_string),
                                         Common::Operator(OPERATOR_FUNCTION_LIKE_MAP.at(token_string)));
        } else {
            m_last_token = IdentifierToken(TokenType::TOK_IDENTIFIER,
                                           token_string);
        }
        putbackCharacter();
    } else if(std::isdigit(last)) {
        for(; std::isdigit(last); last = getNextCharacter())
            token_string += last;
        bool is_hex = false;
        bool is_oct = false;
        if(token_string == "0") {
            switch(last) {
                case 'x':
                    is_hex = true; break;
                case 'X':
                    is_oct = true; break;
                case 'b': // TODO: Do we want to support binary numbers?
                default:; // Do nothing
            }

            if(is_hex) {
                for(; std::isxdigit(last); last = getNextCharacter())
                    token_string += last;
            } else if(is_oct) {
                for(; Common::isOctDigit(last); last = getNextCharacter())
                    token_string += last;
            }
        }

        // Either a double or a float
        if(last == '.') {
            if(is_hex) {
                // TODO: Raise an error if the first part was hex
            }

            token_string += last; // Add the decimal
            // Parse the next part of the decimal
            for(last = getNextCharacter(); std::isdigit(last); last = getNextCharacter())
                token_string += last;

            // This means that it is a float
            if(last == 'f') {
                m_last_token = ValueToken(TokenType::TOK_FLOAT,
                                          (float)std::atof(token_string.c_str()));
            } else {
                m_last_token = ValueToken(TokenType::TOK_DOUBLE,
                                          std::atof(token_string.c_str()));
                putbackCharacter();
            }
        } else {
            m_last_token = ValueToken(TokenType::TOK_INTEGER,
                                      std::stoi(token_string.c_str(),
                                                  nullptr,
                                                  is_hex ? 16 : 10));
            putbackCharacter();
        }
    } else if(last == '\"') {
        char prev = '\\';
        // Keep walking until we see another " that is not preceded by a backslash
        // TODO: Support escaping characters
        for(last = getNextCharacter(); !(last == '\"' && prev != '\\');
            prev = last, last = getNextCharacter())
        {
            token_string += last;
        }

        m_last_token = ValueToken(TokenType::TOK_STRING, token_string);
    } else if(last == '\'') {
        last = getNextCharacter();
        if(last == '\'') {
            Common::Log::error("Unexpected end of quotation.");
            ++::num_errors;
        } else {
            char c = last;
            last = getNextCharacter();

            // TODO: Support escaping characters
            if(last != '\'') {
                Common::Log::error("Expected end of quotation. Got "s + last +
                                   " instead.");
                ++::num_errors;
            } else {
                m_last_token = ValueToken(TokenType::TOK_CHARACTER, c);
            }
        }
    } else if(SIMPLE_OPERATORS.find(last) != std::string::npos) {
        std::string full_operator;
        full_operator += last;

        // Check if this operator is longer than one character
        last = getNextCharacter();
        if(SIMPLE_OPERATORS.find(last) != std::string::npos) {
            full_operator += last;
        } else {
            putbackCharacter();
        }

        // -> looks like an operator, but it's not
        if(full_operator == "->") {
            m_last_token = Token(TokenType::TOK_FUNC_ARROW);
        } else {
            m_last_token = OperatorToken(TokenType::TOK_SIMPLE_OPERATOR,
                                         Common::Operator(OPERATOR_MAP.at(full_operator)));
        }
    } else if(last == '(') {
        m_last_token = OperatorToken(TokenType::TOK_START_OPERATOR,
                                     Common::Operator(Common::OperatorType::PAREN_START));
    } else if(last == ')') {
        m_last_token = OperatorToken(TokenType::TOK_END_OPERATOR,
                                     Common::Operator(Common::OperatorType::PAREN_END));
    } else if(last == ',') {
        m_last_token = Token(TokenType::TOK_ARG_SEPERATOR);
    } else if(last == ';') {
        m_last_token = Token(TokenType::TOK_END_STATEMENT);
    } else if(last == '[') {
        m_last_token = Token(TokenType::TOK_START_CONSTRAINTS);
    } else if(last == ']') {
        m_last_token = Token(TokenType::TOK_END_CONSTRAINTS);
    } else if(last == EOF) {
        m_last_token = Token(TokenType::TOK_EOF);
        return;
    } else {
        Common::Log::error("Unknown character: '"s + last + "'");
        ++::num_errors;
        return;
    }

    auto line = m_line;
    auto line_number = m_line_number;
    std::visit([first, line, line_number, token_string](auto&& token) {
        using namespace std::string_literals;
        if(!token_string.empty())
            token.setSubstr(token_string);
        else
            token.setSubstr(""s + first);
        token.setLine(line);
        token.setLineNumber(line_number);
    }, m_last_token);
}

// -----------------------------------------------------------------------------

/**
 * @brief Constructs a TokenStream
 *
 * @param The stream we are getting tokens from.
 */
ISL::Lexer::TokenStream::TokenStream(std::istream& stream):
    m_stream(stream)
{ }

/**
 * @brief Gets the beginning of the stream
 *
 * @return An iterator to the beginning of the stream.
 */
ISL::Lexer::TokenStreamIter ISL::Lexer::TokenStream::begin() {
    return TokenStreamIter(*this);
}

/**
 * @brief Gets the end of the stream
 *
 * @return An iterator to the end of the stream.
 */
ISL::Lexer::TokenStreamIter ISL::Lexer::TokenStream::end() {
    return TokenStreamIter(*this, TokenType::TOK_EOF);
}

/**
 * @brief Gets the istream
 *
 * @return The istream
 */
std::istream& ISL::Lexer::TokenStream::getStream() {
    return m_stream;
}

/**
 * @brief Creates a TokenStream from the given istream
 *
 * @return A TokenStream representation of the given input stream
 */
ISL::Lexer::TokenStream ISL::Lexer::tokenize(std::istream& stream) {
    return TokenStream(stream);
}

/**
 * @brief Gets the number of errors that occurred since the start of the program
 *
 * @return The total number of errors that occurred.
 */
size_t ISL::Lexer::getErrorCount() {
    return ::num_errors;
}

