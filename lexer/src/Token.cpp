
#include "Token.h"

/**
 * @brief Constructs a Token
 *
 * @param type The type of token this is supposed to represent.
 */
ISL::Lexer::Token::Token(const TokenType& type): m_type(type) {
}

const ISL::Lexer::TokenType& ISL::Lexer::Token::getTokenType() const {
    return m_type;
}

bool ISL::Lexer::Token::operator==(const Token& other) const {
    return m_type == other.getTokenType();
}

size_t ISL::Lexer::Token::getLineNumber() const {
    return m_line_number;
}

const std::string& ISL::Lexer::Token::getLine() const {
    return m_line;
}

const std::string& ISL::Lexer::Token::getSubstr() const {
    return m_tok_substr;
}

void ISL::Lexer::Token::setLineNumber(size_t line_number) {
    m_line_number = line_number;
}

void ISL::Lexer::Token::setLine(const std::string& line) {
    m_line = line;
}

void ISL::Lexer::Token::setSubstr(const std::string& tok_substr) {
    m_tok_substr = tok_substr;
}

bool ISL::Lexer::Token::operator!=(const Token& other) const {
    return m_type != other.getTokenType();
}

ISL::Lexer::IdentifierToken::IdentifierToken(const TokenType& type,
                                             const std::string& iden):
    Token(type), m_identifier(iden)
{ }

const std::string& ISL::Lexer::IdentifierToken::getIdentifier() const {
    return m_identifier;
}

bool ISL::Lexer::IdentifierToken::operator==(const IdentifierToken& other) const {
    return Token::operator==(other) && m_identifier == other.getIdentifier();
}

bool ISL::Lexer::IdentifierToken::operator!=(const IdentifierToken& other) const {
    return !((*this) == other);
}

ISL::Lexer::ValueToken::ValueToken(const TokenType& type, ValueType&& value):
    Token(type), m_value(value)
{ }

const ISL::Lexer::ValueToken::ValueType& ISL::Lexer::ValueToken::getValueType() const
{
    return m_value;
}

bool ISL::Lexer::ValueToken::operator==(const ValueToken& other) const {
    return Token::operator==(other) && m_value == other.getValueType();
}

bool ISL::Lexer::ValueToken::operator!=(const ValueToken& other) const {
    return !((*this) == other);
}

ISL::Lexer::OperatorToken::OperatorToken(const TokenType& type, Common::Operator&& op):
    Token(type), m_operator(std::forward<Common::Operator>(op))
{ }

const ISL::Common::Operator& ISL::Lexer::OperatorToken::getOperator() const {
    return m_operator;
}

bool ISL::Lexer::OperatorToken::operator==(const OperatorToken& other) const {
    return Token::operator==(other) && m_operator == other.getOperator();
}

bool ISL::Lexer::OperatorToken::operator!=(const OperatorToken& other) const {
    return !((*this) == other);
}


ISL::Lexer::FuncArgEndToken::FuncArgEndToken():
    Token(TokenType::TOK_FUNC_ARG_END),
    m_parameter_count(0)
{ }

size_t& ISL::Lexer::FuncArgEndToken::getParameterCountRef() {
    return m_parameter_count;
}

size_t ISL::Lexer::FuncArgEndToken::getParameterCount() const {
    return m_parameter_count;
}

ISL::Lexer::FuncArgStartToken::FuncArgStartToken(const std::string& name):
    Token(TokenType::TOK_FUNC_ARG_START),
    m_func_name(name)
{ }

const std::string& ISL::Lexer::FuncArgStartToken::getFunctionName() const {
    return m_func_name;
}

