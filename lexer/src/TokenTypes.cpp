
#include "TokenTypes.h"
#include "Preprocessor.h"

std::ostream& operator<<(std::ostream& os, const ISL::Lexer::TokenType& type) {
    switch(type) {
#define X(VALUE) case ISL::Lexer::TokenType:: VALUE : os << STR(VALUE); break;
        TOKEN_ENTRIES
#undef X
        case ISL::Lexer::TokenType::NUM_TOKENS:
            os << "<UNKNOWN_TOK>";
    }

    return os;
}

