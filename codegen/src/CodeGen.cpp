
#include "CodeGen.h"

#include <ostream>
#include <istream>
#include <fstream>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"

#include "TokenizerError.h"
#include "Tokenizer.h"
#include "Parser.h"
#include "ErrorCount.h"
#include "Logger.h"

#include "IRVisitor.h"
#include "LLVMOstream.h"

void ISL::CodeGen::genCode(const std::filesystem::path& path,
                           const std::string& name,
                           const std::vector<Parser::AST::ExpressionPtr>& ast,
                           std::ostream& out,
                           const std::vector<std::filesystem::path>& import_prefixes)
{
    MainIRVisitor ir_visitor(name, std::filesystem::absolute(path));

    // Register all prefixes with the IRVisitor
    for(auto&& prefix : import_prefixes) {
        ir_visitor.addImportPrefix(prefix);
    }

    ir_visitor.traverseAST(ast);
    ir_visitor.generateMainFunction();

    IRVisitor::ModulePtrRef module = ir_visitor.getModule();

    LLVMOstream llvm_ostream(out);
    module->print(llvm_ostream, nullptr);
}

bool ISL::CodeGen::genCode(const std::filesystem::path& path,
                           const std::string& module_name,
                           std::ostream& out,
                           const std::vector<std::filesystem::path>& import_prefixes)
{
    std::ifstream file(path);
    return genCode(path, module_name, file, out, import_prefixes);
}

bool ISL::CodeGen::genCode(const std::filesystem::path& path,
                           const std::string& module_name,
                           std::istream& input_stream,
                           std::ostream& out,
                           const std::vector<std::filesystem::path>& import_prefixes)
{
    try {
        auto token_stream = Lexer::tokenize(input_stream);
        std::vector<Lexer::TokenVariant> tokens(token_stream.begin(),
                                                token_stream.end());

        auto statements = Parser::parse(tokens);

        if(auto errors = Parser::getErrorCount(); errors > 0) {
            Common::Log::error(std::to_string(errors) + " errors generated.");
            return false;
        }

        genCode(path, module_name, statements, out, import_prefixes);

        return true;
    } catch(const Lexer::TokenizerError& e) {
        ISL::Common::Log::error(e.what());
        ISL::Common::Log::error(std::to_string(ISL::Parser::getErrorCount()) + " errors generated.");
        return false;
    }
}

