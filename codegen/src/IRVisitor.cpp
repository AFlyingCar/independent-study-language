
#include "IRVisitor.h"

#include <fstream> // TODO: Do we actually want to write imported files to the disk?
#include <sstream>
#include <thread>

#include "Logger.h"

#include "TokenizerError.h"
#include "Tokenizer.h"
#include "Parser.h"
#include "ErrorCount.h"

#include "LLVMOstream.h"

#include "CodeGen.h"

namespace {
    /**
     * @brief Creates an LLVM FunctionType based on the return type and arguments.
     *
     * @param ret_type The return type
     * @param arg_types The parameter types
     *
     * @return A FunctionType.
     */
    llvm::FunctionType* createFunctionType(llvm::Type* ret_type,
                                           std::vector<llvm::Type*> arg_types)
    {
        return llvm::FunctionType::get(ret_type, arg_types, false);
    }

    /**
     * @brief Creates an LLVM Function in the given module.
     *
     * @param module The module to create the function inside of.
     * @param name The name of the function
     * @param ftype The type for the function
     * @param arg_names The name of each argument.
     *
     * @return The function's type and the function
     */
    std::pair<llvm::FunctionType*, llvm::Function*>
        createFunction(llvm::Module* module, const std::string& name,
                       llvm::FunctionType* ftype,
                       const std::vector<std::string>& arg_names)
    {
#if 0
        llvm::Function* func = llvm::Function::Create(ftype,
                                                      llvm::Function::ExternalLinkage,
                                                      name, module);
#else
        auto* func = (llvm::Function*)module->getOrInsertFunction(name, ftype).getCallee();
#endif
        auto i = 0;
        for(auto& arg : func->args())
            arg.setName(arg_names.at(i++));

        return { ftype, func };
    }

    /**
     * @brief Creates an LLVM function in the given module.
     *
     * @param module The module to create the function inside of.
     * @param name The name of the function
     * @param ret_type The return type
     * @param arg_types Each argument type
     * @param arg_names The name of each argument
     *
     * @return The function's type and the function
     */
    std::pair<llvm::FunctionType*, llvm::Function*>
        createFunction(llvm::Module* module, const std::string& name,
                       llvm::Type* ret_type,
                       std::vector<llvm::Type*> arg_types,
                       const std::vector<std::string>& arg_names)
    {
        llvm::FunctionType* ftype = createFunctionType(ret_type, arg_types);
        return createFunction(module, name, ftype, arg_names);
    }

    /**
     * @brief Creates an alloca instruction at the top of the current function
     *
     * @param func The current function
     * @param type The type to create an alloca for.
     * @param name The name of the variable to assign the alloca to
     *
     * @return an Alloca instruction
     */
    llvm::AllocaInst* createAllocaAtEntry(llvm::Function* func,
                                          llvm::Type* type,
                                          const std::string& name)
    {
        llvm::IRBuilder<> tmp(&func->getEntryBlock(),
                              func->getEntryBlock().begin());
        return tmp.CreateAlloca(type, nullptr, name);
    }

    /**
     * @brief Creates a string containing N tabs
     *
     * @param depth The number of tabs to create.
     *
     * @return a string containing N tabs.
     */
    std::string tabs(size_t depth) {
        return std::string(depth == 0 ? 0 : depth - 1, '\t');
    }

    // Will attempt to decay an array value to a pointer value
    // https://stackoverflow.com/questions/38548680/confused-about-llvm-arrays
    // https://github.com/Microsoft/llvm/blob/master/examples/BrainF/BrainF.cpp#L166
    llvm::Value* decayConstant(llvm::Constant* value,
                               llvm::LLVMContext& context)
    {
        auto* null_int32 = llvm::Constant::getNullValue(llvm::IntegerType::getInt32Ty(context));
        llvm::Constant* params[] = { null_int32 };

        // while(v->getType()->isArrayTy()) {
            ISL::Common::Log::debug("Decaying value from array to pointer");
            value = llvm::ConstantExpr::getGetElementPtr(value->getType(),
                                                         value, params);
        // }

        return value;
    }
}

// By default we just print the imported file 
const ISL::CodeGen::IRVisitor::ImportedFileWriterType
    ISL::CodeGen::IRVisitor::DEFAULT_IMPORTED_FILE_WRITER = [](ModulePtrRef module,
                                                               std::ostream& out)
{
    ISL::CodeGen::LLVMOstream llvm_ostream(out);
    module->print(llvm_ostream, nullptr);
    return true;
};

/**
 * @brief Constructs an IRVisitor
 *
 * @param name The name of the module.
 * @param path The path to where the module lives.
 */
ISL::CodeGen::IRVisitor::IRVisitor(const std::string& name,
                                   const std::filesystem::path& path):
    m_values(),
    m_blocks(),
    m_name(name),
    m_path(path),
    m_depth(0),
    m_import_prefixes{ path.parent_path() },
    m_imported()
{
}

/**
 * @brief Traverses the AST
 *
 * @param ast The AST to traverse.
 *
 * @return true on success, false otherwise.
 */
bool ISL::CodeGen::IRVisitor::traverseAST(const std::vector<Parser::AST::ExpressionPtr>& ast)
{
    // Create the initial symbol table.
    m_blocks.push({nullptr, SymbolTable(), nullptr});

    // Create some default values
    getSymbols().push_back({
        "true", {
            llvm::ConstantInt::get(llvm::Type::getInt1Ty(getContext()), 1),
            getTypeExprToWrappedType("bool")
        }
    });
    getSymbols().push_back({
        "false", {
            llvm::ConstantInt::get(llvm::Type::getInt1Ty(getContext()), 0),
            getTypeExprToWrappedType("bool")
        }
    });

    // Create the module-main function
    std::vector<std::string> arg_names {
        "argc",
        "argv"
    };

    llvm::FunctionType* ftype = llvm::FunctionType::get(
            llvm::Type::getVoidTy(getContext()),
            {
                llvm::Type::getInt32Ty(getContext()),
                llvm::PointerType::get(llvm::PointerType::get(llvm::IntegerType::get(getContext(), 8), 0), 0),
                // llvm::Type::getInt8PtrTy(getContext()),
            }, true);

    auto [_,main_func] = createFunction(getModule(), m_name + "_main",
                                        ftype, arg_names);

    m_blocks.top().block = llvm::BasicBlock::Create(getContext(), "entry", main_func);
    getBuilder().SetInsertPoint(m_blocks.top().block);

    auto result = Parser::ASTVisitor::traverseAST(ast);

    getBuilder().CreateRetVoid();

    return result;
}

void ISL::CodeGen::IRVisitor::traverseSubExprGroupStart(const std::string&) {
    // TODO: Start a basic block
}

/**
 * @brief Ends a sub-expression group
 */
void ISL::CodeGen::IRVisitor::traverseSubExprGroupEnd(const std::string&) {
    if(!m_call_on_group_end.empty()) {
        m_call_on_group_end.back()();
        m_call_on_group_end.pop();
    }
}

void ISL::CodeGen::IRVisitor::traverseSubExprEnd(const std::string&) {
    if(!m_values.empty()) {
#if 0
        TypeCheckedValue tcv = m_values.top();
        m_values.pop();

        Common::Log::debug("Inserting value into builder.");
        if(tcv.value != nullptr)
            getBuilder().Insert(tcv.value);
#endif
    }
}

/**
 * @brief Sets the current imported-file writer
 *
 * @param ifw the new ImportedFileWriter
 */
void ISL::CodeGen::IRVisitor::setImportFileWriter(const ImportedFileWriterType& ifw)
{
    m_imported_file_writer = ifw;
}

/**
 * @brief Handles importing a file
 *
 * @param file The file to import
 *
 * @return true on success, false otherwise.
 */
bool ISL::CodeGen::IRVisitor::importFile(const std::string& file) {
    // TODO: Get root of current file and make sure everythign is relative to that
    bool success = true;

    std::filesystem::path abs_file_path;

    // Find the file to import
    for(auto&& prefix : m_import_prefixes) {
        if(std::filesystem::exists(prefix / file)) {
            abs_file_path = prefix / file;
            break;
        }
    }
    if(abs_file_path.empty()) {
        using namespace std::string_literals;
        Common::Log::error("Cannot import '"s + file + "'. File does not exist.");
        return false;
    }

    // Check to make sure we don't import a file twice
    if(std::find(m_imported.begin(), m_imported.end(), abs_file_path) == m_imported.end())
    {
        // Do this first, because even if the import fails, we don't want to try
        //   importing it again
        m_imported.push_back(abs_file_path);

        // Make local-references so that we can pass them into the lambda
        SymbolTable& symbols = getSymbols();
        ModulePtrRef module = getModule();
        auto& context = getContext();
        auto& prefixes = m_import_prefixes;
        auto& import_file_writer = m_imported_file_writer;
        auto depth = m_depth;

        // Note: We do this in a thread so that each import gets its own stack
        //  We do this to prevent a stack overflow
        // Another option is to do a fork(), but that's not standard C/C++
        Common::Log::debug(::tabs(m_depth) + "Importing file from " + abs_file_path.generic_string());
        Common::Log::debug(::tabs(m_depth) + "Writing intermediate file to " + (abs_file_path.parent_path() / (abs_file_path.stem().generic_string() + ".ll")).generic_string());
        std::thread([&abs_file_path, &file, &success, &symbols, &module, &depth,
                     &context, &prefixes, &import_file_writer]()
        {
            std::ofstream module_out(abs_file_path.parent_path() / (abs_file_path.stem().generic_string() + ".ll"));

            // Note: We don't call codegen here because we want modules to be
            //  handled specially: No main(), and we need to be able to access
            //  the IRVisitor so we can get the symbol table back out
            std::ifstream input_stream(abs_file_path);
            std::vector<Lexer::TokenVariant> tokens;

            // Tokenize first, perform error checking
            try {
                auto token_stream = Lexer::tokenize(input_stream);
                tokens = decltype(tokens)(token_stream.begin(),
                                          token_stream.end());
            } catch(const Lexer::TokenizerError& e) {
                Common::Log::error(e.what());
                Common::Log::error(std::to_string(Lexer::getErrorCount()) + " errors generated.");
                success = false;
                return;
            }

            // Parse the imported file
            auto ast = Parser::parse(tokens);

            if(auto errors = Parser::getErrorCount(); errors > 0) {
                Common::Log::error(std::to_string(errors) + " errors generated.");
                success = false;
                return;
            }

            // Special IRVisitor that does not generate its own context
            ModuleIRVisitor ir_visitor(context, file, abs_file_path);

            // Tell the other IRVisitor about the import prefixes in case it wants
            //  to import things as well
            for(auto&& prefix : prefixes) {
                ir_visitor.addImportPrefix(prefix);
            }

            // Now perform codegen on the other visitor
            if(!ir_visitor.traverseAST(ast)) {
                success = false;
                return;
            }

            ModulePtrRef imported_module = ir_visitor.getModule();

            // Perform an action on the module
            if(!import_file_writer(imported_module, module_out)) {
                Common::Log::error("Failed to write imported module!");
            }

            Common::Log::debug(::tabs(depth) + "Importing symbols.");
            for(auto&& symbol : ir_visitor.getSymbols()) {
                // We can only import functions
                if(symbol.second.type.type->getMetaType() == Type::MetaType::FUNCTION) {
                    Common::Log::debug(::tabs(depth) + "Importing function '" + symbol.first + "'");

                    // Reconstruct the function type, since it seems to be
                    //  broken when we import it
                    llvm::FunctionType* oldftype = (llvm::FunctionType*)symbol.second.type.llvm;

                    llvm::FunctionType* ftype = llvm::FunctionType::get(oldftype->getReturnType(), oldftype->params(), false);

                    // Insert the symbol into our symbol table
                    symbols.push_back({ symbol.first, { symbol.second.value,
                                                        { ftype,
                                                          symbol.second.type.type },
                                                        symbol.second.name }});

                    // Insert the function into llvm
                    module->getOrInsertFunction(symbol.first, ftype);
                } else {
                    Common::Log::debug(::tabs(depth) + "Skipping symbol '" + symbol.first + "'");
                }
            }
            Common::Log::debug(::tabs(depth) + "Done.");
        }).join();
    }

    return success;
}

/**
 * @brief Get the next node in the tree (if it exists)
 *
 * @return An optional reference to the next node
 */
auto ISL::CodeGen::IRVisitor::getNextNode() const
    -> std::optional<std::reference_wrapper<const Parser::AST::ExpressionPtr>>
{
    auto&& [index, list] = getTraversalState();

    if(index < list.size() - 1) {
        return list.at(index + 1);
    } else {
        return std::nullopt;
    }
}

/**
 * @brief Gets the symbol table for the current scope.
 *
 * @return The symbol table for the current scope.
 */
auto ISL::CodeGen::IRVisitor::getSymbols() const -> const SymbolTable& {
    return m_blocks.top().symbols;
}

/**
 * @brief Gets the symbol table for the current scope.
 *
 * @return The symbol table for the current scope.
 */
auto ISL::CodeGen::IRVisitor::getSymbols() -> SymbolTable& {
    return m_blocks.top().symbols;
}

/**
 * @brief Gets the return type for the current scope.
 *
 * @return The return type for the current scope.
 */
auto ISL::CodeGen::IRVisitor::getCurrentReturnType() -> Type* {
    return m_blocks.top().return_type;
}

/**
 * @brief Adds an import prefix to the list of available prefixes
 *
 * @param prefix The prefix to add
 */
void ISL::CodeGen::IRVisitor::addImportPrefix(const std::filesystem::path& prefix)
{
    m_import_prefixes.push_back(prefix);
}

/**
 * @brief Converts a Type Expression to a WrappedType
 *
 * @param expr The expression to convert
 * @param constant Whether the type should be constant
 * @param sub_type The sub-type of this type
 */
ISL::CodeGen::WrappedType ISL::CodeGen::IRVisitor::getTypeExprToWrappedType(const Parser::AST::SimpleTypeExpression& expr, bool constant,
                                                                            WrappedType sub_type)
{
    return getTypeExprToWrappedType(expr.getTypeIdentifier()->getIdentifier(), constant, sub_type);
}

/**
 * @brief Converts a Type identifier to a WrappedType
 *
 * @param identifier The type identifier
 * @param constant Whether the type should be constant
 * @param sub_type The sub-type of this type
 */
ISL::CodeGen::WrappedType ISL::CodeGen::IRVisitor::getTypeExprToWrappedType(const std::string& identifier, bool constant,
                                                                            WrappedType sub_type)
{
    using namespace std::string_literals;
    Common::Log::debug(::tabs(m_depth) + "getTypeExprToWrappedType(" + identifier + ")");

    if(identifier == "void") {
        auto* type = llvm::Type::getVoidTy(getContext());
        return { type, buildType<Common::VoidType>(constant) };
    } else if(identifier == "bool") {
        auto* type = llvm::Type::getInt1Ty(getContext());
        return { type, buildType<Common::BoolType>(constant) };
    } else if(identifier == "char") {
        auto* type = llvm::Type::getInt1Ty(getContext());
        return { type, buildType<Common::CharType>(constant) };
    } else if(identifier == "uchar") {
        auto* type = llvm::Type::getInt1Ty(getContext());
        return { type, buildType<Common::UCharType>(constant) };
    } else if(identifier == "int") {
        auto* type = llvm::Type::getInt64Ty(getContext());
        return { type, buildType<Common::IntType>(constant) };
    } else if(identifier == "uint") {
        auto* type = llvm::Type::getInt64Ty(getContext());
        return { type, buildType<Common::UIntType>(constant) };
    } else if(identifier == "float") {
        auto* type = llvm::Type::getFloatTy(getContext());
        return { type, buildType<Common::FloatType>(constant) };
    } else if(identifier == "double") {
        auto* type = llvm::Type::getDoubleTy(getContext());
        return { type, buildType<Common::DoubleType>(constant) };
    } else if(identifier == "String") {
        auto* type = llvm::PointerType::get(llvm::IntegerType::get(getContext(), 8), 0);
        return { type, buildType<Common::StringType>(constant) };
    // } else if(identifier == "List") {
    }  else if(identifier == "Any") {
        return { nullptr, buildType() };
    } else if(identifier == "Pointer") {
        // TODO: Do we want to allow pointers to be constant?
        auto* type = llvm::Type::getInt64PtrTy(getContext());
        return { type, buildType<Common::PointerType>(constant, sub_type.type) };
    } else {
        // TODO: user defined types
        return { nullptr, buildType("<INVALID TYPE>", {}) };
    }
}

/**
 * @brief Converts a TypeOfExpression to a WrappedType
 * @details incomplete
 */
ISL::CodeGen::WrappedType ISL::CodeGen::IRVisitor::getTypeExprToWrappedType(const Parser::AST::TypeOfExpression&)
{
    // TODO
    return { nullptr, buildType<int>() };
}

/**
 * @brief Generates the function 'main'
 *
 * @param main_name The name to give to the main function (usually it's just "main")
 */
void ISL::CodeGen::IRVisitor::generateMainFunction(const std::string& main_name)
{
    llvm::FunctionType* ftype = llvm::FunctionType::get(
            llvm::Type::getInt32Ty(getContext()), {
                llvm::Type::getInt32Ty(getContext()),
                llvm::PointerType::get(llvm::PointerType::get(llvm::IntegerType::get(getContext(), 8), 0), 0),
            }, false);

    auto [_,main_func] = createFunction(getModule(), main_name, ftype,
                                        { "argc", "argv" });
    // We technically _do_ have a return type here, but it's going to be hidden
    //  from the user, as we don't want to allow them to return from main
    m_blocks.push({llvm::BasicBlock::Create(getContext(), "entry", main_func),
                   SymbolTable(getSymbols().begin(), getSymbols().end()),
                   nullptr});
    getBuilder().SetInsertPoint(m_blocks.top().block);

    auto module_main = getModule()->getOrInsertFunction(m_name + "_main", ftype);

    std::vector<llvm::Value*> call_arguments{ main_func->arg_begin(), std::next(main_func->arg_begin()) };

    // Call out to the module-main function
    // Call {m_name}_main
    llvm::CallInst* call_inst = getBuilder().CreateCall(module_main, call_arguments);

    // return 0;
    getBuilder().CreateRet(llvm::Constant::getNullValue(llvm::IntegerType::getInt32Ty(getContext())));
}

/**
 * @brief Defines a visitor function for the given expression
 *
 * @param EXPR The expression type to visit
 * @param BODY what to do when visiting
 */
#define DEF_VISIT_EXPR(EXPR, BODY)                                             \
    bool ISL::CodeGen::IRVisitor::visit##EXPR (const Parser::AST:: EXPR & expr)\
    {                                                                          \
        bool result = true;                                                    \
        BODY;                                                                  \
        return result;                                                         \
    }

/**
 * @brief visits the given expression
 *
 * @param EXPR The expression type to visit
 * @param MEMBER the member we are going to be visiting
 */
#define VISIT(EXPR, MEMBER) { \
    if(!( result = visit##EXPR(*expr.get##MEMBER()) )) \
        return result;                                 \
}

#define VISIT_ALL(EXPR, MEMBER)        \
    for(auto&& e : expr.get##MEMBER()) \
        result = result && visit##EXPR(*e);

// TODO: float vs int needs to be taken into consideration
// TODO: Signed vs Unsigned
// Need to do type checking at some point
auto ISL::CodeGen::IRVisitor::getLLVMBinaryOpBuilder(const Common::Operator& op,
                                                     WrappedType& type)
    -> BinaryOpBuilder
{
    // Is Floating Point Operation
    bool is_fop = type.type->getMetaType() == Type::MetaType::FLOAT ||
                  type.type->getMetaType() == Type::MetaType::DOUBLE;
    // Is Signed/Unsigned operation
    bool is_sop = type.type->getMetaType() != Type::MetaType::UCHAR ||
                  type.type->getMetaType() != Type::MetaType::UINT;

    switch(op.getOperatorType()) {
        case Common::OperatorType::ADD:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value*
                {
                    return getBuilder().CreateFAdd(lhs, rhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value*
                {
                    return getBuilder().CreateAdd(lhs, rhs);
                };
            }
        case Common::OperatorType::SUB:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    return getBuilder().CreateFSub(lhs, rhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    return getBuilder().CreateSub(lhs, rhs);
                };
            }
        case Common::OperatorType::MUL:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    return getBuilder().CreateFMul(lhs, rhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    return getBuilder().CreateMul(lhs, rhs);
                };
            }
        case Common::OperatorType::DIV:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    return getBuilder().CreateFDiv(lhs, rhs);
                };
            } else {
                if(is_sop) {
                    return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                        return getBuilder().CreateSDiv(lhs, rhs);
                    };
                } else {
                    return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                        return getBuilder().CreateUDiv(lhs, rhs);
                    };
                }
            }
        case Common::OperatorType::MOD:
            if(is_sop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    return getBuilder().CreateSRem(lhs, rhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    return getBuilder().CreateURem(lhs, rhs);
                };
            }
        case Common::OperatorType::BLSHIFT:
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateShl(lhs, rhs);
            };
        case Common::OperatorType::BRSHIFT:
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateLShr(lhs, rhs);
            };
        case Common::OperatorType::EQUAL:
            type = getTypeExprToWrappedType("bool");
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateICmpEQ(lhs, rhs);
            };
        case Common::OperatorType::NOT_EQUAL:
            type = getTypeExprToWrappedType("bool");
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateICmpNE(lhs, rhs);
            };
        case Common::OperatorType::LESS:
            Common::Log::debug(::tabs(m_depth) + "Common::OperatorType::LESS");
            type = getTypeExprToWrappedType("bool");
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateICmpSLT(lhs, rhs);
            };
        case Common::OperatorType::LESS_EQ:
            type = getTypeExprToWrappedType("bool");
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateICmpSLE(lhs, rhs);
            };
        case Common::OperatorType::GREATER:
            type = getTypeExprToWrappedType("bool");
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateICmpSGT(lhs, rhs);
            };
        case Common::OperatorType::GREATER_EQ:
            type = getTypeExprToWrappedType("bool");
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateICmpSGE(lhs, rhs);
            };
        case Common::OperatorType::ASSIGN:
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                // Store rhs (value) into lhs (pointer)
                return getBuilder().CreateStore(rhs, lhs);
            };
        case Common::OperatorType::ADD_ASSIGN:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Add rhs and temp together
                    llvm::Value* add = getBuilder().CreateFAdd(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(add, lhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Add rhs and temp together
                    llvm::Value* add = getBuilder().CreateAdd(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(add, lhs);
                };
            }
        case Common::OperatorType::SUB_ASSIGN:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Subtract rhs and temp together
                    llvm::Value* sub = getBuilder().CreateFSub(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(sub, lhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Subtract rhs and temp together
                    llvm::Value* sub = getBuilder().CreateSub(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(sub, lhs);
                };
            }
        case Common::OperatorType::MUL_ASSIGN:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Multiply rhs and temp together
                    llvm::Value* mul = getBuilder().CreateFMul(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(mul, lhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Multiply rhs and temp together
                    llvm::Value* mul = getBuilder().CreateMul(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(mul, lhs);
                };
            }
        case Common::OperatorType::DIV_ASSIGN:
            if(is_fop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Divide rhs and temp together
                    // TODO: Check signs (signed vs unsigned) + type (float vs int)
                    llvm::Value* div = getBuilder().CreateFDiv(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(div, lhs);
                };
            } else {
                if(is_sop) {
                    return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                        // Load the value
                        llvm::Value* temp = getBuilder().CreateLoad(lhs);
                        // Divide rhs and temp together
                        llvm::Value* div = getBuilder().CreateSDiv(temp, rhs);
                        // Store the result back into lhs
                        return getBuilder().CreateStore(div, lhs);
                    };
                } else {
                    return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                        // Load the value
                        llvm::Value* temp = getBuilder().CreateLoad(lhs);
                        // Divide rhs and temp together
                        llvm::Value* div = getBuilder().CreateUDiv(temp, rhs);
                        // Store the result back into lhs
                        return getBuilder().CreateStore(div, lhs);
                    };
                }
            }
        case Common::OperatorType::MOD_ASSIGN:
            if(is_sop) {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Mod rhs and temp together
                    llvm::Value* mod = getBuilder().CreateSRem(temp, rhs);
                    // Store the result back into lhs
                    return getBuilder().CreateStore(mod, lhs);
                };
            } else {
                return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                    // Load the value
                    llvm::Value* temp = getBuilder().CreateLoad(lhs);
                    // Mod rhs and temp together
                    llvm::Value* mod = getBuilder().CreateURem(temp, rhs);
                    return getBuilder().CreateStore(mod, lhs);
                };
            }
        case Common::OperatorType::LSHIFT_ASSIGN:
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                // Load the value
                llvm::Value* temp = getBuilder().CreateLoad(lhs);
                // Shift rhs and temp together
                llvm::Value* shl = getBuilder().CreateShl(temp, rhs);
                return getBuilder().CreateStore(shl, lhs);
            };
        case Common::OperatorType::RSHIFT_ASSIGN:
            return [this](llvm::Value* lhs, llvm::Value* rhs) -> llvm::Value* {
                // Load the value
                llvm::Value* temp = getBuilder().CreateLoad(lhs);
                // Shift rhs and temp together
                llvm::Value* shl = getBuilder().CreateLShr(temp, rhs);
                return getBuilder().CreateStore(shl, lhs);
            };
        case Common::OperatorType::CAST: {
            auto* to_type = type.llvm;
            return [this, to_type](llvm::Value*, llvm::Value* rhs) -> llvm::Value* {
                return getBuilder().CreateBitCast(rhs, to_type);
            };
        }
        case Common::OperatorType::BAND_ASSIGN:
        case Common::OperatorType::BXOR_ASSIGN:
        case Common::OperatorType::BOR_ASSIGN:
        case Common::OperatorType::BNOT_ASSIGN:
        // TODO
        default:
            return [](llvm::Value*, llvm::Value*) -> llvm::Value* {
                // TODO: Error
                return nullptr;
            };
    }
}

auto ISL::CodeGen::IRVisitor::getLLVMUnaryOpBuilder(const Common::Operator& op,
                                                    WrappedType& type)
    -> UnaryOpBuilder
{
    switch(op.getOperatorType()) {
        case Common::OperatorType::NEGATIVE:
        case Common::OperatorType::SUB:
            return [this](llvm::Value* value) -> llvm::Value* {
                return getBuilder().CreateNeg(value);
            };
        default:
            return [](llvm::Value*) -> llvm::Value* {
                // TODO: Error
                return nullptr;
            };
    }
}

DEF_VISIT_EXPR(ConstantExpression, {
// bool ISL::CodeGen::IRVisitor::visitConstantExpression (const Parser::AST:: ConstantExpression& expr)
// {                                                                          
//    bool result = true;                                                    

    ++m_depth;
    auto value = expr.getValue();

    Common::Log::debug(::tabs(m_depth) + "Creating Constant");

    m_values.push(std::visit([this](auto&& v) -> TypeCheckedValue {
        using T = std::decay_t<decltype(v)>;

        if constexpr(std::is_same_v<T, Common::BoolType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "Bool");
            auto* type = llvm::Type::getInt1Ty(getContext());
            return { llvm::ConstantInt::get(type, v),
                     getTypeExprToWrappedType("bool", true)
                   };
        } else if constexpr(std::is_same_v<T, Common::CharType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "Char");
            auto* type = llvm::Type::getInt1Ty(getContext());
            return { llvm::ConstantInt::get(type, v),
                     getTypeExprToWrappedType("char", true)
                   };
        } else if constexpr(std::is_same_v<T, Common::UCharType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "UChar");
            auto* type = llvm::Type::getInt1Ty(getContext());
            return { llvm::ConstantInt::get(type, v),
                     getTypeExprToWrappedType("uchar", true)
                   };
        } else if constexpr(std::is_same_v<T, Common::IntType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "Int=" + std::to_string(v));
            auto* type = llvm::Type::getInt64Ty(getContext());
            TypeCheckedValue tcv { llvm::ConstantInt::get(type, v), 
                     getTypeExprToWrappedType("int", true)
                   };
            Common::Log::debug(::tabs(m_depth + 1) + "Value=" + std::to_string((std::intptr_t)tcv.value));
            return tcv;
        } else if constexpr(std::is_same_v<T, Common::UIntType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "UInt");
            auto* type = llvm::Type::getInt64Ty(getContext());
            return { llvm::ConstantInt::get(type, v),  
                     getTypeExprToWrappedType("uint", true)
                   };
        } else if constexpr(std::is_same_v<T, Common::FloatType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "Float");
            auto* type = llvm::Type::getFloatTy(getContext());
            return { llvm::ConstantFP::get(type, v), 
                     getTypeExprToWrappedType("float", true)
                   };
        } else if constexpr(std::is_same_v<T, Common::DoubleType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "Double");
            auto* type = llvm::Type::getDoubleTy(getContext());
            return { llvm::ConstantFP::get(type, v), 
                     getTypeExprToWrappedType("double", true)
                   };
        } else if constexpr(std::is_same_v<T, Common::StringType>) {
            Common::Log::debug(::tabs(m_depth + 1) + "String");
            // TODO: What is the llvm::Type* value??
            return { getBuilder().CreateGlobalString(v),
                     getTypeExprToWrappedType("String", true)
                   };
        }

        // TODO: Error: Unknown type

        Common::Log::warn(::tabs(m_depth) + "Unknown type.");
        return { nullptr, { nullptr, buildType<int>() } };
    }, value));
    --m_depth;

    return result;
})

DEF_VISIT_EXPR(IdentifierExpression, {
    ++m_depth;
    using namespace std::string_literals;

    const auto& iden = expr.getIdentifier();
    if(auto it = std::find_if(getSymbols().begin(), getSymbols().end(),
                              [&iden](const auto& elem) {
                                  return elem.first == iden;
                              });
       it != getSymbols().end())
    {
        // Is it a builtin identifier?
        if(iden == "true" || iden == "false" || iden == "null") {
            m_values.push({
                    it->second.value,
                    it->second.type,
                    iden
            });
        } else {
            Common::Log::debug(::tabs(m_depth) + "Referring to identifier "s + iden);
            m_values.push({
                    getBuilder().CreateLoad(it->second.value, iden.c_str()),
                    it->second.type,
                    iden
            });
        }
    } else {
        Common::Log::error(::tabs(m_depth) + "Unrecognized identifier '"s + iden + '\'');
        --m_depth;
        return false;
    }
    --m_depth;
})

DEF_VISIT_EXPR(BinaryExpression, {
    ++m_depth;
    Common::Log::debug(::tabs(m_depth) + "Visit Binary Expression");
    Common::Log::debug(::tabs(m_depth) + "m_values.size() == " + std::to_string(m_values.size()));

    // Also TODO: make sure that the operator can even perform the operation on
    //  this type.

    auto op = expr.getOperator();

    // If we are doing any form of assignment, then don't visit the left hand
    //  side as it _has_ to be a variable
    TypeCheckedValue lhs;
    if(op == Common::OperatorType::ASSIGN        ||
       op == Common::OperatorType::ASSIGN        ||
       op == Common::OperatorType::ADD_ASSIGN    ||
       op == Common::OperatorType::SUB_ASSIGN    ||
       op == Common::OperatorType::MUL_ASSIGN    ||
       op == Common::OperatorType::DIV_ASSIGN    ||
       op == Common::OperatorType::MOD_ASSIGN    ||
       op == Common::OperatorType::LSHIFT_ASSIGN ||
       op == Common::OperatorType::RSHIFT_ASSIGN ||
       op == Common::OperatorType::BAND_ASSIGN   ||
       op == Common::OperatorType::BXOR_ASSIGN   ||
       op == Common::OperatorType::BOR_ASSIGN    ||
       op == Common::OperatorType::BNOT_ASSIGN)
    {
        const auto& lhs_expr = expr.getLHS();
        Parser::AST::IdentifierExpression* lhs_iden = nullptr;
        if(lhs_iden = dynamic_cast<const decltype(lhs_iden)>(lhs_expr.get());
           lhs_iden == nullptr)
        {
            Common::Log::error(::tabs(m_depth) + "LHS must be an identifier during assignment!");
            return false;
        }

        auto iden_name = lhs_iden->getIdentifier();
        if(auto it = std::find_if(getSymbols().begin(), getSymbols().end(),
                                  [&iden_name](auto&& element) {
                                      return element.first == iden_name;
                                  }); it != getSymbols().end())
        {
            lhs = it->second;
            Common::Log::debug(::tabs(m_depth) + "We did not visit LHS at all");
        } else {
            Common::Log::error(::tabs(m_depth) + "Could not find symbol '" + iden_name + "'");
            return false;
        }
    } else if(op == Common::OperatorType::CAST) {
        const auto& lhs_expr = expr.getLHS();
        Parser::AST::TypeExpression* lhs_type = nullptr;
        if(lhs_type = dynamic_cast<const decltype(lhs_type)>(lhs_expr.get());
           lhs_type == nullptr)
        {
            Common::Log::error(::tabs(m_depth) + "LHS must be a type during cast!");
            return false;
        }

        visitTypeExpression(*lhs_type);
        lhs = m_values.top();
        m_values.pop();
    } else {
        Common::Log::debug(::tabs(m_depth) + "Visit LHS");
        VISIT(Expression, LHS);
        lhs = m_values.top();
        m_values.pop();
    }

    Common::Log::debug(::tabs(m_depth) + "m_values.size() == " + std::to_string(m_values.size()));
    Common::Log::debug(::tabs(m_depth) + "Visit RHS");
    VISIT(Expression, RHS);

    // We visit LHS on its own, so expect only RHS to be in m_values still
    if(m_values.size() < 1) {
        Common::Log::error(::tabs(m_depth) + "Invalid number of arguments for binary expression. Got " +
                           std::to_string(m_values.size()) + ", expected 2");
        --m_depth;
        return false;
    }

    auto rhs = m_values.top();
    m_values.pop();

    WrappedType type = lhs.type;
    if(*type.type != *rhs.type.type) {
        // Check if we can do an implicit cast
        if(type.type->canImplicitCastTo(*rhs.type.type)) {
            type = rhs.type;
        } else {
            Common::Log::error(::tabs(m_depth) + "Cannot perform binary operation on two operands of differing types.");
            --m_depth;
            return false;
        }
    }

    // TODO: Determine new type if operator is cast()

    llvm::Value* bin_expr = getLLVMBinaryOpBuilder(op, type)(lhs.value, rhs.value);

    m_values.push({ bin_expr, type });
    --m_depth;
})

DEF_VISIT_EXPR(UnaryExpression, {
    ++m_depth;
    VISIT(Expression, Operand);

    if(m_values.size() < 1) {
        Common::Log::error(::tabs(m_depth) + "Invalid number of arguments for unary expression.");
        --m_depth;
        return false;
    }

    auto operand = m_values.top();

    auto op = expr.getOperator();

    WrappedType type = operand.type;
    // TODO: Handle type decay

    llvm::Value* unary_expr = getLLVMUnaryOpBuilder(op, type)(operand.value);

    m_values.push({ unary_expr, type });
    --m_depth;

    return result;
})

DEF_VISIT_EXPR(CallExpression, ({
    ++m_depth;
    VISIT_ALL(Expression, ArgumentList);

    Common::Log::debug("Visiting Call");

    // TODO: We should do type checking probably, to make sure that these
    //  values are of the right type for each argument
    std::vector<llvm::Value*> call_arguments;
    for(auto i = 0; i < expr.getArgumentList().size(); ++i) {
        auto arg = m_values.top();
        m_values.pop();

        llvm::Value* value = arg.value;

        // Note: this isn't in a function because for some weird reason when I
        //   did that LLVM decided not to actually insert the getelementptr
        //   instruction.
        // https://github.com/Microsoft/llvm/blob/master/examples/BrainF/BrainF.cpp#L166
        if(arg.type.type->getMetaType() == Type::MetaType::STRING &&
           arg.type.type->isConstant())
        {
            Common::Log::debug(::tabs(m_depth) + "String is const");
            llvm::Constant* null_int32 = llvm::Constant::getNullValue(llvm::IntegerType::getInt32Ty(getContext()));
            llvm::Constant* gep_params[] = { null_int32, null_int32 };

            llvm::GlobalVariable* gv = (llvm::GlobalVariable*)value;
            llvm::Constant* cv = (llvm::Constant*)value;

            value = llvm::ConstantExpr::getGetElementPtr(gv->getValueType(), cv,
                                                         gep_params);
        }

        call_arguments.push_back(value);

        // call_arguments.push_back(decayConstant((llvm::Constant*)arg.value, getContext()));
    }

    auto func_name = expr.getIdentifier();

    if(std::find_if(getSymbols().begin(), getSymbols().end(),
                    [&func_name](auto&& element) {
                        return element.first == func_name;
                    }) != getSymbols().end())
    {
        llvm::Function* func = getModule()->getFunction(func_name);
        llvm::FunctionType* func_type = func->getFunctionType();

        // Call func
        llvm::CallInst* call_inst = getBuilder().CreateCall(func_type, func,
                                                         call_arguments);
    } else {
        using namespace std::string_literals;
        Common::Log::error("Unknown identifier '"s + func_name + "' called.");
        result = false;
    }

    --m_depth;
}))

DEF_VISIT_EXPR(WhileExpression, {
    ++m_depth;

    // Make all possible blocks now, even if we end up not using them
    llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

    llvm::BasicBlock* prewhile_bb = llvm::BasicBlock::Create(getContext(), "prewhile",
                                                          current_func);
    llvm::BasicBlock* while_bb = llvm::BasicBlock::Create(getContext(), "while");
    llvm::BasicBlock* endwhile_bb = llvm::BasicBlock::Create(getContext(),
                                                             "endwhile");

    // Pre while loop section: check condition, and jump to endwhile if false
    getBuilder().CreateBr(prewhile_bb);
    getBuilder().SetInsertPoint(prewhile_bb);

    // Don't visit the condition until we are in the prewhile block
    VISIT(Expression, CondExpression);

    auto cond_value = m_values.top();
    m_values.pop();

    if(cond_value.type.type->getMetaType() != Type::MetaType::BOOL) {
        std::stringstream ss;
        ss << "Type of condition must be bool, not "
           << cond_value.type.type->getMetaType() << "!";
        Common::Log::error(ss.str());
        --m_depth;
        return false;
    }
    getBuilder().CreateCondBr(cond_value.value, while_bb, endwhile_bb);

    // Start inserting into the while block
    current_func->getBasicBlockList().push_back(while_bb);
    getBuilder().SetInsertPoint(while_bb);
    m_blocks.push({while_bb, SymbolTable(getSymbols().begin(), getSymbols().end()), m_blocks.top().return_type});

    m_call_on_group_end.push([this, prewhile_bb, while_bb, endwhile_bb]() {
        llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

        Common::Log::debug("Inserting endwhile block.");
        // Codegen of if block can change the current block, update value for PHI
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // Unconditionally jump back to the top,
        getBuilder().CreateBr(prewhile_bb);

        // getBuilder().CreateBr(endwhile_bb);
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // Emit merge block
        current_func->getBasicBlockList().push_back(endwhile_bb);
        getBuilder().SetInsertPoint(endwhile_bb);

        // Pop off the else and if blocks
        m_blocks.pop();

        --m_depth;
    });
})

DEF_VISIT_EXPR(DoWhileExpression, {
    ++m_depth;

    // Make all possible blocks now, even if we end up not using them
    llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

    llvm::BasicBlock* dowhile_bb = llvm::BasicBlock::Create(getContext(),
                                                            "dowhile",
                                                            current_func);
    llvm::BasicBlock* enddowhile_bb = llvm::BasicBlock::Create(getContext(),
                                                               "enddowhile");
    llvm::BasicBlock* postenddowhile_bb = llvm::BasicBlock::Create(getContext(),
                                                                   "postenddowhile");
    // Start inserting into the dowhile block
    getBuilder().CreateBr(dowhile_bb); // Note: every single block _must_ have a terminator instruction
    getBuilder().SetInsertPoint(dowhile_bb);
    m_blocks.push({dowhile_bb, SymbolTable(getSymbols().begin(), getSymbols().end()), m_blocks.top().return_type});

    m_call_on_group_end.push([this, dowhile_bb, enddowhile_bb, postenddowhile_bb, &expr]()
    {
        llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

        Common::Log::debug("Inserting enddowhile block.");
        // Codegen of if block can change the current block, update value for PHI
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // We are now in the enddowhile block
        getBuilder().CreateBr(enddowhile_bb);
        current_func->getBasicBlockList().push_back(enddowhile_bb);
        getBuilder().SetInsertPoint(enddowhile_bb);

        // Don't visit the condition until we are in the predowhile block

        if(!visitExpression(*expr.getCondExpression())) {
            return false;
        }

        auto cond_value = m_values.top();
        m_values.pop();

        if(cond_value.type.type->getMetaType() != Type::MetaType::BOOL) {
            std::stringstream ss;
            ss << "Type of condition must be bool, not "
               << cond_value.type.type->getMetaType() << "!";
            Common::Log::error(ss.str());
            --m_depth;
            return false;
        }
        getBuilder().CreateCondBr(cond_value.value, dowhile_bb, postenddowhile_bb);

        Common::Log::debug("Inserting postenddowhile block.");
        getBuilder().CreateBr(postenddowhile_bb);
        current_func->getBasicBlockList().push_back(postenddowhile_bb);
        getBuilder().SetInsertPoint(postenddowhile_bb);

        m_blocks.pop();

        --m_depth;

        return true;
    });
})

DEF_VISIT_EXPR(IfExpression, {
    ++m_depth;
    VISIT(Expression, CondExpression);
    auto cond_value = m_values.top();
    m_values.pop();

    if(cond_value.type.type->getMetaType() != Type::MetaType::BOOL) {
        std::stringstream ss;
        ss << "Type of condition must be bool, not "
           << cond_value.type.type->getMetaType() << "!";
        Common::Log::error(ss.str());
        --m_depth;
        return false;
    }

    // Make all possible blocks now, even if we end up not using them
    llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();
    llvm::BasicBlock* if_bb = llvm::BasicBlock::Create(getContext(), "if",
                                                       current_func);
    llvm::BasicBlock* else_bb = llvm::BasicBlock::Create(getContext(), "else");
    llvm::BasicBlock* endif_bb = llvm::BasicBlock::Create(getContext(), "endif");

    // Create the conditional
    if(auto next = getNextNode();
            next &&
            (dynamic_cast<Parser::AST::ElseExpression*>(next->get().get()) ||
             dynamic_cast<Parser::AST::ElseIfExpression*>(next->get().get())))
    {
        // TODO: Split this up to jump to the elseif block if needed
        getBuilder().CreateCondBr(cond_value.value, if_bb, else_bb);
    } else {
        getBuilder().CreateCondBr(cond_value.value, if_bb, endif_bb);
    }

    // Start inserting
    getBuilder().SetInsertPoint(if_bb);
    m_blocks.push({if_bb, SymbolTable(getSymbols().begin(), getSymbols().end()), m_blocks.top().return_type});
    m_temp_blocks.push(else_bb);
    m_temp_blocks.push(endif_bb);

    // TODO: We need to mark that we are doing codegen
    m_call_on_group_end.push([this, if_bb, else_bb, endif_bb]() {
        llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

        // Codegen of if block can change the current block, update value for PHI
        m_blocks.top().block = getBuilder().GetInsertBlock();

        if(auto next = getNextNode();
                next &&
                (dynamic_cast<Parser::AST::ElseExpression*>(next->get().get()) ||
                 dynamic_cast<Parser::AST::ElseIfExpression*>(next->get().get())))
        {
            Common::Log::debug(::tabs(m_depth) + "We have a matching Else Expression!");
            getBuilder().CreateBr(endif_bb);
        } else {
            Common::Log::debug(::tabs(m_depth) + "We do not have a matching Else Expression!");

            Common::Log::debug("Inserting endif block.");
            llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

            getBuilder().CreateBr(endif_bb);
            m_blocks.top().block = getBuilder().GetInsertBlock();

            // Emit merge block
            current_func->getBasicBlockList().push_back(endif_bb);
            getBuilder().SetInsertPoint(endif_bb);

            // Pop off the else and if blocks
            m_blocks.pop();
        }

        --m_depth;
    });
})

DEF_VISIT_EXPR(ElseIfExpression, {
})

DEF_VISIT_EXPR(ElseExpression, {
    ++m_depth;
    Common::Log::debug(::tabs(m_depth) + "visitElseExpression");

    llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();
    // Get the two blocks back from the earlier if statement
    llvm::BasicBlock* endif_bb = m_temp_blocks.top();
    m_temp_blocks.pop();

    llvm::BasicBlock* else_bb = m_temp_blocks.top();
    m_temp_blocks.pop();

    Common::Log::debug(::tabs(m_depth) + "Inserting else block.");
    Common::Log::debug(::tabs(m_depth) + "Current block == " + current_func->getBasicBlockList().back().getName().data());
    current_func->getBasicBlockList().push_back(else_bb);
    Common::Log::debug(::tabs(m_depth) + "Current block == " + current_func->getBasicBlockList().back().getName().data());
    getBuilder().SetInsertPoint(else_bb);
    m_blocks.push({else_bb, SymbolTable(getSymbols().begin(), getSymbols().end()), m_blocks.top().return_type});

    // Now we run code after the else-block codegen
    m_call_on_group_end.push([this, endif_bb]() {
        Common::Log::debug("Inserting endif block.");
        llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

        getBuilder().CreateBr(endif_bb);
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // Emit merge block
        current_func->getBasicBlockList().push_back(endif_bb);
        getBuilder().SetInsertPoint(endif_bb);

        // TODO: Is this supposed to be double in all cases? or was that just
        //  the value the tutorial decided to use?
        // llvm::PHINode* phi = getBuilder().CreatePHI(llvm::Type::getDoubleTy(getContext()), 2, "iftmp");
        // phi->addIncoming(if_bb);

        // TODO: What about elseif?
        // Pop off the else and if blocks
        m_blocks.pop();
        m_blocks.pop();

        --m_depth;
    });
})

DEF_VISIT_EXPR(ForExpression, {
    ++m_depth;
    // Visit the variable expression immediately
    VISIT(VariableExpression, VarExpression);

    // Note that variables are _not_ stored in m_values, but in the symbol table
    //  instead
    auto&& var = *std::find_if(getSymbols().begin(), getSymbols().end(),
                               [&expr](auto&& symbol) {
                                   return symbol.first == expr.getVarExpression()->getVarIdentifier()->getIdentifier();
                               });

    // Make all possible blocks now, even if we end up not using them
    llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

    llvm::BasicBlock* prefor_bb = llvm::BasicBlock::Create(getContext(), "prefor",
                                                          current_func);
    llvm::BasicBlock* for_bb = llvm::BasicBlock::Create(getContext(), "for");
    llvm::BasicBlock* endfor_bb = llvm::BasicBlock::Create(getContext(),
                                                             "endfor");

    // Pre for loop section: check condition, and jump to endfor if false
    getBuilder().CreateBr(prefor_bb);
    getBuilder().SetInsertPoint(prefor_bb);

    // Don't visit the condition until we are in the prefor block
    VISIT(Expression, CondExpression);

    auto cond_value = m_values.top();
    m_values.pop();

    if(cond_value.type.type->getMetaType() != Type::MetaType::BOOL) {
        std::stringstream ss;
        ss << "Type of condition must be bool, not "
           << cond_value.type.type->getMetaType() << "!";
        Common::Log::error(ss.str());
        --m_depth;
        return false;
    }
    getBuilder().CreateCondBr(cond_value.value, for_bb, endfor_bb);

    // Start inserting into the for block
    current_func->getBasicBlockList().push_back(for_bb);
    getBuilder().SetInsertPoint(for_bb);
    m_blocks.push({for_bb, SymbolTable(getSymbols().begin(), getSymbols().end()), m_blocks.top().return_type});

    m_call_on_group_end.push([this, prefor_bb, for_bb, endfor_bb, &expr]() {
        llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

        // Insert the inc expression at the end of the for loop
        // Don't visit the inc until we are at the end of the for block

        if(!visitExpression(*expr.getIncExpression())) {
            return false;
        }

        // We don't actually need the inc expression, we just need it to
        //  generate
        m_values.pop();

        Common::Log::debug("Inserting endfor block.");
        // Codegen of if block can change the current block, update value for PHI
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // Unconditionally jump back to the top,
        getBuilder().CreateBr(prefor_bb);

        // getBuilder().CreateBr(endfor_bb);
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // Emit merge block
        current_func->getBasicBlockList().push_back(endfor_bb);
        getBuilder().SetInsertPoint(endfor_bb);

        // Pop off the for block
        m_blocks.pop();

        --m_depth;
        return true;
    });
})

DEF_VISIT_EXPR(FunctionDefinitionExpression, ({
    ++m_depth;
    VISIT_ALL(ParameterExpression, ParameterList);
    VISIT(TypeExpression, ReturnType);

    // Param1 Type, Param2 Type, ...
    std::vector<llvm::Type*> arg_llvmtypes;
    std::vector<Type*> arg_types;
    std::vector<std::string> arg_names;

    auto ret_type = m_values.top();
    m_values.pop();

    for(auto i = 0; i < expr.getParameterList().size(); ++i) {
        auto param = m_values.top();
        m_values.pop();

        arg_llvmtypes.push_back(param.type.llvm);
        arg_types.push_back(param.type.type);
        arg_names.push_back(param.name);
    }

    auto func_name = expr.getFunctionIdentifier()->getIdentifier();

    auto [ftype, func] = ::createFunction(getModule(), func_name,
                                          ret_type.type.llvm, arg_llvmtypes,
                                          arg_names);
    m_blocks.push({llvm::BasicBlock::Create(getContext(), func_name + "entry", func),
                   SymbolTable(getSymbols().begin(), getSymbols().end()),
                   ret_type.type.type});
    getBuilder().SetInsertPoint(m_blocks.top().block);

    m_values.push({func, { ftype,
                           buildType(func_name, ret_type.type.type, arg_types)
                         }, func_name});

    m_call_on_group_end.push([this]() {
        llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

        // We are done with the function, stop inserting into it
        m_blocks.pop();

        Common::Log::debug("Inserting endfunction block.");

        getBuilder().SetInsertPoint(m_blocks.top().block);

        --m_depth;
        return true;
    });

    --m_depth;
}))

DEF_VISIT_EXPR(ImportExpression, ({
    auto&& import_value = expr.getValue();

    if(!std::holds_alternative<Common::StringType>(import_value->getValue())) {
        Common::Log::error("Import value must evaluate to a constant.");
        return false;
    }

    auto import_location = std::get<Common::StringType>(import_value->getValue());

    llvm::FunctionType* ftype = llvm::FunctionType::get(
            llvm::Type::getInt32Ty(getContext()), {
                llvm::Type::getInt32Ty(getContext()),
                llvm::PointerType::get(llvm::PointerType::get(llvm::IntegerType::get(getContext(), 8), 0), 0),
            }, false);

    auto* main_func = getModule()->getFunction(m_name + "_main");
    auto module_main = getModule()->getOrInsertFunction(import_location + "_main", ftype);

    std::vector<llvm::Value*> call_arguments{ main_func->arg_begin(),
                                              std::next(main_func->arg_begin())
                                            };

    // Call {m_name}_main
    llvm::CallInst* call_inst = getBuilder().CreateCall(module_main,
                                                     call_arguments);

    return importFile(import_location);
}))

DEF_VISIT_EXPR(ExternExpression, {
    ++m_depth;

    Common::Log::debug(::tabs(m_depth) + "visitExternExpression");

    const auto& symbol = expr.getSymbol();

    return std::visit([this](auto&& symbol) -> bool {
        bool result = true;

        using T = std::decay_t<decltype(symbol)>;

        if constexpr(std::is_same_v<T, Parser::AST::ParameterExpressionPtr>) {
            auto& expr = *symbol;

            std::string name = expr.getVarIdentifier()->getIdentifier();

            VISIT(TypeExpression, TypeIdentifier);

            auto type = m_values.top();
            m_values.pop();

            llvm::GlobalVariable* value = new llvm::GlobalVariable(*getModule(),
                                                                   type.type.llvm, 
                                                                   false,
                                                                   llvm::GlobalValue::ExternalLinkage,
                                                                   0, name);
            
            getSymbols().push_back({name, { value,
                                         type.type,
                                         name
                                       }
                                 });
            Common::Log::debug(::tabs(m_depth) + "Extern has placed a value named '" + name + "' into the symbol table.");
        } else {
            auto& expr = *symbol;

            VISIT_ALL(ParameterExpression, ParameterList);
            VISIT(TypeExpression, ReturnType);

            // Param1 Type, Param2 Type, ...
            std::vector<llvm::Type*> arg_llvmtypes;
            std::vector<Type*> arg_types;
            std::vector<std::string> arg_names;

            Common::Log::debug(::tabs(m_depth) + "m_values.size() == " + std::to_string(m_values.size()));

            auto ret_type = m_values.top();
            m_values.pop();

            for(auto i = 0; i < expr.getParameterList().size(); ++i) {
                auto param = m_values.top();
                m_values.pop();

                arg_llvmtypes.push_back(param.type.llvm);
                arg_types.push_back(param.type.type);
                arg_names.push_back(param.name);
            }

            auto func_name = expr.getFunctionIdentifier()->getIdentifier();
            // auto [ftype, func] = ::createFunction(getModule(), func_name,
            //                                       ret_type.type.llvm,
            //                                       arg_llvmtypes, arg_names);
            auto ftype = ::createFunctionType(ret_type.type.llvm, arg_llvmtypes);
#if 1
            auto func = getModule()->getOrInsertFunction(func_name, ftype);
#endif

            getSymbols().push_back({func_name, { nullptr, //func.getCallee(),
                                              { ftype, buildType(func_name,
                                                                 ret_type.type.type,
                                                                 arg_types)
                                            }, "" },
                                });
            Common::Log::debug(::tabs(m_depth) + "Extern has placed a function named '" + func_name + "' into the symbol table.");

            std::stringstream ss;
            ss << ::tabs(m_depth) << ", ftype=" << (void*)ftype;
            Common::Log::debug(ss.str());
        }

        --m_depth;

        return true;
    }, symbol);
})

DEF_VISIT_EXPR(LoopExpression, {
    ++m_depth;

    // Make all possible blocks now, even if we end up not using them
    llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

    llvm::BasicBlock* loop_bb = llvm::BasicBlock::Create(getContext(), "loop",
                                                         current_func);
    llvm::BasicBlock* endloop_bb = llvm::BasicBlock::Create(getContext(),
                                                             "endloop");

    // Start inserting into the loop block
    m_blocks.push({loop_bb, SymbolTable(getSymbols().begin(), getSymbols().end()), m_blocks.top().return_type});

    m_call_on_group_end.push([this, loop_bb, endloop_bb]() {
        llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

        Common::Log::debug("Inserting endloop block.");
        // Codegen of if block can change the current block, update value for PHI
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // Unconditionally jump back to the top,
        getBuilder().CreateBr(loop_bb);

        // getBuilder().CreateBr(endloop_bb);
        m_blocks.top().block = getBuilder().GetInsertBlock();

        // Emit merge block
        current_func->getBasicBlockList().push_back(endloop_bb);
        getBuilder().SetInsertPoint(endloop_bb);

        // Pop off the else and if blocks
        m_blocks.pop();

        --m_depth;
    });
})

DEF_VISIT_EXPR(ReturnExpression, {
    ++m_depth;
    VISIT(Expression, Expr);

    auto expr = m_values.top();
    m_values.pop();

    auto rtype = getCurrentReturnType();

    if(rtype == nullptr) {
        Common::Log::error("Cannot return from outside of a function!");
        --m_depth;
        return false;
    }

    if(rtype->getMetaType() != Type::MetaType::ANY) {
        if(*rtype != *expr.type.type &&
           !expr.type.type->canImplicitCastTo(*rtype))
        {
            Common::Log::error("Function return type does not match expr's type.");
            --m_depth;
            return false;
        }
    }

    auto* instr = getBuilder().CreateRet(expr.value);

    m_values.push({instr, expr.type, ""});

    --m_depth;
})

DEF_VISIT_EXPR(StructureExpression, {
})

DEF_VISIT_EXPR(VariableExpression, ({
    ++m_depth;
    Common::Log::debug(::tabs(m_depth) + "Visit Variable Expression.");

    VISIT(TypeExpression, TypeExpression);
    // VISIT(IdentifierExpression, VarIdentifier);
    VISIT(Expression, ValueExpr);

    using namespace std::string_literals;
    if(m_values.size() < 2) {
        Common::Log::error("Invalid number of arguments left on value stack. Expected 2, got "s + std::to_string(m_values.size()));
        --m_depth;
        return false;
    }

    auto value = m_values.top();
    m_values.pop();

    auto type = m_values.top();
    m_values.pop();

    // auto identifier = m_values.top();
    std::string name = expr.getVarIdentifier()->getIdentifier();
    llvm::Function* current_func = getBuilder().GetInsertBlock()->getParent();

    llvm::AllocaInst* alloca;

    // TODO: Enforce that the type of value matches the type of type
    Common::Log::debug(::tabs(m_depth) + "MetaType=" +
                       std::to_string(type.type.type->getMetaType()));
    if(type.type.type->getMetaType() != Type::MetaType::ANY) {
        if(*type.type.type != *value.type.type &&
           !value.type.type->canImplicitCastTo(*type.type.type))
        {
            Common::Log::error("Specified type does not match value's type.");
            --m_depth;
            return false;
        }
        alloca = ::createAllocaAtEntry(current_func, type.type.llvm, name);

        // Don't put the variable into 'values', as this should never be part of
        //  another expression
        getSymbols().push_back({name, { alloca,
                                     type.type,
                                     name
                                   }
                             });
    } else {
        Common::Log::debug(::tabs(m_depth) + "Declared type is Any, therefore we"
                           " use the type of the right hand side.");
        alloca = ::createAllocaAtEntry(current_func, value.type.llvm, name);

        // Don't put the variable into 'values', as this should never be part of
        //  another expression
        getSymbols().push_back({name, { alloca,
                                     value.type,
                                     name
                                   }
                             });
    }

    llvm::Value* to_store = value.value;

    // Note: this isn't in a function because for some weird reason when I
    //   did that LLVM decided not to actually insert the getelementptr
    //   instruction.
    // https://github.com/Microsoft/llvm/blob/master/examples/BrainF/BrainF.cpp#L166
    if(value.type.type->getMetaType() == Type::MetaType::STRING &&
       value.type.type->isConstant())
    {
        llvm::Constant* null_int32 = llvm::Constant::getNullValue(llvm::IntegerType::getInt32Ty(getContext()));
        llvm::Constant* gep_params[] = { null_int32, null_int32 };

        llvm::GlobalVariable* gv = (llvm::GlobalVariable*)to_store;
        llvm::Constant* cv = (llvm::Constant*)to_store;

        to_store = llvm::ConstantExpr::getGetElementPtr(gv->getValueType(), cv,
                                                        gep_params);
    }

    getBuilder().CreateStore(to_store, alloca);

    Common::Log::debug(::tabs(m_depth) + "Variable creation complete.");

    --m_depth;
}))

DEF_VISIT_EXPR(SimpleTypeExpression, ({
    ++m_depth;

    WrappedType sub_type = { nullptr, nullptr };
    if(expr.getSubType() != nullptr) {
        VISIT(TypeExpression, SubType);
        sub_type = m_values.top().type;
        m_values.pop();
    }

    using namespace std::string_literals;
    Common::Log::debug(::tabs(m_depth) + "visitSimpleTypeExpression(): m_values.size() = "s + std::to_string(m_values.size()));

    auto type = getTypeExprToWrappedType(expr, false, sub_type);

    // Error check and make sure that getTypeExprToWrappedType succeeded
    if(type.llvm == nullptr) {
        Common::Log::error("No such type-name: "s + expr.getTypeIdentifier()->getIdentifier());
        return false;
    }

    // Types do not have a value, so they may _not_ be used in places where
    //  values are expected
    m_values.push({nullptr, type, ""});
    --m_depth;
}))

DEF_VISIT_EXPR(TypeOfExpression, {
    ++m_depth;

#if 0
    if(expr.getSubType() != nullptr) {
        VISIT(TypeExpression, SubType);
    }
#endif

    VISIT(Expression, Expression);

    auto texpr = m_values.top();
    m_values.pop();
    m_values.push({nullptr, texpr.type, ""});

    --m_depth;
})

DEF_VISIT_EXPR(EmptyExpression, { })

DEF_VISIT_EXPR(ParameterExpression, {
    ++m_depth;
    VISIT(TypeExpression, TypeIdentifier);

    auto type = m_values.top();
    m_values.pop();

    m_values.push({nullptr, type.type, expr.getVarIdentifier()->getIdentifier() });

    --m_depth;
})

// -----------------------------------------------------------------------------

ISL::CodeGen::MainIRVisitor::MainIRVisitor(const std::string& name,
                                           const std::filesystem::path& path):
    IRVisitor(name, path),
    m_context(),
    m_builder(m_context),
    m_module(nullptr)
{
    m_module = /*std::make_unique<*/new llvm::Module/*>*/(name, getContext());
}

llvm::LLVMContext& ISL::CodeGen::MainIRVisitor::getContext() {
    return m_context;
}

auto ISL::CodeGen::MainIRVisitor::getModule() -> ModulePtrRef {
    return m_module;
}

llvm::IRBuilder<>& ISL::CodeGen::MainIRVisitor::getBuilder() {
    return m_builder;
}


ISL::CodeGen::ModuleIRVisitor::ModuleIRVisitor(llvm::LLVMContext& context,
                                               const std::string& name,
                                               const std::filesystem::path& path):
    IRVisitor(name, path),
    m_context(context),
    m_builder(m_context),
    m_module(nullptr)
{
    m_module = /*std::make_unique<*/new llvm::Module/*>*/(name, getContext());
}

llvm::LLVMContext& ISL::CodeGen::ModuleIRVisitor::getContext() {
    return m_context;
}

auto ISL::CodeGen::ModuleIRVisitor::getModule() -> ModulePtrRef {
    return m_module;
}

llvm::IRBuilder<>& ISL::CodeGen::ModuleIRVisitor::getBuilder() {
    return m_builder;
}


