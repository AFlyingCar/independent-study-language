
#include "LLVMOstream.h"

/**
 * @brief Constructs an LLVMOstream
 *
 * @param stream The stream to wrap around
 * @param unbuffered
 */
ISL::CodeGen::LLVMOstream::LLVMOstream(std::ostream& stream, bool unbuffered):
    llvm::raw_ostream(unbuffered),
    m_stream(stream)
{ }

void ISL::CodeGen::LLVMOstream::write_impl(const char *Ptr, size_t Size) {
    m_stream << std::string(Ptr, Size);
}

uint64_t ISL::CodeGen::LLVMOstream::current_pos() const {
    return m_stream.tellp();
}

