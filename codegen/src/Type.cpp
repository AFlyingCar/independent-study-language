
#include "Type.h"

ISL::CodeGen::Type::Type(MetaType meta): m_type(meta) { }

bool ISL::CodeGen::Type::operator!=(const Type& type) const {
    return !(*this == type);
}
ISL::CodeGen::Type::MetaType ISL::CodeGen::Type::getMetaType() const {
    return m_type;
}

ISL::CodeGen::BuiltinType::BuiltinType(MetaType meta, bool constant):
    Type(meta),
    m_constant(constant)
{ }
ISL::CodeGen::BuiltinType::BuiltinType(MetaType meta, Type* sub_type,
                                       bool constant):
    Type(meta),
    m_subtype(std::move(sub_type)),
    m_constant(constant)
{ }

ISL::CodeGen::BuiltinType::~BuiltinType() {
    delete m_subtype;
}

bool ISL::CodeGen::BuiltinType::operator==(const Type& type) const {
    // TODO: Check subtypes

    // We know that if the meta-types match, then the other one _has_ to be a
    //  BuiltinType
    return getMetaType() == type.getMetaType();
}

bool ISL::CodeGen::BuiltinType::canImplicitCastTo(const Type& type) const {
    return (getMetaType() == MetaType::CHAR && type.getMetaType() == MetaType::UCHAR) ||
           (getMetaType() == MetaType::INT && type.getMetaType() == MetaType::UINT) ||
           (getMetaType() == MetaType::POINTER && (type.getMetaType() == MetaType::INT ||
                                                   type.getMetaType() == MetaType::UINT));
}

bool ISL::CodeGen::BuiltinType::isConstant() const {
    return m_constant;
}

bool ISL::CodeGen::BuiltinType::hasSubType() const {
    return m_subtype != nullptr;
}

const ISL::CodeGen::Type* ISL::CodeGen::BuiltinType::getSubType() const {
    return m_subtype;
}

ISL::CodeGen::UserType::UserType(const std::string& user_typename,
                                 const std::vector<Type*>& members):
    Type(MetaType::USER_DEFINED),
    m_typename(user_typename),
    m_members(members)
{ }

ISL::CodeGen::UserType::~UserType() {
    for(auto&& m : m_members) {
        delete m;
    }
}

bool ISL::CodeGen::UserType::operator==(const Type& type) const {
    // TODO
    return false;
}

bool ISL::CodeGen::UserType::canImplicitCastTo(const Type&) const {
    return false;
}

const std::string& ISL::CodeGen::UserType::getTypeName() const {
    return m_typename;
}

auto ISL::CodeGen::UserType::getMemberTypes() const -> const std::vector<Type*>&
{
    return m_members;
}

bool ISL::CodeGen::UserType::isConstant() const {
    return false;
}

ISL::CodeGen::FunctionType::FunctionType(const std::string& user_typename,
                                         Type* ret_type,
                                         const std::vector<Type*>& param_types):
    Type(MetaType::FUNCTION),
    m_typename(user_typename), m_return_type(ret_type),
    m_parameter_types(param_types)
{ }

ISL::CodeGen::FunctionType::~FunctionType() {
    delete m_return_type;
    for(auto&& pt : m_parameter_types) {
        delete pt;
    }
}

bool ISL::CodeGen::FunctionType::operator==(const Type& type) const {
    return false;
}

const std::string& ISL::CodeGen::FunctionType::getTypeName() const {
    return m_typename;
}

bool ISL::CodeGen::FunctionType::canImplicitCastTo(const Type&) const {
    return false;
}

auto ISL::CodeGen::FunctionType::getReturnType() const -> const Type* {
    return m_return_type;
}

auto ISL::CodeGen::FunctionType::getParameterTypes() const
    -> const std::vector<Type*>&
{
    return m_parameter_types;
}

bool ISL::CodeGen::FunctionType::isConstant() const {
    return true;
}

ISL::CodeGen::BuiltinType* ISL::CodeGen::buildType() {
    return new BuiltinType(Type::MetaType::ANY);
}

auto ISL::CodeGen::buildType(const std::string& user_typename,
                             std::vector<Type*> members)
    -> UserType*
{
    return new UserType(user_typename, members);
}

auto ISL::CodeGen::buildType(const std::string& user_typename, Type* ret_type,
                             std::vector<Type*> param_types)
    -> FunctionType*
{
    return new FunctionType(user_typename, ret_type, param_types);
}

