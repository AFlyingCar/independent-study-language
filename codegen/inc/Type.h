#ifndef TYPE_H
#define TYPE_H

#include <string>
#include <vector>
#include <variant>

#include "Types.h"

namespace ISL::CodeGen {
    /**
     * @brief Defines a type as can be understood by the IRVisitor
     */
    class Type {
        public:
            /**
             * @brief A simple MetaType that gives a high-level idea of what
             *        the type is.
             */
            enum MetaType {
                VOID,
                BOOL,
                CHAR,
                UCHAR,
                INT,
                UINT,
                FLOAT,
                DOUBLE,
                STRING,
                LIST,
                POINTER,

                ANY,
                USER_DEFINED,
                FUNCTION
            };

            Type(MetaType);
            virtual ~Type() = default;

            MetaType getMetaType() const;

            virtual bool operator==(const Type&) const = 0;
            bool operator!=(const Type&) const;

            virtual bool canImplicitCastTo(const Type&) const = 0;

            virtual bool isConstant() const = 0;
        private:
            //! The meta type
            MetaType m_type;
    };

    /**
     * @brief Defines a builtin type.
     */
    class BuiltinType: public Type {
        public:
            BuiltinType(MetaType, bool = false);
            BuiltinType(MetaType, Type*, bool = false);
            virtual ~BuiltinType();

            virtual bool operator==(const Type&) const override;
            virtual bool canImplicitCastTo(const Type&) const override;

            bool hasSubType() const;
            const Type* getSubType() const;

            virtual bool isConstant() const override;
        private:
            //! The subtype
            Type* m_subtype;

            //! Whether this type is a constant
            bool m_constant;
    };

    /**
     * @brief Defines a user-defined type
     */
    class UserType: public Type {
        public:
            UserType(const std::string&, const std::vector<Type*>&);
            virtual ~UserType();

            virtual bool operator==(const Type&) const override;
            virtual bool canImplicitCastTo(const Type&) const override;

            const std::string& getTypeName() const;
            const std::vector<Type*>& getMemberTypes() const;

            virtual bool isConstant() const override;
        private:
            //! The name of the user-defined type
            std::string m_typename;
            //! The members in the user-defined type
            std::vector<Type*> m_members;
    };

    /**
     * @brief Defines a function-type
     */
    class FunctionType: public Type {
        public:
            FunctionType(const std::string&, Type*, const std::vector<Type*>&);
            virtual ~FunctionType();

            virtual bool operator==(const Type&) const override;
            virtual bool canImplicitCastTo(const Type&) const override;

            const std::string& getTypeName() const;
            const Type* getReturnType() const;
            const std::vector<Type*>& getParameterTypes() const;

            virtual bool isConstant() const override;
        private:
            //! The name of the function-type
            std::string m_typename;
            //! The type this function type returns
            Type* m_return_type;
            //! The argument types this function type takes
            std::vector<Type*> m_parameter_types;
    };

    using TypeVariant = std::variant<BuiltinType, UserType>;

    /**
     * @brief Builds a BuitinType
     *
     * @tparam T The type to make a BuiltinType for
     *
     * @param constant Whether this type is to be a constant
     * @param sub_type The sub type for this type
     */
    template<typename T>
    BuiltinType* buildType(bool constant = false, Type* sub_type = nullptr) {
        if constexpr(std::is_same_v<T, Common::VoidType>) {
            return new BuiltinType(Type::MetaType::VOID, constant);
        } else if constexpr(std::is_same_v<T, Common::BoolType>) {
            return new BuiltinType(Type::MetaType::BOOL, constant);
        } else if constexpr(std::is_same_v<T, Common::CharType>) {
            return new BuiltinType(Type::MetaType::CHAR, constant);
        } else if constexpr(std::is_same_v<T, Common::UCharType>) {
            return new BuiltinType(Type::MetaType::UCHAR, constant);
        } else if constexpr(std::is_same_v<T, Common::IntType>) {
            return new BuiltinType(Type::MetaType::INT, constant);
        } else if constexpr(std::is_same_v<T, Common::UIntType>) {
            return new BuiltinType(Type::MetaType::UINT, constant);
        } else if constexpr(std::is_same_v<T, Common::FloatType>) {
            return new BuiltinType(Type::MetaType::FLOAT, constant);
        } else if constexpr(std::is_same_v<T, Common::DoubleType>) {
            return new BuiltinType(Type::MetaType::DOUBLE, constant);
        } else if constexpr(std::is_same_v<T, Common::StringType>) {
            return new BuiltinType(Type::MetaType::STRING, constant);
        } else if constexpr(std::is_same_v<T, Common::PointerType>) {
            return new BuiltinType(Type::MetaType::POINTER, sub_type, constant);
#if 0
        } else if constexpr(std::is_same_v<T, ListType>) {
            return std::make_unique<BuiltinType>(Type::MetaType::LIST);
#endif
        } else {
            return nullptr;
        }
    }

    BuiltinType* buildType();
    UserType* buildType(const std::string&, std::vector<Type*>);
    FunctionType* buildType(const std::string&, Type*, std::vector<Type*>);
}

#endif

