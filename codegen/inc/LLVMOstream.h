
#ifndef LLVM_OSTREAM_H
#define LLVM_OSTREAM_H

#include "llvm/Support/raw_ostream.h"

namespace ISL::CodeGen {
    // This is basically a carbon-copy of raw_fd_ostream, only with std::ostream
    //  instead
    /**
     * @brief A simple wrapper around llvm::raw_ostream for converting/writing
     *        to std::ostream
     */
    class LLVMOstream: public llvm::raw_ostream {
        public:
            LLVMOstream(std::ostream&, bool = false);
            virtual ~LLVMOstream() = default;

            /// The is the piece of the class that is implemented by subclasses.  This
            /// writes the \p Size bytes starting at
            /// \p Ptr to the underlying stream.
            ///
            /// This function is guaranteed to only be called at a point at which it is
            /// safe for the subclass to install a new buffer via SetBuffer.
            ///
            /// \param Ptr The start of the data to be written. For buffered streams this
            /// is guaranteed to be the start of the buffer.
            ///
            /// \param Size The number of bytes to be written.
            ///
            /// \invariant { Size > 0 }
            virtual void write_impl(const char *Ptr, size_t Size) override;

            /// Return the current position within the stream, not counting the bytes
            /// currently in the buffer.
            virtual uint64_t current_pos() const override;

        private:
            //! The std::ostream to write to
            std::ostream& m_stream;
    };
}

#endif

