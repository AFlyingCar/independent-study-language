
#ifndef CODEGEN_SCOPE_H
#define CODEGEN_SCOPE_H

#include <utility>
#include <vector>
#include <string>

#include "TypeCheckedValue.h"

namespace ISL::CodeGen {
    //! How a Symbol Table is represented internally
    using SymbolTable = std::vector<std::pair<std::string, TypeCheckedValue>>;

    /**
     * @brief A scope
     */
    struct Scope {
        //! The scope's block
        llvm::BasicBlock* block;

        //! The scope's symbol table
        SymbolTable symbols;

        //! The return type for this scope (if one exists, can be null)
        Type* return_type;
    };

    using ScopeStack = std::stack<Scope>;
}

#endif

