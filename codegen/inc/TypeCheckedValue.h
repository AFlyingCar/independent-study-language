
#ifndef TYPECHECKED_VALUE_H
#define TYPECHECKED_VALUE_H

#include "llvm/IR/Value.h"
#include "llvm/IR/Type.h"

#include "Type.h"

namespace ISL::CodeGen {
    /**
     * @brief A wrapper around both an LLVM type and a Type
     */
    struct WrappedType {
        //! The LLVM type
        llvm::Type* llvm;
        //! The CodeGen Type
        Type* type; 
    };

    /**
     * @brief An llvm::Value which is combined with a type for easy
     *        type-checking
     */
    struct TypeCheckedValue {
        //! The LLVM Value
        llvm::Value* value = nullptr;
        //! The type
        WrappedType type{ nullptr, buildType<int>() };
        //! The name
        std::string name = "";
    };
}

#endif

