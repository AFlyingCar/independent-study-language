/**
 * @brief File for defining IRVisitor, MainIRVisitor, and ModuleIRVisitor
 * @author Tyler Robbins
 */

#ifndef IRVISITOR_H
#define IRVISITOR_H

#include <stack>
#include <memory>
#include <filesystem>
#include <queue>

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/BasicBlock.h"

#include "operator/Operator.h"
#include "ASTVisitor.h"

#include "TypeCheckedValue.h"
#include "Scope.h"
#include "Type.h"

namespace ISL::CodeGen {
    /**
     * @brief A visitor type for generating IR
     */
    class IRVisitor: public Parser::ASTVisitor {
        public:
            //! A pointer to a module
            using ModulePtr = llvm::Module*;//std::shared_ptr<llvm::Module>;
            //! A reference to a pointer to a module
            using ModulePtrRef = ModulePtr&;
            //! A functor that can build a binary operator
            using BinaryOpBuilder = std::function<llvm::Value*(llvm::Value*,
                                                               llvm::Value*)>;
            //! A functor that can build a unary operator
            using UnaryOpBuilder = std::function<llvm::Value*(llvm::Value*)>;
            //! A functor that can write an imported module to an ostream
            using ImportedFileWriterType = std::function<bool(ModulePtrRef,
                                                              std::ostream&)>;

            //! The default file writer for imported files
            static const ImportedFileWriterType DEFAULT_IMPORTED_FILE_WRITER;

            IRVisitor(const std::string&, const std::filesystem::path&);
            virtual ~IRVisitor() = default;

            virtual bool traverseAST(const std::vector<Parser::AST::ExpressionPtr>&) override;

            virtual llvm::LLVMContext& getContext() = 0;

            //! X-macro for visitors
#define X(EXPR) virtual bool visit##EXPR (const Parser::AST:: EXPR & ) override;
            AST_EXPRESSIONS()
#undef X

            virtual ModulePtrRef getModule() = 0;
            virtual llvm::IRBuilder<>& getBuilder() = 0;

            void generateMainFunction(const std::string& = "main");

            const SymbolTable& getSymbols() const;

            void addImportPrefix(const std::filesystem::path&);

            void setImportFileWriter(const ImportedFileWriterType&);

        protected:
            BinaryOpBuilder getLLVMBinaryOpBuilder(const Common::Operator&, WrappedType&);
            UnaryOpBuilder getLLVMUnaryOpBuilder(const Common::Operator&, WrappedType&);

            WrappedType getTypeExprToWrappedType(const Parser::AST::SimpleTypeExpression&, bool = false,
                                                 WrappedType = { nullptr, nullptr });
            WrappedType getTypeExprToWrappedType(const Parser::AST::TypeOfExpression&);

            WrappedType getTypeExprToWrappedType(const std::string&, bool = false,
                                                 WrappedType = { nullptr, nullptr });

            SymbolTable& getSymbols();
            Type* getCurrentReturnType();

            virtual void traverseSubExprEnd(const std::string&) override;
            virtual void traverseSubExprGroupStart(const std::string&) override;
            virtual void traverseSubExprGroupEnd(const std::string&) override;

            bool importFile(const std::string&);

            std::optional<std::reference_wrapper<const Parser::AST::ExpressionPtr>>
                getNextNode() const;

        private:
            //! Value stack, used for communicating between visitors
            std::stack<TypeCheckedValue> m_values;

            ScopeStack m_blocks; //!< The stack of scopes

            //! temporary block stack, used for communicating between visitors
            std::stack<llvm::BasicBlock*> m_temp_blocks;

            //! The name of this module
            std::string m_name;

            //! The path to this module
            std::filesystem::path m_path;

            //! Current depth of traversal. Used for debugging
            size_t m_depth;

            //! List of importable-file prefixes
            std::vector<std::filesystem::path> m_import_prefixes;

            //! List of files that have been imported
            std::vector<std::filesystem::path> m_imported;

            //! queue of functions to be called at the end of a statement group
            std::queue<std::function<void()>> m_call_on_group_end;

            //! The writer to call when importing a file
            ImportedFileWriterType m_imported_file_writer = DEFAULT_IMPORTED_FILE_WRITER;
    };

    /**
     * @brief An implementation of IRVisitor for the main module
     */
    class MainIRVisitor: public IRVisitor {
        public:
            MainIRVisitor(const std::string&, const std::filesystem::path&);
            virtual ~MainIRVisitor() = default;

            virtual llvm::LLVMContext& getContext() override;
            virtual ModulePtrRef getModule() override;
            virtual llvm::IRBuilder<>& getBuilder() override;

        private:
            //! The LLVM Context
            llvm::LLVMContext m_context;
            //! The IR builder
            llvm::IRBuilder<> m_builder;
            //! The module
            ModulePtr m_module;
    };

    /**
     * @brief An implementation of IRVisitor for imported modules
     */
    class ModuleIRVisitor: public IRVisitor {
        public:
            ModuleIRVisitor(llvm::LLVMContext&, const std::string&,
                            const std::filesystem::path&);
            virtual ~ModuleIRVisitor() = default;

            virtual llvm::LLVMContext& getContext() override;
            virtual ModulePtrRef getModule() override;
            virtual llvm::IRBuilder<>& getBuilder() override;
        private:
            //! The LLVM Context
            llvm::LLVMContext& m_context;
            //! The IR builder
            llvm::IRBuilder<> m_builder;
            //! The module
            ModulePtr m_module;
    };
}

#endif

