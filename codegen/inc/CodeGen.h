
#ifndef CODEGEN_H
#define CODEGEN_H

#include <filesystem>
#include <ostream>
#include <vector>
#include <string>

#include "ast/Expression.h"

namespace ISL::CodeGen {
    bool genCode(const std::filesystem::path&, const std::string&,
                 std::istream&, std::ostream&,
                 const std::vector<std::filesystem::path>&);
    bool genCode(const std::filesystem::path&, const std::string&,
                 std::ostream&, const std::vector<std::filesystem::path>&);

    void genCode(const std::filesystem::path&, const std::string&,
                 const std::vector<Parser::AST::ExpressionPtr>&,
                 std::ostream&, const std::vector<std::filesystem::path>&);
}

#endif

