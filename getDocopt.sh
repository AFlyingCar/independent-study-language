
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit 1
fi

if [ ! -d "$1" ]; then
    echo "Cannot find directory '$1'"
    exit 1
fi

cd $1
git clone https://github.com/docopt/docopt.cpp.git docopt.cpp
cd docopt.cpp

mkdir build
cd build
cmake ..

make install

