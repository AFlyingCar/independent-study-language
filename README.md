
# Independent Study Language

## How To Build

First, download and install docopt by following the instructions
[here](https://github.com/docopt/docopt.cpp#getting-and-using), and make sure
that you install clang-8.0.0 or higher, and CMake 3.15 or higher.

### Install Dependencies:
```
sudo apt-get install clang++-8 make
```

### Optional Dependencies:

```
sudo apt install googletest libgtest-dev
```

Important note: `libgtest-dev` only installs the source code, and must still be
compiled manually. The source code is usually placed in `/usr/src/gtest`. See
[this link](https://www.eriksmistad.no/getting-started-with-google-test-on-ubuntu/)
for a tutorial on how to build gtest. If googletest was not found, the project will
still compile, however unit tests will be disabled and not compiled.

### Building the project:
```
git clone https://bitbucket.org/AFlyingCar/Independent-Study-Language.git
cd Independent-Study-Language
mkdir build
cd build
cmake ..
make
```

## Current Status:

### Lexer

An input stream of characters can be input to the top-level `tokenize()`
function, and a stream of parse-able tokens will be output. The frontend can
take in a set of files and tokenize each one in turn. Running the frontend with
either no files specified, or with the `-h` flag will list all options that the
frontend can be given (such as verbose output or quiet output)

### Parser

An input stream of tokens can be given to the top-level `parse()` function, and
can be partially parsed into an AST. A frontend exists that will dump the entire
AST out as a JSON object to stdout if there were no errors in earlier parsing.
Running the frontend with either no files specified, or with the `-h` flag will
list all options that the frontend can be given (such as verbose output or quiet
output)

* Parameter Expressions  ![](https://img.shields.io/badge/feature-complete-success)
* Value Expressions      ![](https://img.shields.io/badge/feature-complete-success)
* Binary Expressions     ![](https://img.shields.io/badge/feature-complete-success)
* Unary Expressions      ![](https://img.shields.io/badge/feature-complete-success)
* Call Expressions       ![](https://img.shields.io/badge/feature-complete-success)
* Variable Expressions   ![](https://img.shields.io/badge/feature-complete-success)
* TypeOf Expressions     ![](https://img.shields.io/badge/feature-complete-success)
* Cast Expressions       ![](https://img.shields.io/badge/feature-complete-success)
* For Statements         ![](https://img.shields.io/badge/feature-complete-success)
* If Statements          ![](https://img.shields.io/badge/feature-complete-success)
* ElseIf Statements      ![](https://img.shields.io/badge/feature-complete-success)
* EndIf Statements       ![](https://img.shields.io/badge/feature-complete-success)
* Function Statements    ![](https://img.shields.io/badge/feature-complete-success)
* Import Statements      ![](https://img.shields.io/badge/feature-complete-success)
* Statement              ![](https://img.shields.io/badge/feature-complete-success)
* Error Checking         ![](https://img.shields.io/badge/feature-inprogress-yellow)
  * Exists, but doesn't provide all proper information on where specifically the error occurred.
* Type Expressions       ![](https://img.shields.io/badge/feature-inprogress-yellow)
  * Mostly complete, but still missing parsing for function-pointers
* While Statements       ![](https://img.shields.io/badge/feature-complete-success)
* DoWhile Statements     ![](https://img.shields.io/badge/feature-complete-success)
* Loop Statements        ![](https://img.shields.io/badge/feature-complete-success)
* Constraint Expressions ![](https://img.shields.io/badge/feature-incomplete-red)
* Enum Statements        ![](https://img.shields.io/badge/feature-incomplete-red)
* Structure Statements   ![](https://img.shields.io/badge/feature-complete-success)
* Return Statements      ![](https://img.shields.io/badge/feature-complete-success)

### CodeGen

A complete AST can be given to the top-level `genCode()` function along with a
module name and output stream, and LLVM IR will be generated for the given AST.
A frontend exists that will dump the generated IR to stdout if there were no
errors in generation or any earlier steps. Running the frontend with either no
file specified, or with the `-h` flag will list all options that the frontend
can be given (such as verbose output or quiet output).

* Function Statements    ![](https://img.shields.io/badge/feature-complete-success)
* Parameter Expressions  ![](https://img.shields.io/badge/feature-complete-success)
* Binary Expressions     ![](https://img.shields.io/badge/feature-complete-success)
* Value Expressions      ![](https://img.shields.io/badge/feature-complete-success)
* Unary Expressions      ![](https://img.shields.io/badge/feature-complete-success)
* Call Expressions       ![](https://img.shields.io/badge/feature-complete-success)
* Variable Expressions   ![](https://img.shields.io/badge/feature-complete-success)
* TypeOf Expressions     ![](https://img.shields.io/badge/feature-incomplete-red)
* Cast Expressions       ![](https://img.shields.io/badge/feature-incomplete-red)
* For Statements         ![](https://img.shields.io/badge/feature-complete-success)
* If Statements          ![](https://img.shields.io/badge/feature-complete-success)
* ElseIf Statements      ![](https://img.shields.io/badge/feature-complete-success)
* EndIf Statements       ![](https://img.shields.io/badge/feature-complete-success)
* Import Statements      ![](https://img.shields.io/badge/feature-complete-success)
* Error Checking         ![](https://img.shields.io/badge/feature-inprogress-yellow)
  * Exists, but doesn't provide all proper information on where specifically the error occurred.
* Type Expressions       ![](https://img.shields.io/badge/feature-complete-success)
* While Statements       ![](https://img.shields.io/badge/feature-complete-success)
* DoWhile Statements     ![](https://img.shields.io/badge/feature-complete-success)
* Loop Statements        ![](https://img.shields.io/badge/feature-complete-success)
* Constraint Expressions ![](https://img.shields.io/badge/feature-incomplete-red)
* Enum Statements        ![](https://img.shields.io/badge/feature-incomplete-red)
* Structure Statements   ![](https://img.shields.io/badge/feature-incomplete-red)
* Return Statements      ![](https://img.shields.io/badge/feature-complete-success)

#### Using the CodeGen

The codegen frontend of the compiler will currently only generate LLVM IR. This
is not executable in its own right, and must be converted to an executable
through clang.

```
./codegen_frontend /path/to/file.isl --output=- | llvm-as-9 -o /path/to/output/file.bc
llvm-link-9 /path/to/output/*.bc -o /path/to/output/full.bc
llc-9 -filetype=obj /path/to/output/full.bc -o /path/to/output/object_file.o
clang object_file.o -o output.exe
```

The exact version of the LLVM tools do not have to be 9, however that is the
version that I have tested and confirmed to work.

Note that the `llvm-link-9` step is not required if no imports happen.
Additionally, to link against external libraries, you would pass those in to
the step that calls clang.

### Compiler

The compiler can be given a single .isl file and generate a runnable executable
for the current machine's architecture. By default it will output to `a.{EXT}`
where `{EXT}` is the extension for the output file.

The output format can be switched from executable to object file using the `-c`
flag, or it can be switched to outputting assembly with the `-S` flag.

#### Using the Compiler

For simple files that do not use extern or importing:

```
./compiler /path/to/file.isl -o /path/to/output
```

If extern is used, make sure you link in the files where those symbols are
defined. If those symbols are in a library, use the `-l` flag, whereas if they
are in an object file, use the `-O` flag.

```
./compiler /path/to/file.isl -o /path/to/output -l libname -O /path/to/object.o
```

If you are importing files, by default the only place that gets searched is the
directory of the main .isl file. To add more prefixes that get searched, use the
`-i` flag:

```
./compiler /path/to/file.isl -o /path/to/output -i /path/to/modules/
```

Keep in mind that these are prefixes, and that `-i` can be called multiple
times. Since they are prefixes, imported file paths will get appended to each
prefix while it is searching for if the module can get imported.

