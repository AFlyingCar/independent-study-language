
cmake_minimum_required(VERSION 3.2)

include("cmake/HunterGate.cmake")

HunterGate(
    URL "https://github.com/cpp-pm/hunter/archive/v0.16.15.tar.gz"
    SHA1 "6974c2150fc0d3b09de3ad1efcbf15d360647ffa"
)

function(getDependencies)
    # Make sure we don't already have LLVM installed
    find_package(LLVM REQUIRED CONFIG)

    if(NOT ${LLVM_FOUND})
        hunter_add_package(LLVM)
    endif()

    hunter_add_package(GTest)

################################################################################

###########################
# Non Hunter Dependencies #
###########################

endfunction()

