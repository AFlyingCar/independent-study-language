
#include <sstream>

#include "gtest/gtest.h"

#include "Tokenizer.h"

const ISL::Lexer::Token* base_tok(const ISL::Lexer::TokenVariant& tok_variant) {
    return std::visit([](auto&& tok) -> const ISL::Lexer::Token* {
        return &tok;
    }, tok_variant);
}

TEST(LexerTests, BasicLexTest) {
    auto sample_file = R"(
        3 + 2;
    )";

    std::istringstream sstream(sample_file);
    auto tstream = ISL::Lexer::tokenize(sstream);
    auto tok_iter = tstream.begin();

    EXPECT_EQ(base_tok(*tok_iter)->getTokenType(), ISL::Lexer::TokenType::TOK_INTEGER);
    ++tok_iter;

    EXPECT_EQ(base_tok(*tok_iter)->getTokenType(), ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR);
    ++tok_iter;

    EXPECT_EQ(base_tok(*tok_iter)->getTokenType(), ISL::Lexer::TokenType::TOK_INTEGER);
    ++tok_iter;

    EXPECT_EQ(base_tok(*tok_iter)->getTokenType(), ISL::Lexer::TokenType::TOK_END_STATEMENT);
    ++tok_iter;
}

TEST(LexerTests, LineInfoTest) {
    // Make sure this is by itself, we want to enforce a specific set of line
    //  numbers
    auto sample_file = R"(3 + 2;)";

    std::istringstream sstream(sample_file);
    auto tstream = ISL::Lexer::tokenize(sstream);
    auto tok_iter = tstream.begin();

    EXPECT_EQ(base_tok(*tok_iter)->getLineNumber(), 1);
    EXPECT_EQ(base_tok(*tok_iter)->getLine(), "3 + 2;");
    EXPECT_EQ(base_tok(*tok_iter)->getSubstr(), "3");
    ++tok_iter;

    EXPECT_EQ(base_tok(*tok_iter)->getLineNumber(), 1);
    EXPECT_EQ(base_tok(*tok_iter)->getLine(), "3 + 2;");
    EXPECT_EQ(base_tok(*tok_iter)->getSubstr(), "+");
    ++tok_iter;

    EXPECT_EQ(base_tok(*tok_iter)->getLineNumber(), 1);
    EXPECT_EQ(base_tok(*tok_iter)->getLine(), "3 + 2;");
    EXPECT_EQ(base_tok(*tok_iter)->getSubstr(), "2");
    ++tok_iter;

    EXPECT_EQ(base_tok(*tok_iter)->getLineNumber(), 1);
    EXPECT_EQ(base_tok(*tok_iter)->getLine(), "3 + 2;");
    EXPECT_EQ(base_tok(*tok_iter)->getSubstr(), ";");
}

