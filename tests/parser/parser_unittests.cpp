
#include <bitset>

#include "TestUtil.h"

#include "gtest/gtest.h"

#include "Utility.h"

#include "TokenList.h"
#include "ParseExpressions.h"
#include "ParseVariableExpression.h"

#include "ASTVisitor.h"
#include "JSONASTPrinter.h"

#include "ast/IdentifierExpression.h"
#include "ast/ConstantExpression.h"
#include "ast/BinaryExpression.h"

TEST(ParserTests, ValueTest) {
    std::vector<ISL::Lexer::ValueToken> tokens = {
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_BOOLEAN, true),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_BOOLEAN, false),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_CHARACTER, 'b'),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 5),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_FLOAT, 3.1415f),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_DOUBLE, 3.1415),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_STRING, "foobar")
    };

    for(auto it = tokens.begin(); it != tokens.end(); ++it) {
        auto expr = ISL::Parser::parseValue(it->getValueType());
        ASSERT_EQ((bool)expr, true); // valid value must be returned

        EXPECT_EQ(expr.value()->getValue(), it->getValueType());
    }
}

TEST(ParserTests, BinaryExpressionTest) {
    ISL::Parser::TokenList tokens = {
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 5),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                  ISL::Common::Operator(ISL::Common::OperatorType::ADD)),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 10)
    };

    ISL::Parser::TokenListIter it = tokens.cbegin();
    ISL::Parser::TokenListIter end = tokens.cend();
    auto expr = ISL::Parser::parseExpression(it, end);

    ASSERT_EQ((bool)expr, true);

    auto binexpr = dynamic_cast<ISL::Parser::AST::BinaryExpression*>(expr.value().get());

    ASSERT_NE(binexpr, nullptr);

    EXPECT_NE(binexpr->getLHS(), nullptr);
    EXPECT_NE(binexpr->getRHS(), nullptr);
    EXPECT_EQ(binexpr->getOperator(),
              ISL::Common::Operator(ISL::Common::OperatorType::ADD));

    auto* lhs = dynamic_cast<ISL::Parser::AST::ConstantExpression*>(binexpr->getLHS().get());
    ASSERT_NE(lhs, nullptr);
    EXPECT_EQ(std::holds_alternative<int>(lhs->getValue()), true);
    EXPECT_EQ(std::get<int>(lhs->getValue()), 5);

    auto* rhs = dynamic_cast<ISL::Parser::AST::ConstantExpression*>(binexpr->getRHS().get());
    ASSERT_NE(rhs, nullptr);
    EXPECT_EQ(std::holds_alternative<int>(rhs->getValue()), true);
    EXPECT_EQ(std::get<int>(rhs->getValue()), 10);
}

TEST(ParserTests, ComplexBinaryExpressionTest) {
    ISL::Parser::TokenList tokens = {
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_START_OPERATOR,
                                  ISL::Common::Operator(ISL::Common::OperatorType::PAREN_START)),
            ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 5),
            ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                      ISL::Common::Operator(ISL::Common::OperatorType::MUL)),
            ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 10),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_END_OPERATOR,
                                  ISL::Common::Operator(ISL::Common::OperatorType::PAREN_END)),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                  ISL::Common::Operator(ISL::Common::OperatorType::ADD)),
        ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 10)
    };

    ISL::Parser::TokenListIter it = tokens.cbegin();
    ISL::Parser::TokenListIter end = tokens.cend();
    auto expr = ISL::Parser::parseExpression(it, end);

    ASSERT_EQ((bool)expr, true);

    auto binexpr = dynamic_cast<ISL::Parser::AST::BinaryExpression*>(expr.value().get());

    ASSERT_NE(binexpr, nullptr);

    EXPECT_NE(binexpr->getLHS(), nullptr);
    EXPECT_NE(binexpr->getRHS(), nullptr);
    EXPECT_EQ(binexpr->getOperator(),
              ISL::Common::Operator(ISL::Common::OperatorType::ADD));

    auto* lhs = dynamic_cast<ISL::Parser::AST::BinaryExpression*>(binexpr->getLHS().get());
    ASSERT_NE(lhs, nullptr);
    {
        EXPECT_NE(lhs->getLHS(), nullptr);
        EXPECT_NE(lhs->getRHS(), nullptr);
        EXPECT_EQ(lhs->getOperator(),
                  ISL::Common::Operator(ISL::Common::OperatorType::MUL));

        auto* lhs2 = dynamic_cast<ISL::Parser::AST::ConstantExpression*>(lhs->getLHS().get());
        ASSERT_NE(lhs2, nullptr);
        EXPECT_EQ(std::holds_alternative<int>(lhs2->getValue()), true);
        EXPECT_EQ(std::get<int>(lhs2->getValue()), 5);

        auto* rhs2 = dynamic_cast<ISL::Parser::AST::ConstantExpression*>(lhs->getRHS().get());
        ASSERT_NE(rhs2, nullptr);
        EXPECT_EQ(std::holds_alternative<int>(rhs2->getValue()), true);
        EXPECT_EQ(std::get<int>(rhs2->getValue()), 10);
    }

    auto* rhs = dynamic_cast<ISL::Parser::AST::ConstantExpression*>(binexpr->getRHS().get());
    ASSERT_NE(rhs, nullptr);
    EXPECT_EQ(std::holds_alternative<int>(rhs->getValue()), true);
    EXPECT_EQ(std::get<int>(rhs->getValue()), 10);
}

TEST(ParserTests, IdentifierTest) {
    using namespace std::string_literals;

    ISL::Parser::TokenList tokens = {
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "foobar")
    };

    ISL::Parser::TokenListIter it = tokens.cbegin();
    ISL::Parser::TokenListIter end = tokens.cend();
    auto expr = ISL::Parser::parseIdentifier(it, end);

    ASSERT_EQ((bool)expr, true);

    EXPECT_EQ(expr.value()->getIdentifier(), "foobar"s);
}

TEST(ParserTests, TypeTest) {
    ISL::Parser::TokenList tokens = {
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "List"),
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "List"),
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "Pointer"),
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "int")
    };

    ISL::Parser::TokenListIter it = tokens.cbegin();
    ISL::Parser::TokenListIter end = tokens.cend();
    auto expr = parseTypeExpression(it, end);

    ASSERT_EQ((bool)expr, true);

    auto ste = dynamic_cast<ISL::Parser::AST::SimpleTypeExpression*>(expr.value().get());
    EXPECT_EQ(ste->getTypeIdentifier()->getIdentifier(), "int");

    auto sub2 = dynamic_cast<ISL::Parser::AST::SimpleTypeExpression*>(ste->getSubType().get());
    EXPECT_EQ(sub2->getTypeIdentifier()->getIdentifier(), "Pointer");

    auto sub3 = dynamic_cast<ISL::Parser::AST::SimpleTypeExpression*>(sub2->getSubType().get());
    EXPECT_EQ(sub3->getTypeIdentifier()->getIdentifier(), "List");

    auto sub4 = dynamic_cast<ISL::Parser::AST::SimpleTypeExpression*>(sub2->getSubType().get());
    EXPECT_EQ(sub4->getTypeIdentifier()->getIdentifier(), "List");
}

TEST(ParserTests, VariableTest) {
    ISL::Parser::TokenList tokens = {
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "int"),
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "a"),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                  ISL::Common::Operator(ISL::Common::OperatorType::ASSIGN)),
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "5")
    };

    ISL::Parser::TokenListIter it = tokens.cbegin();
    ISL::Parser::TokenListIter end = tokens.cend();
    auto expr = parseVariableExpression(it, end);

    ASSERT_NE(expr, nullptr);

    ASSERT_NE(expr->getTypeExpression(), nullptr);
    ASSERT_NE(expr->getVarIdentifier(), nullptr);
    ASSERT_NE(expr->getValueExpr(), nullptr);

    EXPECT_EQ(dynamic_cast<ISL::Parser::AST::SimpleTypeExpression*>(expr->getTypeExpression().get())->getTypeIdentifier()->getIdentifier(), "int");
    EXPECT_EQ(expr->getVarIdentifier()->getIdentifier(), "a");
}

TEST(ParserTests, VisitorTest) {
    ISL::Parser::TokenList tokens = {
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_START_OPERATOR,
                                  ISL::Common::Operator(ISL::Common::OperatorType::PAREN_START)),
            ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 5),
            ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                      ISL::Common::Operator(ISL::Common::OperatorType::MUL)),
            ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 10),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_END_OPERATOR,
                                  ISL::Common::Operator(ISL::Common::OperatorType::PAREN_END)),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                  ISL::Common::Operator(ISL::Common::OperatorType::ADD)),
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "a"),
    };

    ISL::Parser::TokenListIter it = tokens.cbegin();
    ISL::Parser::TokenListIter end = tokens.cend();
    auto expr = ISL::Parser::parseExpression(it, end);

    ASSERT_EQ((bool)expr, true);

    std::vector<ISL::Parser::AST::ExpressionPtr> exprs;
    exprs.push_back(std::move(expr.value()));

    // One bit for each expression
    std::bitset<5> visited_exprs;

    class TestVisitor: public ISL::Parser::ASTVisitor {
        public:
            TestVisitor(std::bitset<5>& visited_exprs):
                visited_exprs(visited_exprs)
            { }
            bool visitBinaryExpression(const ISL::Parser::AST::BinaryExpression& expr) override
            {
                if(expr.getOperator() == ISL::Common::Operator(ISL::Common::OperatorType::MUL))
                {
                    PRINTF("Visited Multiply Expression.\n");
                    visited_exprs.set(0);
                }
                else if(expr.getOperator() == ISL::Common::Operator(ISL::Common::OperatorType::ADD))
                {
                    PRINTF("Visited Add Expression.\n");
                    visited_exprs.set(1);
                } else {
                    EXPECT_FALSE(false);
                }

                return true;
            }

            bool visitConstantExpression(const ISL::Parser::AST::ConstantExpression& expr) override
            {
                auto value = expr.getValue();

                // ASSERT_EQ(std::holds_alternative<int>(value), true);

                int ivalue = std::get<int>(value);

                if(ivalue == 5) {
                    PRINTF("Visited Constant 5\n");
                    visited_exprs.set(2);
                } else if(ivalue == 10) {
                    PRINTF("Visited Constant 10\n");
                    visited_exprs.set(3);
                }

                return true;
            }

            bool visitIdentifierExpression(const ISL::Parser::AST::IdentifierExpression& expr) override
            {
                if(expr.getIdentifier() == "a") {
                    PRINTF("Visited Identifier 'a'\n");
                    visited_exprs.set(4);
                }

                return true;
            }
        private:
            std::bitset<5>& visited_exprs;
    } visitor(visited_exprs);

    EXPECT_EQ(visitor.traverseAST(exprs), true);
    EXPECT_EQ(visited_exprs.all(), true);
}

TEST(ParserTests, JSONPrinterTest) {
    std::stringstream outstream;

    auto json_output = "[\n"
                       "    {\n"
                       "        \"type\": \"BinaryExpression\",\n"
                       "        \"operator\": \"ADD\",\n"
                       "        \"lhs\": {\n"
                       "            \"type\": \"BinaryExpression\",\n"
                       "            \"operator\": \"MUL\",\n"
                       "            \"lhs\": {\n"
                       "                \"type\": \"Value\",\n"
                       "                \"value\": 5\n"
                       "            },\n"
                       "            \"rhs\": {\n"
                       "                \"type\": \"Value\",\n"
                       "                \"value\": 10\n"
                       "            }\n"
                       "        },\n"
                       "        \"rhs\": {\n"
                       "            \"type\": \"Identifier\",\n"
                       "            \"iden\": \"a\"\n"
                       "        }\n"
                       "    }\n"
                       "]"
        ;

    ISL::Parser::TokenList tokens = {
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_START_OPERATOR,
                                  ISL::Common::Operator(ISL::Common::OperatorType::PAREN_START)),
            ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 5),
            ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                      ISL::Common::Operator(ISL::Common::OperatorType::MUL)),
            ISL::Lexer::ValueToken(ISL::Lexer::TokenType::TOK_INTEGER, 10),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_END_OPERATOR,
                                  ISL::Common::Operator(ISL::Common::OperatorType::PAREN_END)),
        ISL::Lexer::OperatorToken(ISL::Lexer::TokenType::TOK_SIMPLE_OPERATOR, 
                                  ISL::Common::Operator(ISL::Common::OperatorType::ADD)),
        ISL::Lexer::IdentifierToken(ISL::Lexer::TokenType::TOK_IDENTIFIER, "a"),
    };

    ISL::Parser::TokenListIter it = tokens.cbegin();
    ISL::Parser::TokenListIter end = tokens.cend();
    auto expr = ISL::Parser::parseExpression(it, end);

    ASSERT_EQ((bool)expr, true);

    std::vector<ISL::Parser::AST::ExpressionPtr> exprs;
    exprs.push_back(std::move(expr.value()));

    ISL::Parser::JSONASTPrinter json_printer(outstream);
    json_printer.traverseAST(exprs);

    EXPECT_EQ(outstream.str(), json_output);
}

