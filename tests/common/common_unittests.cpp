
#include <map>
#include <vector>

#include "gtest/gtest.h"

#include "Utility.h"

TEST(CommonTests, toLower) {
    const auto TEST_VALUES = std::map<std::string, std::string>{
        { "hello there", "hello there" },
        { "HELLO THERE", "hello there" },
        { "HeLLo THERE", "hello there" },
        { "Hello There", "hello there" },
        { "+1234567890", "+1234567890" },
    };

    for(auto&& [test, expected] : TEST_VALUES) {
        EXPECT_EQ(ISL::Common::toLower(test), expected);
    }
}

TEST(CommonTests, isOctDigit) {
    EXPECT_EQ(true, ISL::Common::isOctDigit('0'));
    EXPECT_EQ(true, ISL::Common::isOctDigit('1'));
    EXPECT_EQ(true, ISL::Common::isOctDigit('2'));
    EXPECT_EQ(true, ISL::Common::isOctDigit('3'));
    EXPECT_EQ(true, ISL::Common::isOctDigit('4'));
    EXPECT_EQ(true, ISL::Common::isOctDigit('5'));
    EXPECT_EQ(true, ISL::Common::isOctDigit('6'));
    EXPECT_EQ(true, ISL::Common::isOctDigit('7'));

    EXPECT_EQ(false, ISL::Common::isOctDigit('8'));
    EXPECT_EQ(false, ISL::Common::isOctDigit('9'));

    EXPECT_EQ(false, ISL::Common::isOctDigit('a'));
    EXPECT_EQ(false, ISL::Common::isOctDigit('\0'));
}

TEST(CommonTests, subvec) {
    std::vector<int> testvec = {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
    };

    EXPECT_EQ(ISL::Common::subvec(testvec, 0, 0), testvec);
    EXPECT_EQ(ISL::Common::subvec(testvec, 0, 10),
              std::vector<int>({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
    EXPECT_EQ(ISL::Common::subvec(testvec, 10, 0),
              std::vector<int>({10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}));
}

