
function f(int a, int b) -> int;
    return a + b;
endfunction;

function foo(int a, Pointer (int, int) -> int func_call, float b, int c, String string) -> void;
    return Concat(ToString(cast(float, a + func_call(a, c)) + b), string);

endfunction;

f(1,2);

f(f(1,2), 3);

f(1,2) + f(1,2);

// Nested example
int i = 0;

f(i, f(1, 2));

// Very complex example
foo(5, f, cast(float, i), i, "Hello there!");

