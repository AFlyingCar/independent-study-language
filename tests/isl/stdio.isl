
extern function putchar(char) -> int;
extern function puts(String) -> int;

extern function getchar() -> char;
extern function gets(String) -> String;

