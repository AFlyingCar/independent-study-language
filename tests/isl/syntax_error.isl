
// This code has several syntax errors
// No steps of the compiler should crash, instead they should successfully
//  report that the syntax is erroneous and fail to compile.

import "extern.isl"

puts("hello, world"

/* this is a broken comment */

puts("This string works");

