
import "stdio.isl";

function isPrime(int n) -> bool;
    if(n == 1);
        return true;
    else;
        for(int i = 2; i < n; ++i);
            if(n % 2 == 0 && i != n);
                return false;
            endif;
        endfor;
    endif;

    return true;
endfunction;

printf("%d", isPrime(1)); // 1
printf("%d", isPrime(2)); // 1
printf("%d", isPrime(3)); // 1
printf("%d", isPrime(4)); // 0
printf("%d", isPrime(5)); // 1
printf("%d", isPrime(6)); // 0

