
// This should succeed
function foo() -> int;
    return 5;
endfunction;

// This should not work
function bar() -> ThisTypeDoesNotExist;
endfunction;

// This should not
function f() -> void;
    return 5;
endfunction;

