/**
 * Implements fizzbuzz, from 0 to 100
 */

import "stdio.isl"; // get standard printing functions

// Walk until i becomes 100 from 0
for(int i = 0; i < 100; ++i);
    bool printnumber = true;

    // If i is a multiple of 3 print Fizz
    if(i % 3 == 0);
        puts("Fizz");
        printnumber = false;
    endif;

    // If i is a multiple of 5 print Buzz
    if(i % 5 == 0);
        puts("Buzz");
        printnumber = false;
    endif;

    // If we didn't print either Fizz or Buzz, then print i
    if(printnumber);
        // printf("%d", i);
    endif;
endfor;

