#!/usr/bin/env python3
import os,sys,datetime

NAME = "Tyler" # sys.argv[1]

def getStartTime():
    now = datetime.datetime.time(datetime.datetime.now())
    nowd = datetime.datetime.date(datetime.datetime.now())
    return str(nowd.year) + "-" + str(nowd.month) + "-" + str(nowd.day) + " " + str(now.hour)+":"+str(now.minute)+":"+str(now.second)

def logTimeWorked(nameAndStart):
    open("clockin.log","a").write(nameAndStart + "\n")

def setStarted(started,name):
    if(started):
        nameAndStartTime = open(".clockinout.lock",'r').read()
        logTimeWorked(nameAndStartTime + " -> " + getStartTime())
        os.remove(".clockinout.lock")
    else:
        startTime = getStartTime()
        open(".clockinout.lock",'w').write(name + " - " + startTime);

def checkStarted():
    return os.path.exists(".clockinout.lock")

def main():
    # Are we clocking in or out?
    if(checkStarted()):
        # Don't bother sending a name if we are clocking out, since that is already known
        setStarted(True, "")
    else:
        setStarted(False, NAME)

if __name__ == "__main__":
    main()

