
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>

#include "docopt.h"

#include "llvm/ADT/Optional.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/Program.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Transforms/IPO/Internalize.h"

// TODO: This forking thing won't work on Windows, so we need to do conditional
//  compilation to make it work
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "Parser.h"
#include "Logger.h"
#include "Settings.h"

#include "LLVMOstream.h"

#include "TokenizerError.h"
#include "Tokenizer.h"

#include "ErrorCount.h"

#include "IRVisitor.h"
#include "CodeGen.h"

static bool emit_asm = false;
static bool emit_obj = false;
static const char CODEGEN_USAGE[] = 
R"(Independent Study Language (ISL) Compiler

    Usage:
      isl <filename> [-v | --verbose | -q | --quiet ] [ -o <output_filename> | --output=<output_filename> ] [ -i <import_prefix,...> | --import=<import_prefix,...> ] [ -S | --emit-assembly ] [ -c ] [ -l <library,...> ] [ -O <object,...> ]

    Options:
      -h --help                        Show this message
      -v --verbose                     Use verbose output
      -q --quiet                       Use verbose output
      -o --output <output_filename>    Specify where to write the output executable
      -i --import <import_prefix,...>  Add an additional prefix to the import search path.
      -S --emit-assembly               Emit Assembly code instead of an executable.
      -c                               Emit only an object file, and do not attempt to link.
      -l <library,...>                 Link against the specified library.
      -O <object,...>                  Link against the specified Object file.
)";

// https://releases.llvm.org/8.0.0/docs/tutorial/LangImpl08.html
namespace {
    std::filesystem::path getTempDirectory() {
        return std::filesystem::temp_directory_path();
    }

    /*
     * Note, this is how things get invoked when we run clang manually:
     "/usr/bin/ld" --hash-style=both --build-id --eh-frame-hdr -m elf_x86_64
                   -dynamic-linker /lib64/ld-linux-x86-64.so.2 -o extern
                   /usr/bin/../lib/gcc/x86_64-linux-gnu/8/../../../x86_64-linux-gnu/crt1.o
                   /usr/bin/../lib/gcc/x86_64-linux-gnu/8/../../../x86_64-linux-gnu/crti.o
                   /usr/bin/../lib/gcc/x86_64-linux-gnu/8/crtbegin.o
                   -L/usr/bin/../lib/gcc/x86_64-linux-gnu/8
                   -L/usr/bin/../lib/gcc/x86_64-linux-gnu/8/../../../x86_64-linux-gnu
                   -L/lib/x86_64-linux-gnu -L/lib/../lib64
                   -L/usr/lib/x86_64-linux-gnu
                   -L/usr/bin/../lib/gcc/x86_64-linux-gnu/8/../../..
                   -L/usr/lib/llvm-9/bin/../lib
                   -L/lib -L/usr/lib extern.o foo.o -lgcc --as-needed -lgcc_s
                   --no-as-needed -lc -lc -lgcc --as-needed -lgcc_s
                   --no-as-needed /usr/bin/../lib/gcc/x86_64-linux-gnu/8/crtend.o
                   /usr/bin/../lib/gcc/x86_64-linux-gnu/8/../../../x86_64-linux-gnu/crtn.o
     */

    // LLVM cannot actually do this itself, so we need to outsource to ld
    bool createExecutable(const std::filesystem::path& object_file_path,
                          const std::vector<std::filesystem::path>& object_paths,
                          const std::vector<std::string>& libraries)
    {
        std::vector<std::string> arguments;

        // TODO: We will need to grab a different linker on different platforms
        //  Also, we're just hardcoding this path, which is _bad_
        auto linker = "/usr/bin/ld";

        arguments.push_back("-o");
        arguments.push_back(object_file_path.generic_string().c_str());

        // Set up some generic arguments first
        //  These are needed in order for ld to link our executable properly,
        //  and most likely won't change drastically (unless we move to a
        //  different architecture or OS).
        // For now they can be hardcoded, but I grabbed these directly from how
        //  clang invokes 'ld', which is not the best way of doing things since
        //  it builds these paths up.
        // For improvement: search for these files or ship them ourselves.

        // Specify the architecture
        arguments.push_back("-m");
        arguments.push_back("elf_x86_64");
        arguments.push_back("-dynamic-linker");
        arguments.push_back("/lib64/ld-linux-x86-64.so.2");

        // Link in the startup modules so we don't segfault on start/exit
        //  See: https://www.linuxquestions.org/questions/programming-9/segfault-when-linking-with-ld-758599/
        // Note: if we want main to be the actual entry point, then we need to
        //  do more work such as inserting an exit() call
        // For now, it's simpler to just link against these libraries
        arguments.push_back("/usr/lib/gcc/x86_64-linux-gnu/8/../../../x86_64-linux-gnu/crt1.o");
        arguments.push_back("/usr/lib/gcc/x86_64-linux-gnu/8/../../../x86_64-linux-gnu/crti.o");
        arguments.push_back("/usr/lib/gcc/x86_64-linux-gnu/8/crtbegin.o");
        arguments.push_back("--no-as-needed");
        arguments.push_back("/usr/bin/../lib/gcc/x86_64-linux-gnu/8/crtend.o");
        arguments.push_back("/usr/bin/../lib/gcc/x86_64-linux-gnu/8/../../../x86_64-linux-gnu/crtn.o");

        for(auto&& library : libraries) {
            using namespace std::string_literals;
            arguments.push_back("-l"s + library);
        }

        for(auto&& path : object_paths) {
            arguments.push_back(path);
        }

        if(ISL::Common::getOptions().verbose) {
            arguments.push_back("--verbose");
        }

        ISL::Common::Log::debug("Invoking 'ld' like so: ");
        std::stringstream s;
        s << "ld ";
        for(auto i = 0; i < arguments.size(); ++i) {
            s << arguments.at(i) << ' ';
        }

        ISL::Common::Log::debug(s.str());

#if 0
        auto pid = fork();
        if(pid == -1) {
            ISL::Common::Log::error("Failed to fork process.");
            return false;
        } else if(pid == 0) {
            ISL::Common::Log::debug("Invoking 'ld' like so: ");
            std::stringstream s;
            s << "ld ";

            // TODO: We should have a better way of converting the arguments
            //  to a char*[].
            char* args[1024] = { 0 };
            args[0] = const_cast<char*>(linker);
            for(auto i = 0; i < arguments.size(); ++i) {
                args[i + 1] = const_cast<char*>(arguments.at(i).c_str());

                s << arguments.at(i) << ' ';
            }

            ISL::Common::Log::debug(s.str());

            execv(linker, args);
        } else {
            int status = 0;
            waitpid(pid, &status, WCONTINUED | WUNTRACED);

            using namespace std::string_literals;
            ISL::Common::Log::debug("We are done waiting for ld to finish, status is "s + std::to_string(WEXITSTATUS(status)));

            return WEXITSTATUS(status) == 0;
        }

        // Will never get hit, but it pleases the compiler
        return true;
#else
        std::vector<llvm::StringRef> args;
        args.push_back(linker);
        for(auto&& arg : arguments) args.push_back(arg);

        std::string error_message;
        auto result = llvm::sys::ExecuteAndWait(linker, args, llvm::None, {}, 0, 0, &error_message);

        ISL::Common::Log::debug(std::string(linker) + " finished with a status code of " + std::to_string(result));
        if(result != 0) {
            using namespace std::string_literals;
            ISL::Common::Log::error("ExecuteAndWait Failed. Reason: "s + error_message);
        }

        return result == 0;
#endif
    }

    [[nodiscard]]
        ISL::CodeGen::IRVisitor::ModulePtr
            linkModules(const std::vector<ISL::CodeGen::IRVisitor::ModulePtr>& modules,
                        llvm::LLVMContext& context)
        {
            // We only have one module, so lets just return it without going
            //  through the linker
            if(modules.size() == 1) {
                return modules.at(0);
            }

            auto composite_module = new llvm::Module("composite", context); // std::make_unique<llvm::Module>("composite", context);
            llvm::Linker linker(*composite_module);

            // TODO: Are there any other flags we want?
            auto flags = llvm::Linker::Flags::OverrideFromSrc;

            for(auto&& module : modules) {
                auto mod = std::unique_ptr<llvm::Module>(module);
                // TODO: What does internalizing symbols actually mean?
                linker.linkInModule(std::move(mod), flags/*,
                        // Note: this callback is actually required by LLVM
                        //  They can't place this internally to LinkModules because
                        //  doing so would apparently create a circular dependency
                        // Probably... I'm not actually sure
                        // This has basically been copied from llvm-link.cpp
                        //  anyway
                        [](llvm::Module& m, const llvm::StringSet<>& gvs) {
                            llvm::internalizeModule(m, [&gvs](const llvm::GlobalValue& gv) {
                                                           return !gv.hasName() || (gvs.count(gv.getName()) == 0);
                                                       });
                        }*/);
                // We are probably leaking memory here, but we're about to stop
                //  running soon, so who cares
            }

            return composite_module; // std::move(composite_module);
        }

    [[nodiscard]]
        bool writeObjectCode(llvm::Module* module, llvm::TargetMachine* machine,
                             const std::string& triple, llvm::raw_fd_ostream& output)
        {
            module->setDataLayout(machine->createDataLayout());
            module->setTargetTriple(triple);

            llvm::legacy::PassManager pass;
            auto filetype = emit_asm ? llvm::TargetMachine::CGFT_AssemblyFile :
                                       llvm::TargetMachine::CGFT_ObjectFile;

            if(machine->addPassesToEmitFile(pass, output, nullptr, filetype)) {
                ISL::Common::Log::error("Cannot emit a file of this type!");
                return false;
            }
            pass.run(*module);
            
            return true;
        }

    [[nodiscard]]
        bool parseCode(std::istream& input_stream,
                       std::vector<ISL::Parser::AST::ExpressionPtr>& statements)
        {
            try {
                auto token_stream = ISL::Lexer::tokenize(input_stream);
                std::vector<ISL::Lexer::TokenVariant> tokens(token_stream.begin(),
                                                             token_stream.end());

                statements = ISL::Parser::parse(tokens);

                if(auto errors = ISL::Parser::getErrorCount(); errors > 0) {
                    ISL::Common::Log::error(std::to_string(errors) +
                                            " errors generated.");
                    return false;
                }
                return true;
            } catch(const ISL::Lexer::TokenizerError& e) {
                ISL::Common::Log::error(e.what());
                ISL::Common::Log::error(std::to_string(ISL::Parser::getErrorCount()) + " errors generated.");
                return false;
            }

        }


    [[nodiscard]]
        bool compile(const std::filesystem::path& main_filename,
                     const std::string& module_name,
                     std::istream& input,
                     const std::filesystem::path& output_filename, // std::ostream& output,
                     const std::vector<std::filesystem::path>& import_prefixes,
                     const std::vector<std::filesystem::path>& objects,
                     const std::vector<std::string>& libraries)
        {
            // Prepare LLVM for outputting object code!

            ISL::Common::Log::debug("Initializing LLVM targets.");

            // We will only support compiling to the default target, but maybe
            //  we could support targeting other architectures in the future?
            auto triple = llvm::sys::getDefaultTargetTriple();

            llvm::InitializeAllTargetInfos();
            llvm::InitializeAllTargets();
            llvm::InitializeAllTargetMCs();
            llvm::InitializeAllAsmParsers();
            llvm::InitializeAllAsmPrinters();

            std::string error_string;

            auto target = llvm::TargetRegistry::lookupTarget(triple, error_string);

            if(!target) {
                ISL::Common::Log::error(error_string);
                return false;
            }

            // TODO: We should have options for choosing the cpu and features
            auto cpu = "generic";
            auto features = "";

            llvm::TargetOptions options;
            auto relocation_model = llvm::Optional<llvm::Reloc::Model>();

            ISL::Common::Log::debug("Targets initialized. Creating target machine.");

            // Create the "machine"
            auto machine = target->createTargetMachine(triple, cpu, features,
                                                       options,
                                                       relocation_model);
            // Setup is now almost completely done, lets go ahead and parse our
            //  own code
            ISL::Common::Log::debug("Parsing code");
            std::vector<ISL::Parser::AST::ExpressionPtr> ast;
            if(!parseCode(input, ast)) {
                return false;
            }

            ISL::Common::Log::debug("Visiting code");
            ISL::CodeGen::MainIRVisitor ir_visitor(module_name,
                                                   std::filesystem::absolute(main_filename));

            // Register all prefixes with the IRVisitor
            for(auto&& prefix : import_prefixes) {
                ir_visitor.addImportPrefix(prefix);
            }

            std::vector<ISL::CodeGen::IRVisitor::ModulePtr> modules;
            ir_visitor.setImportFileWriter([&modules](ISL::CodeGen::IRVisitor::ModulePtrRef module,
                                                      std::ostream&)
            {
                modules.push_back(module);
                // return writeObjectCode(module, machine, triple, output);
                return true;
            });

            if(!ir_visitor.traverseAST(ast)) return false;
            ir_visitor.generateMainFunction();

            modules.push_back(ir_visitor.getModule());

            using namespace std::string_literals;
            ISL::Common::Log::debug("After visiting, we have "s +
                                    std::to_string(modules.size()) +
                                    " modules to link together.");

            // All of our code is now parsed and codegen has take place, lets
            //  prepare it for machine code generation

            ISL::Common::Log::debug("Linking all modules together");
            auto& context = ir_visitor.getContext();
            auto module = linkModules(modules, context);

            if(emit_obj || emit_asm) {
                ISL::Common::Log::debug("Emitting object file.");
                std::error_code error;
                llvm::raw_fd_ostream output(output_filename.generic_string().c_str(),
                                            error);
                if(error) {
                    using namespace std::string_literals;
                    ISL::Common::Log::error("Failed to open file: "s +
                                            error.message());
                    return false;
                }

                return writeObjectCode(module, machine, triple, output);
            } else {
                ISL::Common::Log::debug("Emitting executable.");
                std::error_code error;
                auto path = (getTempDirectory() / "a.o").generic_string().c_str();
                llvm::raw_fd_ostream output(path, error);
                if(error) {
                    using namespace std::string_literals;
                    ISL::Common::Log::error("Failed to open file: "s +
                                            error.message());
                    return false;
                }

                if(!writeObjectCode(module, machine, triple, output)) {
                    ISL::Common::Log::error("Failed to write object code.");
                    return false;
                }

                auto new_objects = objects;
                new_objects.push_back(path);
                return createExecutable(output_filename, new_objects, libraries);
            }
        }
}

int main(int argc, char** argv) {
    using namespace std::string_literals;

    auto args = docopt::docopt(CODEGEN_USAGE, { argv + 1, argv + argc }, true,
                               "Independent Study Language (ISL) Compiler");


    std::string token_filename = args.at("<filename>").asString();

    ISL::Common::getOptions().verbose = args.at("--verbose").asBool();
    ISL::Common::getOptions().quiet = args.at("--quiet").asBool();

    ISL::Common::Log::debug("isl invoked with the following arguments:");
    std::stringstream arg_data;
    for(auto& arg : args) {
        arg_data << arg.first << "=" << arg.second << std::endl;
    }
    ISL::Common::Log::debug(arg_data.str());

    emit_asm = args.at("--emit-assembly").asBool();
    emit_obj = args.at("-c").asBool();

    std::string output_file;
    std::filesystem::path output_path;

    if(args.at("--output")) {
        output_file = args.at("--output").asString();
    }

    if(output_file.empty()) {
        if(emit_asm) {
            output_path = "a.s";
        } else if(emit_obj) {
            output_path = "a.o";
        } else {
            output_path = "a.out";
        }
    } else {
        output_path = std::filesystem::absolute(output_file);
    }

    std::vector<std::filesystem::path> import_prefixes;
    if(args.at("--import")) {
        auto&& import_prefixes_raw = args.at("--import").asStringList();

        for(auto&& prefix : import_prefixes_raw) {
            import_prefixes.push_back(std::filesystem::absolute(prefix));
        }
    }

    std::vector<std::filesystem::path> objects;
    if(args.at("-O")) {
        auto&& objects_raw = args.at("-O").asStringList();

        for(auto&& object : objects_raw) {
            objects.push_back(std::filesystem::absolute(object));
        }
    }

    std::vector<std::string> libraries;
    if(args.at("-l")) {
        libraries = args.at("-l").asStringList();
    }

    // Make sure we at least link against the C standard library
    if(std::find(libraries.begin(), libraries.end(), "c") == libraries.end()) {
        libraries.push_back("c");
    }

    if(token_filename == "-") {
        // std::ofstream ofile(output_path);
        if(!compile("", "<STDIN>", std::cin, output_path /*ofile*/, import_prefixes, objects, libraries)) {
            ISL::Common::Log::error("Compilation failed.");
        }
    } else {
        std::filesystem::path token_file_path(token_filename);

        if(std::filesystem::exists(token_file_path)) {
            std::ifstream file(token_file_path);
            // std::ofstream ofile(output_path);
            if(!compile(token_filename, token_filename, file, output_path /*ofile*/,
                        import_prefixes, objects, libraries))
            {
                ISL::Common::Log::error("Compilation failed.");
            }
            return 0;
        } else {
            ISL::Common::Log::error("File not found: "s + token_filename);
            return 1;
        }
    }

    return 0;
}
