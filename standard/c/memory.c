
#include <stdlib.h>

void* Allocate(unsigned int size) {
    return malloc(size);
}

void Free(void* p) {
    free(p);
}

